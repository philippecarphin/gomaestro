package nodelogger

import (
	"fmt"
	"log"
	"os"
	"strings"
	"sync"
)

// NodeLoggerArgs specifies the arguments for the Nodelogger RPC
// interface.
type NodeLoggerArgs struct {
	Datestamp   string
	ExpHome     string
	NodePath    string
	Message     string
	LogDest     string
	MessageType string
}

var logLock sync.Mutex

// NodeLogger is the method for logging
func NodeLogger(args *NodeLoggerArgs) error {
	logMessage, err := composeMessage(args)
	if err != nil {
		return nil
	}

	path, err := logFilePath(args)
	if err != nil {
		fmt.Printf("Could not construct log file path")
		return err
	}

	logLock.Lock()
	defer logLock.Unlock()

	f, err := os.OpenFile(path, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Printf("Nodelogger (%s) : %v\n",args.NodePath, err)
	}
	defer f.Close()

	nb, err := f.Write([]byte(logMessage))
	if err != nil {
		return err
	}
	if nb == 0 {
		return fmt.Errorf("%d bytes written", nb)
	}

	return nil
}

func composeMessage(args *NodeLoggerArgs) (string, error) {
	hour := args.Datestamp[8:10]
	min := "00"
	sec := "00"
	date := args.Datestamp[0:8]
	timestamp := fmt.Sprintf("TIMESTAMP=%s.%s:%s:%s", date, hour, min, sec)

	node := fmt.Sprintf("SEQNODE=%s", args.NodePath)

	msgType := fmt.Sprintf("MSGTYPE=%s", args.MessageType)

	loop := fmt.Sprintf("SEQLOOP=%s", "")

	msg := fmt.Sprintf("SEQMSG=[GO]%s", args.Message)

	return strings.Join([]string{timestamp, node, msgType, loop, msg}, ":") + "\n", nil
}

func logFilePath(args *NodeLoggerArgs) (string, error) {
	if args.LogDest == "" {
		panic("LogDest must always explicitely be set.  No default value!")
	}
	path := fmt.Sprintf("%s/logs/%s_%s", args.ExpHome, args.Datestamp, args.LogDest)
	return path, nil
}
