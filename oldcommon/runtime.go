package common

type RuntimeNodeSpec2 struct {
	ExperimentRunSpec
	IterationSpec
}

// /main/b/loop1(x)/loop2(y)/task3 is specified by
// IerationSpec [x, y]
// Run specified by datestamp
//
type IterationSpec struct {
	FindName []SubmittableState
}
type ExperimentRunSpec struct {
	Datestamp string
}

// SubmittableState represents the state of an individual iteration of a loop
type SubmittableState int

// Possible states of the IterationState type
const (
	Unstarted SubmittableState = iota
	Init
	Running
	Finished
	Aborted
)

func (st SubmittableState) MarshalJSON() ([]byte, error) {
	var str string
	switch st {
	case Init:
		str = "init"
	case Running:
		str = "running"
	case Finished:
		str = "finished"
	case Aborted:
		str = "aborted"
	}
	return []byte("\"" + str + "\""), nil
}
