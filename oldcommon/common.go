package common

import (
	"fmt"
	"strings"
)

// LoopCoordinates represents a runtime coordinate specifying loop iterations along a path
type LoopCoordinates []int

// NodeIteration determining the runtime particularities of a node
type NodeIteration struct {
	ExpHome         string
	Datestamp       string
	NodePath        string
	LoopCoordinates LoopCoordinates
}

// ActionArgs represents the arguments for action functions
// of the maestro submittable interface
type ActionArgs struct {
	Datestamp string
	LoopArgs  LoopCoordinates
}

// String method for LoopArgs type
func (la LoopCoordinates) String() string {
	b := strings.Builder{}
	for _, coord := range la {
		b.WriteString(fmt.Sprintf("%d,", coord))
	}
	return b.String()
}
