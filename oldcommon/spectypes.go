package common

// Exp describes how to specify an experiment
type Exp struct {
	expHome string
}

// Run describes how to specify a run of an experiment
type Run struct {
	datestamp string
}

// Node describes how to specify a node of an experiment
type Node struct {
	Path string
}

// Iteration describes how to specify which iteration of
// a runtime node a request targets.  This describes the
// iteration state of all loops along the container path
// of the node.
type Iteration []int

// The full specification of a particular iteration of a
// a particular node in a particular run of an experiment
type RuntimeNodeSpec struct {
	exp  Exp
	run  Run
	node Node
	iter Iteration
}
