package visitors

import (
	"io/ioutil"
	"os"
	"strings"
	"testing"

	flownode "gitlab.science.gc.ca/phc001/gomaestro/flownode/nodes"
)

var expBase string = os.Getenv("PWD") + "/../../mockfiles/dot-suites"

func TestHtmlVisitor(t *testing.T) {
	expHome := expBase + "/philtest"
	tree, err := flownode.GetCompleteTree(expHome)
	if err != nil {
		t.Fatal(err)
	}

	v := HTMLRenderVisitor{}
	v.ExpHome = expHome
	v.XOffset = 250
	v.XSize = 150
	v.YOffset = 70
	v.YSize = 30
	resultBuilder := &strings.Builder{}
	v.Stdout = resultBuilder

	v.DrawHTMLPage(tree)

	// fmt.Println(resultBuilder.String())

	ioutil.WriteFile("wholeflow.html", []byte(resultBuilder.String()), 0644)
}

func TestHtmlBig(t *testing.T) {
	expHome := expBase + "/g0"
	tree, err := flownode.GetCompleteTree(expHome)
	if err != nil {
		t.Fatal(err)
	}

	v := HTMLRenderVisitor{}
	v.ExpHome = expHome
	v.XOffset = 200
	v.XSize = 100
	v.YOffset = 60
	v.YSize = 30
	resultBuilder := &strings.Builder{}
	v.Stdout = resultBuilder

	v.DrawHTMLPage(tree)

	// fmt.Println(resultBuilder.String())

	ioutil.WriteFile("wholeflowbig.html", []byte(resultBuilder.String()), 0644)
}
func TestHtmlSuperBig(t *testing.T) {
	expHome := expBase + "/g1_7.1.0"
	tree, err := flownode.GetCompleteTree(expHome)
	if err != nil {
		t.Fatalf("Error for exp: %s : %v", expHome, err)
	}

	v := HTMLRenderVisitor{}
	v.ExpHome = expHome
	v.XOffset = 200
	v.XSize = 100
	v.YOffset = 60
	v.YSize = 30
	resultBuilder := &strings.Builder{}
	v.Stdout = resultBuilder

	v.DrawHTMLPage(tree)

	// fmt.Println(resultBuilder.String())

	ioutil.WriteFile("wholeflowsuperbig.html", []byte(resultBuilder.String()), 0644)
}
