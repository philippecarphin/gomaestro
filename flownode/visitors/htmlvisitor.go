package visitors

import (
	"fmt"
	"io"

	flownode "gitlab.science.gc.ca/phc001/gomaestro/flownode/nodes"
	common "gitlab.science.gc.ca/phc001/gomaestro/oldcommon"
)

// HTMLRenderVisitor visits nodes in a tree of IFlowNode and produces HTML to draw the tree within an SVG element.
type HTMLRenderVisitor struct {
	common.ActionArgs
	Stdout    io.Writer
	ExpHome   string
	Datestamp string
	XSize     int
	YSize     int
	YOffset   int
	XOffset   int
}

// VisitTask writes the HTML for a Task node
func (h *HTMLRenderVisitor) VisitTask(t *flownode.Task) error {
	width := (3*h.XSize)/2
	height := h.YSize
	_, _ = fmt.Fprintf(h.Stdout, `<rect x="%d", y="%d", width="%d" height="%d" stroke="black" stroke-width="3" fill="blue"/>\n`, -width/2, -height/2, width, height)
	return nil
}

// VisitModule writes the HTML for a Module node
func (h *HTMLRenderVisitor) VisitModule(m *flownode.Module) error {
	width := 3 * h.XSize / 2
	height := 3 * h.YSize / 2
	_, _ = fmt.Fprintf(h.Stdout, "<rect x=\"%d\", y=\"%d\", width=\"%d\" height=\"%d\" stroke=\"black\" stroke-width=\"3\" fill=\"green\"/>\n", -width/2, -height/2, width, height)
	return nil
}

// DrawSubtree draws the subtree t at the requested position.
// The function returns the vertical and horizontal space of the drawn subtree
// in order to place the next subtree.
func (h *HTMLRenderVisitor) DrawSubtree(t flownode.IFlowNode, x int, dy int) (int, int, error) {
	// Set the position of the Subtree relative to its parent.
	_, _ = fmt.Fprintf(h.Stdout, "<g transform=\"translate(%d,%d)\">\n", x, dy)

	// Draw the children of the node
	XTotalSize := x + h.XOffset
	YTotalSize := 0
	endX := h.XOffset
	var toVisit []flownode.IFlowNode
	switch t.(type) {
	case *flownode.Switch:
		toVisit = t.Children() // the switch items
	default:
		toVisit = t.Submits()
	}
	for _, sub := range toVisit {
		// Line (0,0) -> (0,Y) -> (X,Y)
		_, _ = fmt.Fprintf(h.Stdout, `<path d="M%d %d L%d %d L%d %d" stroke-width="3" stroke="black" fill="none"/>`,
			0, 0, 0, YTotalSize, endX, YTotalSize)

		_, subtreeSizeY, err := h.DrawSubtree(sub, h.XOffset, YTotalSize)
		if err != nil {
			return 0, 0, err
		}

		YTotalSize += subtreeSizeY
	}

	// Draw the single node
	link := fmt.Sprintf("/nodeinfo?exp=%s&node=%s&datestamp=%s", h.ExpHome, t.SubmitsPath(), h.Datestamp)
	_, _ = fmt.Fprintf(h.Stdout, `<a href="%s" target="popup" onclick="window.open('%s','popup','width=600,height=600'); return false;">`,
		link, link)
	err := t.AcceptVisitor(h)
	if err != nil {
		return 0, 0, err
	}
	var textColor string
	switch t.(type) {
	case *flownode.NPassTask, *flownode.Loop:
		textColor = "black"
	default:
		textColor = "white"
	}

	fmt.Fprintf(h.Stdout,
		`<text x="%d" y="%d" fill="%s" text-anchor="middle" font-weight="bold" font-size="8" dominant-baseline="middle">%s</text>`,
		0, 0, textColor, t.Name())
	_, _ = fmt.Fprintf(h.Stdout, "</a>\n")

	_, _ = fmt.Fprint(h.Stdout, "</g>\n")
	if YTotalSize == 0 {
		YTotalSize = h.YOffset
	}

	return XTotalSize, YTotalSize, nil
}

// DrawHTMLPage Page draws a complete HTML Page containing an SVG element containing the rendered nodes of the tree t.
func (h *HTMLRenderVisitor) DrawHTMLPage(t flownode.IFlowNode) error {
	_, _ = fmt.Fprintln(h.Stdout, `<!DOCTYPE html> <html lang="en"> <head>
        <style>
        body {
            // background-image: url('http://web.science.gc.ca/~phc001/webflow.jpg');
            background-color:white;
          }
          </style>
          </head>
	    <body>
		<svg width="2000" height="16000" id="flowtree" fill="black">`)
	_, _, err := h.DrawSubtree(t, h.XOffset, h.YOffset)
	if err != nil {
		fmt.Println(err)
		return err
	}
	_, _ = fmt.Fprintln(h.Stdout, `</svg></body> </html>`)
	return nil
}

// VisitLoop visits an Loop node
func (h *HTMLRenderVisitor) VisitLoop(l *flownode.Loop) error {
	_, _ = fmt.Fprintf(h.Stdout, `
<ellipse cx="%d", cy="%d", rx="%d" ry="%d", fill="cyan", stroke="black", stroke-width="5"/>
`, 0, 0, h.XSize, h.YSize)
	return nil
}

// VisitSwitch visits an Switch node
func (h *HTMLRenderVisitor) VisitSwitch(s *flownode.Switch) error {
	const W = 30
	const w = 20
	const H = 10
	_, _ = fmt.Fprintf(h.Stdout, `
<path d="M%d %d L%d %d L%d %d L%d %dZ", fill="yellow", stroke="black", stroke-width="4"/>
`, -W, H, -w, -H, W, -H, w, H)
	return nil
}

// VisitSwitchItem visits an x node
func (h *HTMLRenderVisitor) VisitSwitchItem(s *flownode.SwitchItem) error {
	_, _ = fmt.Fprintf(h.Stdout, `
<circle cx="%d", cy="%d", r="%d", fill="grey", stroke="black", stroke-width="5"/>
`, 0, 0, h.XSize/4)
	return nil
}

// VisitFamily visits an x node
func (h *HTMLRenderVisitor) VisitFamily(f *flownode.Family) error {
	width := 8 * h.XSize / 7
	height := 10 * h.YSize / 7
	_, _ = fmt.Fprintf(h.Stdout, `<rect x="%d", y="%d", width="%d" height="%d" stroke="black" stroke-width="3" fill="magenta"/>\n`, -width/2, -height/2, width, height)
	return nil
}

// VisitForeach visits an x node
func (h *HTMLRenderVisitor) VisitForeach(f *flownode.Foreach) error {
	_, _ = fmt.Fprintf(h.Stdout, `
<circle cx="%d", cy="%d", r="%d", fill="red", stroke="black", stroke-width="5"/>
`, 0, 0, h.XSize/4)
	return nil
}

// VisitNPassTask visits an NPassTask node
func (h *HTMLRenderVisitor) VisitNPassTask(f *flownode.NPassTask) error {
	width := (3*h.XSize)/2
	height := h.YSize
	borderRadius := 5
// 	_, _ = fmt.Fprintf(h.Stdout, `
// <circle cx="%d", cy="%d", r="%d", fill="purple", stroke="black", stroke-width="5"/>
// `, 0, 0, h.XSize/4)
	_, _ = fmt.Fprintf(h.Stdout, `<rect x="%d", y="%d", width="%d" height="%d" stroke="black" stroke-width="3" fill="violet" rx="%dpx" ry="%dpx"/>\n`, -width/2, -height/2, width, height, borderRadius, 2*borderRadius)
	return nil
}

// NewInitVisitor returns a new InitVisitor
func NewInitVisitor() *HTMLRenderVisitor {
	return &HTMLRenderVisitor{}
}

func (h *HTMLRenderVisitor) VisitBase(b *flownode.NodeBase) error {
	_, _ = fmt.Fprintf(h.Stdout, `
<circle cx="%d", cy="%d", r="%d", fill="red", stroke="black", stroke-width="5"/>
`, 0, 0, h.XSize/4)
	return nil
}
