package flownode

import (
	common "gitlab.science.gc.ca/phc001/gomaestro/oldcommon"
)

// Loop represents a loop in maestro
type Loop struct {
	NodeBase
	IterStates       map[int]common.SubmittableState
	CurrentIteration int
}

// AcceptVisitor implements the iVisitable for the Loop node.
func (l *Loop) AcceptVisitor(v IVisitor) error {
	return v.VisitLoop(l)
}

// QueueStatus function for loop
func (l *Loop) QueueStatus(args common.ActionArgs) common.SubmittableState {
	for _, ch := range l.children {
		for i := 0; i < 4; i++ {
			args.LoopArgs = append(args.LoopArgs, i)
			if ch.Status(args) != common.Finished {
				return common.Running
			}
		}
	}
	return common.Finished
}
