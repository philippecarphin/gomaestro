package flownode

import (
	"fmt"

	common "gitlab.science.gc.ca/phc001/gomaestro/oldcommon"
	"gitlab.science.gc.ca/phc001/gomaestro/nodelogger"
)

// Begin for Task nodes signals maestro that a job submitted with ord_soumet
// has started running. This is done by inserting
//
// 		'mclient ... -s begin'
//
// at the start of the wrapped script.
func (t *Task) Begin(args common.ActionArgs) error {
	xFlowBegin(t, args)
	t.RuntimeState = common.Running
	return nil
}

func xFlowBegin(node IFlowNode, args common.ActionArgs) error {
	fmt.Printf("BEGIN Node %s\n", node.Path())

	la := nodeToLoggerArgs(node)
	la.MessageType = "begin"
	la.LogDest = "nodelog"
	nodelogger.NodeLogger(&la)

	return nil
}
