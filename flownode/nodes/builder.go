package flownode

import (
	"fmt"

	"gitlab.science.gc.ca/phc001/gomaestro/flowxml"
)

// We build a braph of *NodeBuilder that all have
type NodeBuilder struct {
	XMLSource       *flowxml.XMLFlowNode
	inner           NodeBase
	children        []*NodeBuilder
	submits         []*NodeBuilder
	Parent          *NodeBuilder
	nodeSubmits     []IFlowNode
	nodeChildren    []IFlowNode
	nodeSwitchItems []IFlowNode
	submitter       IFlowNode
	// Info to provide for building a node
	nodeType string
	NodeName string
	master   *TreeBuilder
}
type TreeBuilder struct {
	rootXml   *flowxml.XMLFlowNode
	rootFlow  IFlowNode
	originMap map[IFlowNode]*flowxml.XMLFlowNode
}

type Builder interface {
	AddChild(IFlowNode)
	AddSubmits(IFlowNode)
	// SetInfo(Info)
	SetParent(IFlowNode)
	SetName(string)
	Children() []*NodeBuilder
	Build() IFlowNode
}

// Children retuns th slice of NodeBuilder children of this builder
func (nb *NodeBuilder) Children() []*NodeBuilder { return nb.Children() }

// AddChild adds a node builder child
func (nb *NodeBuilder) AddChild(c *NodeBuilder) { nb.children = append(nb.children, c) }

// AddChildNode adds build IFlowNode to the builder's NodeChildren
func (nb *NodeBuilder) AddChildNode(c IFlowNode) { nb.nodeChildren = append(nb.nodeChildren, c) }

// AddSubmits adds a node builder in a submits list (not used I don't think)
func (nb *NodeBuilder) AddSubmits(s *NodeBuilder) { nb.submits = append(nb.children, s) }

// NewNodeBuilder returns an initialized NodeBuilder
func NewNodeBuilder(xml *flowxml.XMLFlowNode, master *TreeBuilder) *NodeBuilder {
	return &NodeBuilder{
		XMLSource:       xml,
		children:        make([]*NodeBuilder, 0),
		nodeSubmits:     make([]IFlowNode, 0),
		nodeChildren:    make([]IFlowNode, 0),
		nodeSwitchItems: make([]IFlowNode, 0),
		master:          master,
	}
}

// NewTreeBuilder returns an initialized tree builder
func NewTreeBuilder() *TreeBuilder {
	return &TreeBuilder{
		originMap: make(map[IFlowNode]*flowxml.XMLFlowNode),
	}
}

// Build returns a mostly built node.  The TreeBuilder still does some work.
// The builder does what it can to make the nodes as immutable as possible but
// It's hard to make all the attributes immutable because to do that, you need
// all of them at construction.  But because of that, the parent has to be built
// before we build the child if we want the child's .parent attrib to be constant.
// but to build the parent, we need all the children to be already built to make
// the .children attrib immutable.
// This chicken-and-egg situation makes it so that we have to set some attributes
// with unexported interface functions like setParent and setSubmitter and addSubmits
// (The .children and .submits are also mutually exclusive in that we can't make them
// both immutable by making them at construction with the builder)
func (nb *NodeBuilder) Build() (IFlowNode, error) {
	base := NodeBase{
		NodeBaseGraphStuff: NodeBaseGraphStuff{
			children: nb.nodeChildren,
			submits:  nb.nodeSubmits,
			// switchItems: nb.nodeSwitchItems,
		},
	}
	base.nodeBaseInfo.Name = nb.XMLSource.Name
	node := newNode(nb.XMLSource.XMLName.Local, &base)
	nb.master.originMap[node] = nb.XMLSource
	for _, c := range node.Children() {
		c.setParent(node)
	}
	return node, nil
}

func newNode(nodeType string, base *NodeBase) IFlowNode {
	switch nodeType {
	case "MODULE":
		return &Module{NodeBase: *base}
	case "TASK":
		return &Task{NodeBase: *base}
	case "NPASS_TASK":
		return &NPassTask{NodeBase: *base}
	case "LOOP":
		return &Loop{NodeBase: *base}
	case "SWITCH":
		return &Switch{NodeBase: *base}
	case "SWITCH_ITEM":
		return &SwitchItem{NodeBase: *base}
	case "FOREACH":
		return &Foreach{NodeBase: *base}
	case "FAMILY":
		return &Family{NodeBase: *base}
	default:
		panic(fmt.Errorf("UNSUPPORTED TYPE : %v", nodeType))
	}
}

// FindChildByName is part of the builder interface to set the submits of a node.
func FindChildByName(node IFlowNode, name string) IFlowNode {
	for _, candidate := range node.Children() {
		if candidate.Name() == name {
			return candidate
		}
	}
	return nil
}

func (tb *TreeBuilder) buildTree(root *flowxml.XMLFlowNode) (IFlowNode, error) {
	nb := NewNodeBuilder(root, tb)

	for _, xc := range nb.XMLSource.Children {
		child, err := tb.buildTree(xc)
		if err != nil {
			return nil, err
		}
		nb.AddChildNode(child)
	}

	return nb.Build()
}

func (t *TreeBuilder) resolveSubmits(node IFlowNode) error {

	xmlOrigin := t.originMap[node]

	var search IFlowNode
	switch node.(type) {
	case *Task, *NPassTask:
		search = node.Parent()
	default:
		search = node
	}

	if search == nil {
		return fmt.Errorf("resolveSubmits(): could search node is nil")
	}

	for _, xsub := range xmlOrigin.Submits {
		s := FindChildByName(search, xsub.SubName)
		if s == nil {
			return fmt.Errorf("could not find child of node %s (name=%s) with name %s", node.Path(), node.Name(),  xsub.SubName)
		}
		node.addSubmits(s)
		s.setSubmitter(node)
	}


	for _, ch := range node.Children() {
		err := t.resolveSubmits(ch)
		if err != nil {
			return err
		}
	}

	switch node.(type){
	case *Switch:
		for _, ch := range node.Children() {
			node.addSubmits(ch)
			ch.setSubmitter(node)
		}
	}

	return nil
}

func GetCompleteTree(expHome string) (IFlowNode, error) {
	rootXML, err := flowxml.ReadExperimentHome(expHome)
	if err != nil {
		return nil, err
	}

	b := NewTreeBuilder()

	// Create a tree of node builders
	root, err := b.buildTree(rootXML)
	if err != nil {
		return nil, err
	}

	// Resolve the submits but by going through the node builders.
	err = b.resolveSubmits(root)
	if err != nil {
		return nil, err
	}

	return root, err
}
