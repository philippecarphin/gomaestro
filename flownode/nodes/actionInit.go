package flownode

import (
	"fmt"

	"gitlab.science.gc.ca/phc001/gomaestro/nodelogger"
	common "gitlab.science.gc.ca/phc001/gomaestro/oldcommon"
)

// Init initializes states of the node
func initImpl(b IFlowNode, args common.ActionArgs) error {
	xFlowInit(b, args)
	for _, c := range b.Children() {
		err := c.Init(args)
		if err != nil {
			return err
		}
	}
	return nil
}

func xFlowInit(node IFlowNode, args common.ActionArgs) error {
	fmt.Printf("INIT Node %s\n", node.Path())
	la := nodeToLoggerArgs(node)
	la.MessageType = "init"
	la.LogDest = "nodelog"
	nodelogger.NodeLogger(&la)

	switch node.(type) {
	case *Module:
		la.LogDest = "toplog"
		nodelogger.NodeLogger(&la)
	default:
	}
	return nil
}

func nodeToLoggerArgs(node IFlowNode) nodelogger.NodeLoggerArgs {
	exp := node.Exp()

	if exp == nil {
		panic(fmt.Errorf("No exp pointer for node %s", node.Path()))
	}
	// TODO make this a method of submittableBase
	return nodelogger.NodeLoggerArgs{
		Datestamp: node.Exp().Datestamp,
		ExpHome:   node.Exp().Home,
		NodePath:  node.Path(),
	}
}
