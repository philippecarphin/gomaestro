package flownode

// SwitchItem is an item in a switch
type SwitchItem struct {
	NodeBase
}

// AcceptVisitor implements the iVisitable for the Family node.
func (si *SwitchItem) AcceptVisitor(v IVisitor) error {
	return v.VisitSwitchItem(si)
}

// Path for switch items.  They don't contribute to paths.
// The switch item taken depends on the context.
func (si *SwitchItem) Path() string {
	return si.parent.Path()
}

// ContainerPath for switch items.  They don't contribute to paths.
// The switch item taken depends on the context.
func (si *SwitchItem) ContainerPath() (string, error){
	cp, err := si.parent.ContainerPath()
	if err != nil {
		return "", err
	}
	return cp, nil
}
