package flownode

// Family is a submittable container
type Family struct {
	NodeBase
}

// AcceptVisitor implements the iVisitable for the Family node.
func (f *Family) AcceptVisitor(v IVisitor) error {
	return v.VisitFamily(f)
}
