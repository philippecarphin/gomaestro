package flownode

import(
	common "gitlab.science.gc.ca/phc001/gomaestro/oldcommon"
)
// Module is simply a container
type Module struct {
	NodeBase
}

// AcceptVisitor implements the iVisitable for the Module node.
func (m *Module) AcceptVisitor(v IVisitor) error {
	return v.VisitModule(m)
}

func (m *Module) Init(args common.ActionArgs) error {
	return initImpl(m, args)
}