package flownode

import common "gitlab.science.gc.ca/phc001/gomaestro/oldcommon"

// IFlowNode defines the interface of flow nodes
type IFlowNode interface {
	// The maestro node lives on a graph
	// IGraphNode

	// It has ways of notifying
	IMaestroNotifier
	// It can do queing system related actions
	IMaestroActor
	// It can tell us its state
	IMaestroQueryable
	// TODO : I want this to be an attribute set while parsing
	// and I don't think it has to be a function that behaves
	// differently based on the type
	// // It can tell us its container module
	// container() IFlowNode
	NodeStuff
	iVisitable
}

type NodeStuff interface {
	Path() string
	SubmitsPath() string
	ContainerPath() (string, error)
	Name() string
	Children() []IFlowNode
	Parent() IFlowNode
	Exp() *Exp
	SetExp(e *Exp)
	Submitter() IFlowNode
	Submits() []IFlowNode
	addSubmits(IFlowNode)
	setParent(IFlowNode)
	setSubmitter(IFlowNode)
}

// IMaestroQueryable are able to give us the status of iterations
// of itself
type IMaestroQueryable interface {
	Status(common.ActionArgs) common.SubmittableState
	IsComplete(common.ActionArgs) bool
}

// IMaestroNotifier enforces the requirement that nodes be able to notify the program of events.
type IMaestroNotifier interface {
	// Abort is called to signal maestro that the node has been aborted.
	Abort(common.ActionArgs) error
	// Begin is called to signal maestro that the node has started.
	Begin(common.ActionArgs) error
	// End is called to signal maestro that the node has ended.
	End(common.ActionArgs) error
}

// IMaestroActor defines the required actions that nodes must be able to perform
type IMaestroActor interface {
	// Init initializes internal states for the node.
	Init(common.ActionArgs) error
	// Submit jobs and children of this node that can be submitted
	Submit(common.ActionArgs) error
}
type iVisitable interface {
	AcceptVisitor(v IVisitor) error
}

// IVisitor is the interface for visiting nodes of the IFlowNode composite structure.
type IVisitor interface {
	VisitTask(t *Task) error
	VisitNPassTask(n *NPassTask) error
	VisitModule(a *Module) error
	VisitLoop(a *Loop) error
	VisitSwitch(a *Switch) error
	VisitSwitchItem(a *SwitchItem) error
	VisitFamily(a *Family) error
	VisitForeach(a *Foreach) error
	VisitBase(b *NodeBase) error
}
