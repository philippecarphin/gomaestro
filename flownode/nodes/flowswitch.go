package flownode

// Switch represents a switch in maestro
type Switch struct {
	NodeBase
	SwitchItems map[string]*SwitchItem
}

// AcceptVisitor implements the iVisitable for the Switch node.
func (s *Switch) AcceptVisitor(v IVisitor) error {
	return v.VisitSwitch(s)
}

// Evaluate is a stub for turning a switch into the content of one of its switch items.
func (s *Switch) Evaluate(datestamp string) *Family {
	// SwitchItem is like a Family, so an evaluated switch
	// would be pretty much a family
	// Except the problem is if replace the switch with

	f := Family{}
	// si = findSwitchItem()
	// f.SubmittableBase = si.SubmittableBase
	return &f
}
