package flownode

import (
	"fmt"

	"gitlab.science.gc.ca/phc001/gomaestro/nodelogger"
	common "gitlab.science.gc.ca/phc001/gomaestro/oldcommon"
)

func endLogMessage(n IFlowNode, args common.ActionArgs) error {
	// fmt.Printf("Ending NODE %s with %d SUBMITS\n", n.GetName(), len(n.GetRealSubmits()))
	return xFlowEnd(n, args)
}

func launchNodeSubmits(n IFlowNode, args common.ActionArgs) error {
	for _, realSub := range n.Submits() {
		err := realSub.Submit(args)
		if err != nil {
			return fmt.Errorf("t.End() : Could not submit SUBMITS '%s' of TASK '%s'", realSub.Name(), n.Name())
		}
	}
	return nil
}

func endTaskNodesImpl(n IFlowNode, args common.ActionArgs) error {
	endLogMessage(n, args)
	updateIterationState(n, args)
	launchNodeSubmits(n, args)

	if n.Parent() != nil {
		return n.Parent().End(args)
	}
	return nil
}

func updateIterationState(n IFlowNode, args common.ActionArgs) error {
	return nil
}

// End any task (implemented in nodeBase and added the endLogMessage call)
// func (sb *NodeBase) End(args common.ActionArgs) error {
// 	return endLogMessage(sb, args)
// }

// End a task
func (t *Task) End(args common.ActionArgs) error {
	t.RuntimeState = common.Finished

	// d := t.Exp.ExpHome + "/sequencing/status/" + t.Exp.Datestamp + "0000/" + t.Parent.GetName()
	// os.MkdirAll(d, 0755)
	// file := d + "/" + t.Name + ".end"
	// _, _ = os.Create(file)

	return endTaskNodesImpl(t, args)
}

// End an NPASSTASK (Same as TAsk)
func (t *NPassTask) End(args common.ActionArgs) error {
	return t.Parent().End(args)
}

// IsComplete returns the completeness status of a task by checking the presence of a file.
func (t *Task) IsComplete(args common.ActionArgs) bool {
	// TODO Look for file {expHome}/sequencing/{datestamp}/{nodePath}_{extension}.end
	// Return whether or not it exists
	return t.RuntimeState == common.Finished
}

// IsComplete for submittableContainer propagates isComplete to children
func (sc *NodeBase) IsComplete(args common.ActionArgs) bool {
	for _, ch := range sc.children {
		if !ch.IsComplete(args) {
			return false
		}
	}
	return true
}

// End for loop nodes : If current iteration is complete, submit next iteration
func (l *Loop) End(args common.ActionArgs) error {
	l.CurrentIteration = 8

	if !l.iterationIsComplete(l.CurrentIteration) {
		return nil
	}

	l.IterStates[l.CurrentIteration] = common.Finished

	if !l.isLastIteration(l.CurrentIteration) {
		return l.parent.End(args)
	}

	nextIteration := l.CurrentIteration + 1
	newLoopArgs := make(common.LoopCoordinates, 0)
	newLoopArgs = append(newLoopArgs, nextIteration)

	return l.Submit(args)
}

func (l *Loop) iterationIsComplete(iteration int) bool {
	return true
}

func (l *Loop) isLastIteration(iteration int) bool {
	return true
}

// End method for Module nodes
func (m *Module) End(args common.ActionArgs) error {
	fmt.Printf("Module End function (%s)\n", m.Path())
	if !m.IsComplete(args) {
		fmt.Printf("... module is not complete\n")
		return nil
	}
	fmt.Printf("... module complete!!!\n")

	xFlowEnd(m, args)

	switch m.parent {
	case nil:
		m.Exp().AbortCh <- nil
		return nil
	default:
		return m.parent.End(args)
	}
}

func xFlowEnd(node IFlowNode, args common.ActionArgs) error {

	la := nodeToLoggerArgs(node)
	la.Message = "YAY"
	la.MessageType = "end"
	la.LogDest = "nodelog"
	nodelogger.NodeLogger(&la)
	switch node.(type) {
	case *Module:
		la.LogDest = "toplog"
		return nodelogger.NodeLogger(&la)
	default:
		return nil
	}
}
