package flownode

import (
	"os"
	"testing"
)

var expBase string = os.Getenv("PWD") + "/../../mockfiles/dot-suites/"

func TestBuildTree(t *testing.T) {

	root, err := GetCompleteTree(expBase + "/philtest")
	if err != nil {
		t.Fatal(err)
	}

	if root == nil {
		t.Fatalf("root is nil")
	}
}

func TestBigExperiment(t *testing.T) {
	expHome := expBase + "/g1_7.1.0"
	root, err := GetCompleteTree(expHome)
	if err != nil {
		t.Fatalf("Error for exp %s : %v", expHome, err)
	}

	if root == nil {
		t.Fatalf("root is nil")
	}
}
