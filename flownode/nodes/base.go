package flownode

import (
	"fmt"
	common "gitlab.science.gc.ca/phc001/gomaestro/oldcommon"
)

// Info BASIC_TYPE defines the way of referring to a node
// in the graph
type nodeBaseInfo struct {
	Name      string
	Datestamp string
	ExpHome   string
}

// NodeBase is the type defining the graph structure of maestro experiments
// Note two attributes submits and children, these describe two simultaneous
// graphs with the same set of nodes.
type NodeBase struct {
	nodeBaseInfo
	NodeBaseGraphStuff
	NodeBaseMaestroStuff
}

// NodeBaseGraphStuff is the graph part of maestro nodes.
type NodeBaseGraphStuff struct {
	submits   []IFlowNode
	parent    IFlowNode
	children  []IFlowNode
	submitter IFlowNode
	status    common.SubmittableState
	exp       *Exp
}

// NodeBaseMaestroStuff is a type to hold maestro related information.
type NodeBaseMaestroStuff struct {
}

// setParent is part of the builder interface to set the parent of the node
func (b *NodeBase) setParent(parent IFlowNode) { b.parent = parent }

// setParent is part of the builder interface to set the parent of the node
func (b *NodeBase) setSubmitter(submitter IFlowNode) { b.submitter = submitter }

// Children returns the children of a node.
func (b *NodeBase) Children() []IFlowNode { return b.children }

// Parent returns the Parent of a node.
func (b *NodeBase) Parent() IFlowNode { return b.NodeBaseGraphStuff.parent }

// Exp returns a reference to the experiment that owns this node
func (b *NodeBase) Exp() *Exp { return b.exp }

// Exp returns a reference to the experiment that owns this node
func (b *NodeBase) SetExp(e *Exp) { b.exp = e }

// Submitter returns the Parent of a node.
func (b *NodeBase) Submitter() IFlowNode { return b.NodeBaseGraphStuff.submitter }

// Submits returns the submits of a node.  This is what defines the sequencing of jobs.
func (b *NodeBase) Submits() []IFlowNode { return b.NodeBaseGraphStuff.submits }

// Path is called to get the path from the root to the node by using the Parent relationship (this corresponds to containment in the flow.xml files).
func (b *NodeBase) Path() string {
	if b.parent == nil {
		return "/" + b.Name()
	}

	return b.parent.Path() + "/" + b.Name()
}

// SubmitsPath returns the path of a node on the graph of submits.
func (b *NodeBase) SubmitsPath() string {
	if b.submitter == nil {
		return "/" + b.Name()
	}

	return b.submitter.SubmitsPath() + "/" + b.Name()
}

// AddSubmits is part of the builder interface to append submits to the submits list.
func (b *NodeBase) addSubmits(child IFlowNode) {
	b.submits = append(b.submits, child)
}

func (b *NodeBase) Init(args common.ActionArgs) error {
	return initImpl(b, args)
}

// Begin signals to maestro that this node has begun (maestro will take action accordingly)
func (b *NodeBase) Begin(common.ActionArgs) error {
	// nodelogger -s begin
	return nil
}

// End signals to maestro that this node has ended (maestro will take action accordingly)
func (b *NodeBase) End(args common.ActionArgs) error {
	fmt.Printf("NodeBase::End() (b = %s))\n", b.Path())
	for _, c := range b.children {
		if c.Status(args) == common.Finished {
			return nil
		}
	}

	err := b.parent.End(args)
	if err != nil {
		return err
	}

	endLogMessage(b, args)
	return nil
}

// Abort is called to signal that the node has been aborted.
func (b *NodeBase) Abort(common.ActionArgs) error { return nil }

// Status is called to get the status of the node.
func (b *NodeBase) Status(common.ActionArgs) common.SubmittableState { return b.status }

// Name is part of the IDontKnowWhat interface to get the name of a node (Needed for polymorphic construction of path)
func (b *NodeBase) Name() string { return b.nodeBaseInfo.Name }

// ContainerPath is like Path() except that tasks don't add anything to the ContainerPath()
func (b *NodeBase) ContainerPath() (string, error) {
	if b.submitter == nil {
		return "/" + b.Name(), nil
	}

	cp, err := b.submitter.ContainerPath()
	if err != nil {
		return "", err
	}
	return cp + "/" + b.Name(), nil
}

// ContainerPath is the function with type switch version of the
// IFlowNode ContainerPath() implementation.
func ContainerPath(node IFlowNode) (string, error) {
	if node.Submitter() == nil {
		return "/" + node.Name(), nil
	}

	switch node.(type) {
	case *Task, *NPassTask:
		return node.Submitter().ContainerPath()
	default:
		cp, err := node.Submitter().ContainerPath()
		if err != nil {
			return "", err
		}
		return cp + "/" + node.Name(), nil
	}
}

// AcceptVisitor implements the iVisitable for the Task node.
func (b *NodeBase) AcceptVisitor(v IVisitor) error {
	return v.VisitBase(b)
}
