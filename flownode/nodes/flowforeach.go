package flownode

// Foreach is a container
type Foreach struct {
	NodeBase
}

// AcceptVisitor implements the iVisitable for the Foreach node.
func (f *Foreach) AcceptVisitor(v IVisitor) error {
	return v.VisitForeach(f)
}
