package flownode

import (
	"fmt"

	"github.com/google/uuid"
	common "gitlab.science.gc.ca/phc001/gomaestro/oldcommon"
)

var experiments map[string]*Exp

type Exp struct {
	EntryModule IFlowNode
	Home        string
	Datestamp   string
	Token       string
	AbortCh     chan error
}

func init() {
	experiments = make(map[string]*Exp)
}

/*
func StartExperiment(expHome string, datestamp string) (*flowvisitor.Experiment, error) {

	exp, err := xml.ParseExperiment(expHome, datestamp)
	if err != nil {
		return nil, err
	}

	actionArgs := common.ActionArgs{Datestamp: datestamp}
	err = exp.EntryModule.Init(actionArgs)
	if err != nil {
		return nil, fmt.Errorf("Could not begin experiment: %v", err)
	}

	err = exp.EntryModule.Submit(actionArgs)
	if err != nil {
		return nil, fmt.Errorf("Could not submit experiment : %v", err)
	}
	return exp, nil
}
*/
func (e *Exp) Start() error {
	actionArgs := common.ActionArgs{Datestamp: e.Datestamp}
	err := e.EntryModule.Init(actionArgs)
	if err != nil {
		return err
	}

	// Init entry module (which propagates)
	// - Init methods for nodes
	// - Submit methods for notde (especially tasks)
	// - Begin, End, Abort signal functions
	// - Ensure nodelogger messages and status file creation/deletion
	return nil
}

func CreateExpFromPath(expHome string, datestamp string) (*Exp, error) {
	root, err := GetCompleteTree(expHome)
	if err != nil {
		return nil, fmt.Errorf("Failed to load experiment %s : %v", expHome, err)
	}

	token, err := createRuntimeToken()
	if err != nil {
		return nil, fmt.Errorf("could not generate unique token for exp %s : %v", expHome, err)
	}
	exp := &Exp{EntryModule: root, Home: expHome, Datestamp: datestamp, Token: token}

	exp.assignExpReference(root)

	err = registerExp(exp)
	if err != nil {
		return nil, fmt.Errorf("Could not register experiment %s with token %s: %v", expHome, token, err)
	}

	return exp, nil
}

func createRuntimeToken() (string, error) {

	uuid, err := uuid.NewUUID()
	if err != nil {
		return "", err
	}
	return uuid.String(), nil
}

func (e *Exp) assignExpReference(n IFlowNode) {
	n.SetExp(e)
	for _, c := range n.Children() {
		e.assignExpReference(c)
	}
}

func registerExp(exp *Exp) error {
	experiments[exp.Token] = exp
	fmt.Printf("Registered exp %s with token %s\n", exp.Home, exp.Token)
	return nil
}

func GetExperimentFromToken(token string) (*Exp, error) {
	exp, ok := experiments[token]

	if !ok {
		return nil, fmt.Errorf("No experiment registered with token %s", token)
	}

	fmt.Printf("Successfully got experiment registered with token %s\n", token)

	return exp, nil
}
