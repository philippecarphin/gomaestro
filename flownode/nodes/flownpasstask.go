package flownode

// NPassTask is a task that is donne n passes
type NPassTask struct {
	NodeBase
}

// AcceptVisitor implements the iVisitable for the NPassTask node.
func (n *NPassTask) AcceptVisitor(v IVisitor) error {
	return v.VisitNPassTask(n)
}
