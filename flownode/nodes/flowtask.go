package flownode

import (
	"gitlab.science.gc.ca/phc001/gomaestro/oldcommon"
)

// Task represents a task in maestro
type Task struct {
	NodeBase
	RuntimeState common.SubmittableState
}

// AcceptVisitor implements the iVisitable for the Task node.
func (t *Task) AcceptVisitor(v IVisitor) error {
	return v.VisitTask(t)
}

// ContainerPath goes up the submits graph but does not add anything.
func (t *Task) ContainerPath() (string, error) {
	return t.submitter.ContainerPath()
}

func (t *Task) Init(args common.ActionArgs) error {

	// d := t.Exp().Home + "/sequencing/status/" + t.Exp().Datestamp + "0000/" + t.parent.Name()
	// file := d + "/" + t.Name() + ".end"
	// _, err := os.Stat(file)
	// if err != nil && os.IsNotExist(err) {
	// 	return nil
	// }

	// err = os.Remove(file)
	// if err != nil {
	// 	panic(err)
	// }

	return initImpl(t, args)
}
