package flownode

import (
	"fmt"
	"os"
	"os/exec"
	"strings"

	"gitlab.science.gc.ca/phc001/gomaestro/nodelogger"
	common "gitlab.science.gc.ca/phc001/gomaestro/oldcommon"
)

// func (sb *NodeBase) Submit(args common.ActionArgs) error {
// 	panic(fmt.Errorf("Should never be called, I think the fact that I need this method is an indication that something is wrong with my design"))
// }
//

// Submit a task : Look up the SUBMITS of the task will be triggered when
// this task ends
// TODO :
// - Create task begin file
// - Save script in specific location
// - submit using ord_soumet instead of running with bash
func (t *Task) Submit(args common.ActionArgs) error {

	// From C code about the file to create
	// memset(filename, '\0', sizeof filename);
	// sprintf(filename, "%s/%s/%s.submit", _nodeDataPtr->workdir,
	// 		_nodeDataPtr->datestamp, extName);

	// memset(workdir, '\0', sizeof workdir);
	// sprintf(workdir, "%s/sequencing/status", _exp_home);
	// SeqNode_setWorkdir(nodeDataPtr, workdir);

	// fmt.Printf("SUBMITTING TASK : %s (loopArgs = %v)\n", t.name, loopArgs)
	execPath := os.Getenv("HOME") + "/go/bin/mclient"
	// Look for thing.tsk
	sb := strings.Builder{}
	sb.WriteString("#!/bin/bash\n")
	// - EXPORT VARIABLES
	fmt.Fprintf(&sb, "export SEQ_EXP_HOME=%s\n", t.Exp().Home)
	fmt.Fprintf(&sb, "export SEQ_EXEC_PATH=%s\n", execPath)
	// - TRAP 'SIGNAL ABORT' (SIGABRT and friends)
	fmt.Fprintf(&sb, "trap 'curl -X POST http://localhost:8008/signal?token=%s&signal=%s&node=%s' ABRT\n", t.Exp().Token, "abort", t.Name())
	// - SIGNAL STARTED
	fmt.Fprintf(&sb, "t=\"$(bc -l <<< \"$(( 200 + ($RANDOM %% 600) )) / 100\")\"\n")
	fmt.Fprintf(&sb, "echo \"TASK{%s} : Waiting for submitted job to start (sleep $t) ...\"\n", t.Name())
	fmt.Fprintf(&sb, "sleep $t\n")
	fmt.Fprintf(&sb, "echo \"TASK{%s} : ... JOB HAS COME OUT OF THE QUEUE AND IS RUNNING \"\n", t.Name())
	begin := fmt.Sprintf("curl -X POST 'http://localhost:8008/signal?token=%s&signal=%s&node=%s'", t.Exp().Token, "begin", t.Path())
	fmt.Fprintf(&sb, "echo 'TASK{%s} : %s'\n", t.Name(), begin)
	fmt.Fprintf(&sb, "%s\n", begin)
	// - CALL SCRIPT
	fmt.Fprintf(&sb, "t=\"$(bc -l <<< \"$(( 400 + ($RANDOM %% 100) )) / 100\")\"\n")
	fmt.Fprintf(&sb, "echo \"TASK{%s} : TASK IS RUNNING (sleep $t) ...\"\n", t.Name())
	fmt.Fprintf(&sb, "sleep $t\n")
	fmt.Fprintf(&sb, "echo \"TASK{%s} : ... FINISHED \"\n", t.Name())
	// - SIGNAL FINISHED
	fmt.Fprintf(&sb, "echo 'TASK{%s} : curl ...signal=end'\n", t.Name())
	end := fmt.Sprintf("curl -X POST 'http://localhost:8008/signal?token=%s&signal=%s&node=%s'", t.Exp().Token, "end", t.Path())
	fmt.Fprintf(&sb, "echo 'TASK{%s} : %s'\n", t.Name(), end)
	fmt.Fprintf(&sb, "%s\n", end)

	cmd := exec.Command("/bin/bash")
	cmd.Stdin = strings.NewReader(sb.String())
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	la := nodeToLoggerArgs(t)
	la.Message = "YAY"
	la.MessageType = "submit"
	la.LogDest = "nodelog"
	nodelogger.NodeLogger(&la)

	return cmd.Start()
}

// Submit for NPASSTASK (same as for Task)
func (t *NPassTask) Submit(args common.ActionArgs) error {
	// fmt.Printf("SUBMITTING NPASSTASK : %s\n", t.name)
	execPath := os.Getenv("HOME") + "/go/bin/mclient"
	cmdPrefix := fmt.Sprintf("%s -e %s -n %s -d %s -l '%s'", execPath, t.Exp().Home, t.Path(), args.Datestamp, args.LoopArgs)
	// Look for thing.tsk
	sb := strings.Builder{}
	sb.WriteString("#!/bin/bash\n")

	// Wrap in script
	// - add an export
	// there should be some struct experiment and all nodes have a
	// reference to that struct t.exp and you use t.exp.home
	// and t.exp.datestamp.
	fmt.Fprintf(&sb, "export SEQ_EXP_HOME=%s\n", t.Exp().Home)
	// fmt.Fprintf(&sb, "echo export SEQ_EXEC_PATH=%s\n", execPath)
	fmt.Fprintf(&sb, "export SEQ_EXEC_PATH=%s\n", execPath)
	// - trap 'SIGNAL ABORT' (SIGABRT and friends)
	fmt.Fprintf(&sb, "trap '%s -s abort' ABRT\n", cmdPrefix)
	// - SIGNAL STARTED
	fmt.Fprintf(&sb, "%s -s started\n", cmdPrefix)
	// - call the script
	// fmt.Fprintf(&sb, "echo 'Task name %v, loopargs %#v, datestamp %s'\n", t.name, loopArgs, datestamp)
	// fmt.Fprintf(&sb, "echo 'sleep $(($RANDOM %% 5))'\n")
	fmt.Fprintf(&sb, "sleep $(bc -l <<< $(( ($RANDOM %% 500)))/100)\n")
	// - SIGNAL FINISHED
	fmt.Fprintf(&sb, "echo '%s -s end'\n", cmdPrefix)
	fmt.Fprintf(&sb, "%s -s end\n", cmdPrefix)

	cmd := exec.Command("/bin/bash")
	cmd.Stdin = strings.NewReader(sb.String())
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	return cmd.Start()
}

// Submit for submittableContainer looks in the Submits and submits the children that are referred to by its submits.
func (sc *NodeBase) Submit(args common.ActionArgs) error {
	for _, realSub := range sc.Submits() {
		err := realSub.Submit(args)
		if err != nil {
			return fmt.Errorf("t.End() : Could not submit SUBMITS '%s' of CONTAINER '%s'", realSub.Name(), sc.Name())
		}
	}
	return nil
}

// Submit a switch
func (s *Switch) Submit(args common.ActionArgs) error {
	datestampHour := args.Datestamp[8:10]
	defer func() {
		err := recover()
		if err != nil {
			panic(fmt.Errorf("No SWITCH_ITEM found with datestampHour=%s", datestampHour))
		}
	}()
	return s.SwitchItems[datestampHour].Submit(args)
}
