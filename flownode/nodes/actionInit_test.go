package flownode

import (
	"testing"

	common "gitlab.science.gc.ca/phc001/gomaestro/oldcommon"
)

func TestInit(t *testing.T) {

	exp, err := CreateExpFromPath(expBase + "/philtest", "2021032109430000")
	if err != nil {
		t.Fatal(err)
	}

	if exp == nil {
		t.Fatalf("root is nil")
	}

	err = exp.EntryModule.Init(common.ActionArgs{})
	if err != nil {
		t.Fatal(err)
	}

	err = exp.EntryModule.Submit(common.ActionArgs{})
	if err != nil {
		t.Fatal(err)
	}
}
