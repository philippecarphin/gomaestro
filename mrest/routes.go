package mrest

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"strings"

	"github.com/gorilla/mux"
	flownode "gitlab.science.gc.ca/phc001/gomaestro/flownode/nodes"
	"gitlab.science.gc.ca/phc001/gomaestro/flownode/visitors"
	common "gitlab.science.gc.ca/phc001/gomaestro/oldcommon"
)

type ExpServer struct {
	Dir  string
	Port int
}

// Args of the maestro api functions
type Args struct {
	common.NodeIteration
	Signal string
	Token  string
}

func (e *ExpServer) DemoServeExperiments() {
	r := mux.NewRouter()
	r.HandleFunc("/demo", e.DemoGetExperiment).Methods("GET")
	r.HandleFunc("/", e.DemoGetExperimentList).Methods("GET")
	r.PathPrefix("/list").HandlerFunc(e.GetExperimentList).Methods("GET")
	r.HandleFunc("/exptree/", e.GetExpByPath).Methods("GET")
	r.PathPrefix("/exp").HandlerFunc(e.GetExpByPath).Methods("GET")
	r.PathPrefix("/nodeinfo").HandlerFunc(e.nodeinfoHandler).Methods("GET")

	staticDir := os.Getenv("PWD") + "/../../static"
	prefix := "/static"
	fs := http.FileServer(http.Dir(staticDir))
	prefixHandler := http.StripPrefix(prefix, fs)
	r.PathPrefix(prefix).Handler(prefixHandler)

	r.PathPrefix("/start").HandlerFunc(e.StartExperiment).Methods("POST")
	r.PathPrefix("/signal").HandlerFunc(e.HandleSignal).Methods("POST")

	listener, err := net.Listen("tcp", ":8008")
	if err != nil {
		panic(err)
	}

	e.Port = listener.Addr().(*net.TCPAddr).Port

	fmt.Printf("Listening on port %d\n", e.Port)
	log.Fatal(http.Serve(listener, r))
}
func getChild(cur flownode.IFlowNode, w string) flownode.IFlowNode {
	for _, c := range cur.Children() {
		if c.Name() == w {
			return c
		}
	}
	return nil
}

// ParsePath returns a list of nodes created from the path string and the experiment home
func ParsePath(path string, exp *flownode.Exp) ([]flownode.IFlowNode, error) {

	words := strings.Split(strings.Trim(path, "/"), "/")
	if exp.EntryModule.Name() != words[0] {
		return nil, fmt.Errorf("First token '%s' in path '%s' does not match namt '%s' of entryModule", words[0], path, exp.EntryModule.Name())
	}

	var cur flownode.IFlowNode = exp.EntryModule
	pathList := make([]flownode.IFlowNode, 0)
	pathList = append(pathList, cur)
	for _, w := range words[1:] {
		node := getChild(cur, w)
		if node == nil {
			panic(fmt.Errorf("Could not find child named %s in children of %s", w, cur.Name()))
		}

		pathList = append(pathList, node)
		cur = node
	}

	return pathList, nil
}

func (e *ExpServer) HandleSignal(w http.ResponseWriter, r *http.Request) {
	/* Curl command :
	curl -X POST \
	  'http://localhost:8008/signal?token=467defe8-89ab-11eb-b4cb-38c9862e0be6' \
	  -H 'Postman-Token: 377f5795-2907-4c60-87d2-5c2d929cc303' \
	  -H 'cache-control: no-cache'
	And token is the token set at load time.*/
	fmt.Printf("HANDLE SIGNAL\n")
	q := r.URL.Query()
	expHome := q.Get("exp")
	nodePath := q.Get("node")
	token := q.Get("token")
	signal := q.Get("signal")
	fmt.Printf("Signal Experiment : expHome -> %s\n", expHome)
	fmt.Printf("Signal Experiment : nodePath -> %s\n", nodePath)
	fmt.Printf("Signal Experiment : token -> %s\n", token)
	fmt.Printf("Signal Experiment : signal -> %s\n", signal)

	exp, err := flownode.GetExperimentFromToken(token)
	if err != nil {
		msg := fmt.Sprintf("Could not get experiment with token %s : %v", token, err)
		fmt.Fprint(w, msg)
		fmt.Println(msg)
	}
	args := Args{}
	// fmt.Printf("Maestro RPC : args : %+v\n", args)
	pathList, err := ParsePath(nodePath, exp)
	if err != nil {
		fmt.Println(err)
	}

	node := pathList[len(pathList)-1]

	actionArgs := common.ActionArgs{Datestamp: args.Datestamp, LoopArgs: args.LoopCoordinates}
	switch signal {
	case "submit":
		err = node.Submit(actionArgs)
	case "end":
		err = node.End(actionArgs)
	case "begin":
		err = node.Begin(actionArgs)
	case "init":
		err = node.Init(actionArgs)
	default:
		err = fmt.Errorf("Only know signal submit (args : %v)", args.Signal)
	}
	if err != nil {
		fmt.Println(err)
	}
}

func (e *ExpServer) StartExperiment(w http.ResponseWriter, r *http.Request) {
	/* cURL command :
	curl -X POST \
	  'http://localhost:8002/start?exp=<..>/mockfiles/dot-suites/g1_7.1.0&datestamp=202103200628&node=/main' \
	  -H 'Postman-Token: 9655d4bd-3195-4208-89ae-2c717e5c46e8' \
	  -H 'cache-control: no-cache'
	*/
	q := r.URL.Query()
	expHome := q.Get("exp")
	nodePath := q.Get("node")
	datestamp := q.Get("datestamp")

	fmt.Printf("StartExperiment : expHome -> %s\n", expHome)
	fmt.Printf("StartExperiment : datestamp -> %s\n", datestamp)
	fmt.Printf("StartExperiment : nodePath -> %s\n", nodePath)

	// fmt.Fprintf(w, "StartExperiment : expHome -> %s\n", expHome)
	// fmt.Fprintf(w, "StartExperiment : datestamp -> %s\n", datestamp)
	// fmt.Fprintf(w, "StartExperiment : nodePath -> %s\n", nodePath)
	exp, err := flownode.CreateExpFromPath(expHome, datestamp)
	if err != nil {
		fmt.Println(err)
	}
	exp.Start()
	exp.EntryModule.Init(common.ActionArgs{})
	exp.EntryModule.Submit(common.ActionArgs{})
}

func (e *ExpServer) GetExpByPath(w http.ResponseWriter, r *http.Request) {
	h := w.Header()
	h.Set("Content-Type", "text/html")
	datestamp := r.URL.Query().Get("datestamp")

	fmt.Printf("GetExpByPath : r.URL.Path() -> %s\n", r.URL.Path)
	expHome := strings.TrimPrefix(r.URL.Path, "/exp")
	fmt.Printf("TrimmedPath : expHome -> %s\n", expHome)
	tree, err := flownode.GetCompleteTree(expHome)
	if err != nil {
		fmt.Println(err)
		return
	}

	v := visitors.HTMLRenderVisitor{}
	v.Datestamp = datestamp
	v.ExpHome = expHome
	v.XOffset = 100
	v.XSize = 50
	v.YOffset = 40
	v.YSize = 15
	v.Stdout = w

	v.DrawHTMLPage(tree)
}

func (e *ExpServer) DemoGetExperiment(w http.ResponseWriter, r *http.Request) {
	h := w.Header()
	h.Set("Content-Type", "text/html")

	fmt.Printf("Request : r.URL.Path() -> %s\n", r.URL.Path)
	expHome := "/fs/homeu1/eccc/cmd/cmds/mve000/.suites/g1_7.1.0/"
	tree, err := flownode.GetCompleteTree(expHome)
	if err != nil {
		fmt.Println(err)
		return
	}

	v := visitors.HTMLRenderVisitor{}
	v.ExpHome = expHome
	v.XOffset = 200
	v.XSize = 100
	v.YOffset = 60
	v.YSize = 30
	v.Stdout = w

	v.DrawHTMLPage(tree)
}
func (e *ExpServer) DemoGetExperimentList(w http.ResponseWriter, r *http.Request) {
	e.GetExperimentList(w, r)
}
func (e *ExpServer) GetExperimentList(w http.ResponseWriter, r *http.Request) {
	fmt.Println(r.URL.Path)

	fmt.Printf("Request : r.URL.Path() -> %s\n", r.URL.Path)
	dir := strings.TrimPrefix(r.URL.Path, "/list")
	if dir == "/" || dir == "" {
		dir = e.Dir
	}

	files, err := ioutil.ReadDir(dir)
	if err != nil {
		fmt.Println(err)
	}

	h := w.Header()
	h.Set("Content-Type", "text/html")
	w.Write([]byte(`<!DOCTYPE html> <html lang="en"> <head> </head> <body>`))
	// TODO propose many datestamps?
	// or give this query a parameter for the datestamp and it will list
	// all experiments that have a run for that date.
	for i, f := range files {
		if ok, _ := isExperiment(dir + "/" + f.Name()); ok {
			url := fmt.Sprintf("/exp/%s/%s/", dir, f.Name())
			path := fmt.Sprintf("%s/%s", dir, f.Name())
			fmt.Fprintf(w, `<li><a href="%s">%d: %s</a></div>`, url, i, path)
			fmt.Fprintf(w, `</li>`)
		}
	}
	w.Write([]byte(`</body></html>`))
}

func isExperiment(dir string) (bool, error) {
	_, err := os.Stat(dir + "/EntryModule")
	if os.IsNotExist(err) {
		return false, err
	}

	return err == nil, nil
}

func (e *ExpServer) nodeinfoHandler(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()
	expHome := q.Get("exp")
	nodePath := q.Get("node")
	datestamp := q.Get("datestamp")

	root, err := flownode.GetCompleteTree(expHome)
	if err != nil {
		fmt.Println(err)
		fmt.Fprintf(w, `<p style="color:red">%v</p>`, err)
		return
	}

	node, err := findNodeByPath(root, nodePath)
	if err != nil {
		fmt.Fprintf(w, `<p style="color:red">%v</p>`, err)
		fmt.Println(err)
		return
	}

	h := w.Header()
	h.Set("Content-Type", "text/html")

	nodeinfoHTMLWriter(w, node, expHome, datestamp)
}

func nodeinfoHTMLWriter(w io.Writer, node flownode.IFlowNode, expHome string, datestamp string) error {

	fmt.Fprintf(w, "<h1>%s</h1>", node.Path())

	fmt.Fprintf(w, "<table>")

	cp, err := node.ContainerPath()
	if err != nil {
		return err
	}
	fmt.Fprintf(w, "<tr> <td>node.ContainerPath()</td> <td>%s</td> </tr>", cp)
	fmt.Fprintf(w, "<tr> <td>node.SubmitsPath()</td> <td>%s</td> </tr>", node.SubmitsPath())

	var p string
	switch node.Parent() {
	default:
		p = node.Parent().Path()
	case nil:
		p = "Root node (no parent)"
	}
	fmt.Fprintf(w, "<tr> <td>node.Parent().Path()</td> <td>%s</td> </tr>", p)

	for i, sub := range node.Submits() {
		fmt.Fprintf(w, "<tr><td>node.Submits()[%d].Path()</td> <td>%s</td> </tr>", i, sub.Path())
	}
	for i, ch := range node.Children() {
		fmt.Fprintf(w, "<tr><td>node.Children()[%d].Path()</td> <td>%s</td> </tr>", i, ch.Path())
	}
	sequencingDir := expHome + "/sequencing/status/" + datestamp + cp
	fmt.Fprintf(w, `<tr><td>SequencingDir</td> <td>%s</td> </tr>`, sequencingDir)
	fmt.Fprintf(w, "</table>")

	sequencingDirContent, err := ioutil.ReadDir(sequencingDir)
	if err != nil {
		return err
	}

	fmt.Fprintf(w, "<ul>\n")
	for _, f := range sequencingDirContent {
		fmt.Fprintf(w, `<li>%s</li>`, f.Name())
	}
	fmt.Fprintf(w, "</ul>\n")
	return nil
}

// Find a node by a path in the graph of submits.  Note that we do not need any special
// treatment of tasks because the submits have been resolved to pointers to other notes.
func findNodeByPath(root flownode.IFlowNode, path string) (flownode.IFlowNode, error) {
	cur := root
	tokens := strings.Split(strings.Trim(path, "/"), "/")

	if len(tokens) == 0 {
		return nil, nil
	}

	if tokens[0] != cur.Name() {
		return nil, fmt.Errorf("Error at token[0] : Root module name '%s' does not match first token of path '%s'", cur.Name(), path)
	}

	for _, tok := range tokens[1:] {
		var err error
		cur, err = findSubmitsByName(cur, tok)
		if err != nil {
			return nil, fmt.Errorf("findNodeByPath(root, %s) : Error at token %s : %v", path, tok, err)
		}
	}

	return cur, nil
}

// This function is a duplicate of another function that exists somewhere else
// findChildByName, this one and the other should both become an IFlowNode interface method
func findSubmitsByName(node flownode.IFlowNode, name string) (flownode.IFlowNode, error) {
	for _, ch := range node.Submits() {
		if ch.Name() == name {
			return ch, nil
		}
	}
	return nil, fmt.Errorf("Could not find submits of node %s with name %s", node.Path(), name)
}
