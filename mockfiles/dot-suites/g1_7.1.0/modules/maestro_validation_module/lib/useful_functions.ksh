#==============================================================================
#
# The purpose of this file is to provide useful ksh functions that will
# be sourced by the validation task.
#
#==============================================================================

#==============================================================================
# Check if a variable is set
# Globals:
#   None
# Arguments:
#   Variable to test
# Returns:
#   0 (false) or 1 (true)
#==============================================================================
is_set ()
{
  if [[ "x${1}" == "x" ]]
  then
    return 1
  else
    return 0
  fi
}

#==============================================================================
# Prints a dashed line. 
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
#==============================================================================
print_dashed_line ()
{
  echo -e "---------------------------------------------------------------------------------\n"
}


#==============================================================================
# Prints a line separator. 
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
#==============================================================================
print_separator ()
{
  echo -e "#===============================================================================\n"
}

#==============================================================================
# Prints a message.
# Globals:
#   None
# Arguments:
#   String of the message to be printed.
# Returns:
#   None
#==============================================================================
print_message ()
{
  echo -e "\n#==============================================================================="
  echo -e "#  $1"
  echo -e "#===============================================================================\n"
}

#==============================================================================
# Splits a colon (:) separated list and outputs an array.
# Globals:
#   None
# Arguments:
#   Colon separated string.
# Returns:
#   Array.
#==============================================================================
get_array_from_string ()
{
  echo -e $(echo -e ${1} | tr ":" "\n")
}

#==============================================================================
# For every string in $1, deletes each line containing it in the file $2.
# Globals:
#   None
# Arguments:
#   $1 comma separated string contained in each line to be deleted from $2.
#   $2 filename
# Returns:
#   None
#==============================================================================
delete_strings_from_file ()
{
  string_array=`get_array_from_string ${1}`

  for string_to_delete in ${string_array}
  do
    sed -i "/${string_to_delete}/d" $2
  done
}

#==============================================================================
# Echoes the true_path of a file only if it's a symlink.
# Otherwise echoes nothing
# Globals:
# Arguments:
#   $1 : full file path
# Returns:
#   None
#==============================================================================
display_true_path_if_symlink ()
{
  if [[ -L $1 ]]
  then
    echo -e "INFO : ${1} is a symbolic link to `true_path ${1}`"
  fi
}
#==============================================================================
# Adds a string to the global variable 'STATISTICS_SUMMARY'.
# Globals:
#   STATISTICS_SUMMARY
# Arguments:
#   $1 : filename
#   $2 : corresponding information about the file.
# Returns:
#   None
#==============================================================================
update_statistics ()
{
  STATISTICS_SUMMARY="${STATISTICS_SUMMARY}\n${1} :\t${2}"
}

#==============================================================================
# Updates the global variable CURRENT_DIFF_ADVICE to provide 
# a diff advice for files of type "data".
# globals:
#   CURRENT_DIFF_ADVICE
# arguments:
#   $1 : first file path
#   $2 : second file path
# returns:
#   none
#==============================================================================
update_diff_advice_data ()
{
  CURRENT_DIFF_ADVICE="cmp $1 $2"
}

#==============================================================================
# Updates the global variable CURRENT_DIFF_ADVICE to provide 
# a diff advice for files of type "bulletins".
# globals:
#   CURRENT_DIFF_ADVICE
# arguments:
#   $1 : first file path
#   $2 : second file path
# returns:
#   none
#==============================================================================
update_diff_advice_bulletin ()
{
  CURRENT_DIFF_ADVICE="kdiff3 $1 $2\n"
}

#==============================================================================
# Updates the global variable CURRENT_DIFF_ADVICE to provide 
# a diff advice for files of type "img" or "rrbx".
# globals:
#   CURRENT_DIFF_ADVICE
# arguments:
#   $1 : first file path
#   $2 : second file path
# returns:
#   none
#==============================================================================
update_diff_advice_image ()
{
  CURRENT_DIFF_ADVICE=". ssmuse-sh -d ${SSM_CMDS}; viza $1 $2\n"
}

#==============================================================================
# Updates the global variable CURRENT_DIFF_ADVICE to provide 
# a diff advice for files of type "fstd" compared with $COMPARE_STRICT = 1.
# globals:
#   CURRENT_DIFF_ADVICE
# arguments:
#   $1 : first file path
#   $2 : second file path
# returns:
#   none
#==============================================================================
update_diff_advice_fstcomp ()
{
  CURRENT_DIFF_ADVICE=". ssmuse-sh -d ${SSM_RPN_UTILS};fstcomp -a $1 -b $2\n"
}

#==============================================================================
# Updates the global variable CURRENT_DIFF_ADVICE to provide 
# a diff advice for files of type "fstd" compared with $COMPARE_STRICT = 0.
# globals:
#   CURRENT_DIFF_ADVICE
# arguments:
#   $1 : first file path
#   $2 : second file path
# returns:
#   none
#==============================================================================
update_diff_advice_fst_compare ()
{
  filename=`basename ${1}`

  listing_summary=${TASK_OUTPUT}/${filename}_summary.log

  text="=== File ${filename} ===="
  text="${text}\nFILE 1 : ${1}"
  text="${text}\nFILE 2 : ${2}"

  if [[ -f ${listing_summary} ]]
  then
    text="${text}\n`cat ${listing_summary}`\n"
  fi

  text="${text}\nFULL COMPARE LISTING :\n${TASK_OUTPUT}/${filename}.log\n"

  CURRENT_DIFF_ADVICE=${text}
}

#==============================================================================
# Updates the global variable CURRENT_DIFF_ADVICE to provide 
# a diff advice for files of type "burp".
# globals:
#   CURRENT_DIFF_ADVICE
# arguments:
#   $1 : first file path
#   $2 : second file path
# returns:
#   none
#==============================================================================
update_diff_advice_burp ()
{
  filename=`basename ${1}`

  log_file=${TASK_OUTPUT}/${filename}.log

  burp2ascii_1=${TASK_OUTPUT}/`echo -e ${1} | sed "s:/:_:g"`_ascii_sorted
  burp2ascii_2=${TASK_OUTPUT}/`echo -e ${2} | sed "s:/:_:g"`_ascii_sorted

  #
  # In case of error, the sorted burp files are converted to
  # ascii so one may simply call any diff utility.
  #
  if [[ -f ${burp2ascii_1} && -f ${burp2ascii_2} ]]
  then
    text="=== File ${filename} ===="
    text="${text}\n\tFILE 1 : ${1}"
    text="${text}\n\tFILE 2 : ${2}"

    text="${text}\n\n\tBurpdiff log : ${log_file}"
    text="${text}\n\n\tFiles sorted and converted to ASCII :"
    text="${text}\n\n\tkdiff3 ${burp2ascii_1} ${burp2ascii_2}"
  fi


  CURRENT_DIFF_ADVICE="${text}\n"
}

#==============================================================================
# Updates the global variable CURRENT_DIFF_ADVICE to provide 
# a diff advice for files of type "cmcarc".
# globals:
#   CURRENT_DIFF_ADVICE
# arguments:
#   $1 : path of the reference file.
#   $2 : path of the file to be compared.
# returns:
#   none
#==============================================================================
update_diff_advice_cmcarc ()
{
  filename=`basename ${1}`

  reference_dir=${TASK_OUTPUT}/${filename}/reference
  work_dir=${TASK_OUTPUT}/${filename}/work

  text="\n=== File ${filename} ===="
  text="${text}\nREFERENCE  : ${1}"
  text="${text}\nWORK       : ${2}\n"

  if [[ -d ${reference_dir} && -d ${work_dir} ]]
  then
    text="${text}\nkdiff3 -r ${reference_dir} ${work_dir}\n"
  else
    text="${text}\nWARNING: Unable to find the directories for both files.\n"
  fi

  CURRENT_DIFF_ADVICE=${text}
}


