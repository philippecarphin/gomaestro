#==============================================================================
# Analyze the results an fstcomp diff and returns the 'final' error code.
#
# Globals:
#   In  : ACTIVATE_DEBUGGING
# Arguments:
#   $1 : return code of fstcomp.
#   $2 : file output of fstcomp.
# Returns:
#   Final error code.
#==============================================================================
analyse_results_fstcomp ()
{
  #==========================================================
  # New feature in V1.6: activate debug mode or not
  #==========================================================
  if [[ ${ACTIVATE_DEBUGGING} -eq 0 ]]
  then
    set +x
  else
    set -x
  fi

  diff_found=$1
  fichier_results=`pwd`/$2

  if [ $diff_found -ne 0 ]
  then
    diff_found=1
  fi

  # champ manquant
  ret_code=0
  grep 'PAS TROUVE' $fichier_results > /dev/null 2>&1 || ret_code="$?"
  if [ $ret_code -eq 0 ]
  then
    diff_found=1
  fi

  # champ different
  ret_code=0
  grep '^[ ]*<' $fichier_results > /dev/null 2>&1 || ret_code="$?"
  if [ $ret_code -eq 0 ]
  then
    diff_found=1
  fi

  # dimension incorrecte
  ret_code=0
  grep 'CHERCHE' $fichier_results > /dev/null 2>&1 || ret_code="$?"
  if [ $ret_code -eq 0 ]
  then
    diff_found=1
  fi

  echo -e ${diff_found}
}

 

#==============================================================================
# Execute fstcomp and analyze the result.
#
# Globals:
#   None
# Arguments:
#   $1 : Path of the first standard file to compare.
#   $2 : Path of the second standard file to compare.
# Returns:
#   Compare return value (0 : success, 1 : error).
#==============================================================================
fstcomp_analyze ()
{
  ret_code_1=0

  rm -f file_fstcomp_1 file_fstcomp_2

  ln -s $1 file_fstcomp_1
  ln -s $2 file_fstcomp_2

  set -ex
  ${TASK_BIN}/fstcomp -a file_fstcomp_1 \
                      -b file_fstcomp_2 > $(pwd)/fstcomp_results 2> /dev/null || ret_code_1="$?"

  ret_code_2=$(analyse_results_fstcomp ${ret_code_1} "fstcomp_results")

  ret_code=$(( ret_code_1 + ret_code_2 ))

  set +ex
  echo -e ${ret_code}
}


#==============================================================================
# Compare standard files with:
#   fstcomp $1 $2 
#   and
#   fstcomp $2 $1
#
# Globals:
#   Out : CURRENT_DIFF_STATUS
#   Out : CURRENT_DIFF_MESSAGE
# Arguments:
#   $1 : Path of the first standard file to compare.
#   $2 : Path of the second standard file to compare.
# Returns:
#   None.
#==============================================================================
compare_std ()
{
  diff_found_1=0
  diff_found_2=0

  diff_found_1=$(fstcomp_analyze $1 $2)
  diff_found_2=$(fstcomp_analyze $2 $1)

  ret_code=$(( diff_found_1 + diff_found_2 ))

  if [[ $ret_code -eq 0 ]]
  then
    CURRENT_DIFF_STATUS="SUCCESS"
    CURRENT_DIFF_MESSAGE="STANDARD FILES ARE IDENTICAL"
  else
    CURRENT_DIFF_STATUS="COMPARE FAILED"
    CURRENT_DIFF_MESSAGE="STANDARD FILES DIFFER"
    NUMBER_OF_FAILED_DIFFS=$((NUMBER_OF_FAILED_DIFFS+1))
  fi

}

#==============================================================================
# Compare standard files with fst_compare.
#
# Globals:
#   Out : CURRENT_DIFF_STATUS
#   Out : CURRENT_DIFF_MESSAGE
# Arguments:
#   $1 : Path of the first standard file to compare.
#   $2 : Path of the second standard file to compare.
# Returns:
#   None.
#==============================================================================
fst_compare_std ()
{

  std_file_name=`basename $1`

  ret_code=0
  set -ex
  ln -sf $1 fic_a
  ln -sf $2 fic_b
  #pour debugger on peut passer --directory /path/qu/on/veut  et les tmp seront crees a cet endroit
  eval ${TASK_BIN}/fst_compare ${STD_COMPARE_OPTIONS} fic_a fic_b > ${TASK_OUTPUT}/${std_file_name}.log || ret_code="$?"
  set +ex

  if [[ $ret_code -eq 0 ]]
  then
    #
    # Check if the files are identical
    #
    identical_ret_code=0
    grep 'fichiers sont identiques' ${TASK_OUTPUT}/${std_file_name}.log > /dev/null 2>&1 || identical_ret_code="$?"

    if [ $identical_ret_code -eq 0 ]
    then
      CURRENT_DIFF_STATUS="SUCCESS"
      CURRENT_DIFF_MESSAGE="STANDARD FILES ARE IDENTICAL"
    else
      CURRENT_DIFF_STATUS="SUCCESS"
      CURRENT_DIFF_MESSAGE="STANDARD FILES ARE SIMILAR"
    fi
  else
    #
    # Since level 4 is assumed (very verbose), provide a short
    # summary of the fields that did not meet the threshold.
    #
    cp ${TASK_OUTPUT}/${std_file_name}.log ${TASK_OUTPUT}/${std_file_name}_summary.log

    sed -i '0,/^Fstcomp a/d'  ${TASK_OUTPUT}/${std_file_name}_summary.log
    sed -i '/NIVEAU 3/,$d'    ${TASK_OUTPUT}/${std_file_name}_summary.log
    sed -i '/---/d'           ${TASK_OUTPUT}/${std_file_name}_summary.log

    line_count=$(wc -l ${TASK_OUTPUT}/${std_file_name}_summary.log | cut -f1 -d' ')

    if [[ $line_count -gt 15 ]]
    then
      rm -rf ${TASK_OUTPUT}/${std_file_name}_summary.log

      cat << EOFLOG > ${TASK_OUTPUT}/${std_file_name}_summary.log
      Too many differences to display them here (> 15 fields). Please check the complete fst_compare listing.
EOFLOG
    fi

    echo "****** See ${TASK_OUTPUT}/${std_file_name}.log for more details!"

    CURRENT_DIFF_STATUS="COMPARE FAILED"
    CURRENT_DIFF_MESSAGE="FST_COMPARE FAILED WITH THRESHOLD"
    NUMBER_OF_FAILED_DIFFS=$((NUMBER_OF_FAILED_DIFFS+1))
  fi
}


#==============================================================================
# Compare fake standard files with fst_compare.
#
# Globals:
#   Out : CURRENT_DIFF_STATUS
#   Out : CURRENT_DIFF_MESSAGE
# Arguments:
#   $1 : Path of the first standard file to compare.
#   $2 : Path of the second standard file to compare.
# Returns:
#   None.
#==============================================================================
compare_fake_fstd_files ()
{
  ret_code_1=0
  ret_code_2=0
  ret_code=0

  # Save the files for potential manual comparison
  umique_dirname=`date +%s%N` # seconds since the epoch + current nannoseconds
  mkdir ${TASK_OUTPUT}/$umique_dirname

  filename1=${TASK_OUTPUT}/${umique_dirname}/`basename $1`_reference
  filename2=${TASK_OUTPUT}/${umique_dirname}/`basename $2`_local

  rm -f filein1 filein2
  cp $1 filein1
  cp $2 filein2

  ulimit -s 256000

  set -ex
  spooki_run "[ReaderStd --input filein1] >> [PrintIMO --output ${filename1}]"  > /dev/null 2>&1 || ret_code_1="$?"
  spooki_run "[ReaderStd --input filein2] >> [PrintIMO --output ${filename2}]"  > /dev/null 2>&1 || ret_code_2="$?"
  set +ex

  if [[ $ret_code_1 -ne 0 || $ret_code_2 -ne 0 ]]
  then
    CURRENT_DIFF_STATUS="COMPARE FAILED"
    CURRENT_DIFF_MESSAGE="THESE FILES ARE TOO UNORTHODOX EVEN FOR SPOOKI-PrintIMO, SORRY COULD NOT COMPARE."
    CURRENT_DIFF_ADVICE="THESE FILES ARE SO UNORTHODOX ALL I CAN DO IS WISH YOU GOOD LUCK.\n"
    #CURRENT_DIFF_ADVICE="${CURRENT_DIFF_ADVICE}Please let me know if you find a way to compare them : naysan.saran@canada.ca."
    NUMBER_OF_FAILED_DIFFS=$((NUMBER_OF_FAILED_DIFFS+1))
  else

    sed -i '/_referenceTime/d' ${filename1}
    sed -i '/_referenceTime/d' ${filename2}

    set -ex
    diff ${filename1} ${filename2} || ret_code="$?"
    set +ex

    if [[ $ret_code -eq 0 ]]
    then
      CURRENT_DIFF_STATUS="SUCCESS"
      CURRENT_DIFF_MESSAGE="FAKE STANDARD FILES ARE IDENTICAL"
    else
      CURRENT_DIFF_STATUS="COMPARE FAILED"
      CURRENT_DIFF_MESSAGE="FAKE STANDARD FILES ARE DIFFER."
      CURRENT_DIFF_ADVICE="diff ${filename1} ${filename2}"
      NUMBER_OF_FAILED_DIFFS=$((NUMBER_OF_FAILED_DIFFS+1))
    fi
  fi

}



