#==============================================================================
#
# New in version 2.4 : the validation module can now handle files that are
# hosted on a remote server.
#
#==============================================================================


check_remote_directory ()
{
    directory=${1}
    dir_type=${2}

    if [[ ${directory#*@} == ${directory} ]]
    then
        REMOTE_ON=0
    else
        number_elements=`echo -e ${directory} | sed 's/@/ /g' | wc -w`

        if [[ ${number_elements} -ne 3 ]]
        then
            print_message "ERROR : the directory ${directory} should follow the standatd user@hostname@fullpath"
            exit 1
        fi

        REMOTE_ON=1
        CURRENT_USER_AT_HOST=${directory%@*}
        CURRENT_REMOTE_DIR=${directory##*@}

        # Check if the remote directory exists
        remote_dir_exists=1
        ssh ${CURRENT_USER_AT_HOST} ls ${CURRENT_REMOTE_DIR} > /dev/null 2>&1 || remote_dir_exists=0

        if [[ ${remote_dir_exists} -ne 1 ]]
        then
            print_message "ERROR : the command 'ssh ${CURRENT_USER_AT_HOST} ls ${CURRENT_REMOTE_DIR}' returned an error "
            exit 1
        fi

        CURRENT_SCP_DIR=${TASK_WORK}/${dir_type}/$(echo -e ${CURRENT_USER_AT_HOST} | sed 's/@/_at_/g')/$(basename ${CURRENT_REMOTE_DIR})

        mkdir -p ${CURRENT_SCP_DIR}

        # Scp only if there are files to copy
        number_files=$(ssh ${CURRENT_USER_AT_HOST} ls ${CURRENT_REMOTE_DIR} | wc -l)

        if [[ ${number_files} -gt 0 ]]
        then
            scp -r ${CURRENT_USER_AT_HOST}:${CURRENT_REMOTE_DIR}/* ${CURRENT_SCP_DIR} || true
        fi
    fi
}

#==============================================================================
# If any of the specified directories is located on a remote server, this
# function will scp it's contents to a local directory under ${TASK_WORK}
# and update the corresponding directory list
# globals:
#   local_directory_list
#   reference_directory_list
#   TASK_WORK
# arguments:
# returns:
#   none
#==============================================================================
handle_remotes_transparently ()
{
    
    new_local_directory_list=""
    new_reference_directory_list=""

    for directory in ${local_directory_list} 
    do
        REMOTE_ON=0
        check_remote_directory ${directory} local

        if [[ ${REMOTE_ON} -eq 1 ]]
        then
            new_local_directory_list="${new_local_directory_list} ${CURRENT_SCP_DIR}"
        else 
            new_local_directory_list="${new_local_directory_list} ${directory}"
        fi
    done

    for directory in ${reference_directory_list} 
    do
        REMOTE_ON=0
        check_remote_directory ${directory} reference

        if [[ ${REMOTE_ON} -eq 1 ]]
        then
            new_reference_directory_list="${new_reference_directory_list} ${CURRENT_SCP_DIR}"
        else 
            new_reference_directory_list="${new_reference_directory_list} ${directory}"
        fi
    done
    
    local_directory_list=${new_local_directory_list}
    reference_directory_list=${new_reference_directory_list}
}


