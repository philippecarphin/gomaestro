#==============================================================================
#
# Global variables for the validation task.
#
#==============================================================================

NUMBER_OF_FAILED_DIFFS=0
NUMBER_OF_MISSING_FILES=0

DIFF_ADVICE=""

TOTAL_NUMBER_OF_FILES=0

STATISTICS_SUMMARY=""

CURRENT_DIFF_ADVICE=""
CURRENT_DIFF_STATUS=""
CURRENT_DIFF_MESSAGE=""

REMOTE_ON=false
CURRENT_USER_AT_HOST=""
CURRENT_REMOTE_DIR=""
CURRENT_SCP_DIR=""

