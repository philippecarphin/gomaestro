#!/bin/ksh
#***
#.*****************************************************************************
#.
#.     JOB NAME - g1pps72 
#.
#.     STATUS - OPERATIONAL - SEMI-ESSENTIAL
#.
#.     ORIGINATOR - TDS SECTION
#.
#.     DESCRIPTION -  produces perfect prog fcst temperatures
#.                    in 3hr increments and derives max/min 
#.                    from the spot temperatures. It produces
#.                    a max for day1, a min/max for day2 and 
#.                    a max for day3. This is used in the
#.                    production of the fxcn05 messages.      
#.    
#.     VERIFICATION 
#.       AN 2000    - La scripte a ete verifie pour l'an 2000. Aucune 
#.                    modification n'a ete requise. Seul l'initialisation
#.                    ENFORC8 pour l'annee a 4 chiffres des fichiers BURP a
#.                    ete introduite. Les programmes appeles ont du subir de 
#.                    modifications.
#.                    Claude Landry 08/1998
#.
#.     Linux Migration (fstretro removed)
#.     M. Klasa - CMDW - Oct 2009
#.
#.*****************************************************************************
#   #########################################################
#
#
#            SYSTEME DE PREVISIONS PONCTUELLES DE TEMPERATURE PP.
#
#              VERSION AVEC NOUVEAU FICHIER D'OBSERVATION SYNOP.
#
#              VERSION FAISANT DES PREVISIONS DE 6 A 144 HEURES A 00Z
#              VERSION FAISANT DES PREVISIONS DE 6 A 72 HEURES A 12Z
#
#              VERSION FAISANT INTERPOLATION DES SYNO MANQUANTS
#                  ( GETDATA  -  INTERPOLATION CARRE DE LA DISTANCE)
#                   VERIFIE NB DE STATIONS CANADIENNES
#
#               VERSION AVEC REDUCTION DE L'ANOMALIE (30 MARS 88)
#
#
#               VERSION AVEC FICHIER ANNUEL POUR SW3H44 
#
#             Aug. 2001: Version with systematic bias error correction
#                     system introduced. Job "g6ppgbc" calculates
#                     bias for each station, each forecast hour,
#                     over a period of the last 30 days. File "ppgbicor"
#                     contains these bias values and is used by program
#                     "mpretmp" to adjust the forecast. Both modified
#                     and unmodified forecasts are stored in file
#                     "mpppptp". G.Richard (Wx. Ele. Div.)
#         ##########################################################

# Constant input files
ln -s ${TASK_INPUT}/mcanstn   mcanstn
ln -s ${TASK_INPUT}/muammnz   muammnz
ln -s ${TASK_INPUT}/mclmt24   mclmt24
ln -s ${TASK_INPUT}/mppspte   mppspte
ln -s ${TASK_INPUT}/mrempla   mrempla

# Circular files (input)
cp ${TASK_INPUT}/ms3h144   ms3h144
cp ${TASK_INPUT}/mtstsyn   mtstsyn
cp ${TASK_INPUT}/ppgbicor  ppgbicor

# Circular files (output)
cp ${TASK_INPUT}/mpppptp mpppptp
cp ${TASK_INPUT}/mnnxxll mnnxxll
cp ${TASK_INPUT}/mnnxxcc mnnxxcc 

# Input
ln -s ${TASK_INPUT}/metar
ln -s ${TASK_INPUT}/synop

# Pour introduite l'annee a 4 chiffres aux fichiers BURP
ENFORC8=OUI
export ENFORC8

#************************************************************
#   Use the operational file updated in r1synop
#   execute editbrp
#   CMOI: link made to counter the 50 character 
#   path limit in mgettmp 
#************************************************************
metar_path=metar
synop_path=synop
metar_arg_len=$((`echo ${metar_path}|wc -c`-1))
synop_arg_len=$((`echo ${synop_path}|wc -c`-1))

${TASK_BIN}/mgettmp -isdstn mcanstn -odahre ft09$$ -ivar 1 -date ${CMCSTAMP} -metar ${metar_path} -synop ${synop_path} -metarlen ${metar_arg_len} -synoplen ${synop_arg_len}
#${TASK_BIN}/mgettmp -isdstn mcanstn -odahre ft09$$ -ivar 1 -date ${CMCSTAMP} -metar ${metar_path} -synop ${synop_path}

#*******************************************************************
#       MODIFIE LA DATE SUR LE FICHIER FT79
#          ET ECRIT 1 DANS LE FICHIER FT78
#*******************************************************************

${TASK_BIN}/mpredat -i $in -l $out \
                    -odate ft79 \
                    -opart ft78 \
                    -date ${CMCSTAMP}

#*******************************************************************
#        EXECUTION DU PROGRAMME DE PREVISIONS DES TEMPERATURES
#*******************************************************************

${TASK_BIN}/swef.mpretmp -i $in -l $out \
                         -irspec ms3h144 \
                         -islist mcanstn \
                         -iwuamn muammnz \
                         -ircsfc mclmt24 \
                         -ireqnr mppspte \
                         -idsyno mtstsyn \
                         -isremp mrempla \
                         -orfcst mpppptp \
                         -info ft79 \
                         -ipart ft78 \
                         -irbc ppgbicor \
                         -date ${CMCSTAMP} 

#*******************************************************************
#        EXECUTION  DU PROGRAMME D'EXTRACTION DES MIN/MAX
#*******************************************************************

${TASK_BIN}/swef.mspmxmn -i $in -l $out \
                         -islist mcanstn \
                         -irfcst mpppptp \
                         -ormxlo mnnxxll \
                         -ormxcl mnnxxcc \
                         -info ft79 \
                         -ipart ft78 \
                         -date ${CMCSTAMP}


#
# Dater les fichiers circulaires en sortie 
#
cp  mpppptp ${TASK_OUTPUT}/DEPOT/${mpppptp_out}
cp  mnnxxll ${TASK_OUTPUT}/DEPOT/${mnnxxll_out}
cp  mnnxxcc ${TASK_OUTPUT}/DEPOT/${mnnxxcc_out}

#ln -fs `true_path  ${TASK_OUTPUT}/DEPOT/${mpppptp_out}` ${TASK_OUTPUT}/data2/mpppptp
#ln -fs `true_path  ${TASK_OUTPUT}/DEPOT/${mnnxxll_out}` ${TASK_OUTPUT}/data2/mnnxxll
#ln -fs `true_path  ${TASK_OUTPUT}/DEPOT/${mnnxxcc_out}` ${TASK_OUTPUT}/data2/mnnxxcc

