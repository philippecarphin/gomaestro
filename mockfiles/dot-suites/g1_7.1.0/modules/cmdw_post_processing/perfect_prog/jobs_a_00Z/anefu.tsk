#!/bin/ksh
#
#.*****************************************************************************
#.
#.     JOB NAME - g1anefu 
#.
#.     STATUS - OPERATIONAL - SEMI-ESSENTIAL
#.
#.     ORIGINATOR - cmdw 
#.
#.     MODIFS - Linux Migration (fstretro removed)
#.             Yves Chartier/Marc Klasa - CMDW - juillet 2009/fevrier 2010
#.
#.     PRODUCT - alpha_numeric message (Runs only at 00Z)
#.
#.               time avbl(GMT)   description - header(s) 
#.               --------------   -----------------------
#.     
#.     cmc_bul     0615           FXCN07 CWAO1 thru 12
#.     cmc_bul     0630           FOCN12 CWAO1 thru 12 (english)
#.     cmc_bul     0630           FOCN13 CWAO1 thru 12 (french)
#.     
#.
#.     DESCRIPTION -  produces regular and back-up day3-4-5 
#.                    1) wx element fcsts (fxcn07)
#.                    2) worded fcsts (focn12 and focn13)
#.
#.
#.*****************************************************************************

user=`id -nu`

# Constant input files 
ln -s ${TASK_INPUT}/mpopclm mpopclm
ln -s ${TASK_INPUT}/climt24_61-90 climt24_61-90
ln -s ${TASK_INPUT}/mcanstn mcanstn
ln -s ${TASK_INPUT}/msfcwin msfcwin
ln -s ${TASK_INPUT}/mpcp28y mpcp28y
ln -s ${TASK_INPUT}/mopadly mopadly
ln -s ${TASK_INPUT}/msunmax msunmax
ln -s ${TASK_INPUT}/maufdir maufdir
ln -s ${TASK_INPUT}/mlatlon mlatlon
ln -s ${TASK_INPUT}/mgriddx mgriddx
ln -s ${TASK_INPUT}/mupa28y mupa28y

# Circular files (input)
cp ${TASK_INPUT}/mft6day mft6day

# Circular files (output)
cp ${TASK_INPUT}/mver345 mver345
cp ${TASK_INPUT}/melmnts melmnts

#--------------------------------------------
# up PGSM directive files
#--------------------------------------------
cat <<EOT >pgsm1
 SORTIE(STD,100,A) 
 GRILLE(PS,22,20,14.,20.,381000.,350.,NORD) 
 HEURE(60,84,108,132)
 CHAMP(Z,500,1000)
EOT
cat <<EOT >pgsm2
 SORTIE(STD,100) 
 GRILLE(TAPE1,7,10,40)
 HEURE(60,84,108,132)
 CHAMP(EPAIS,1000,500)
 CHAMP(EPAIS,850,700) 
 CHAMP(EPAIS,1000,850)
EOT
cat <<EOT >pgsm3
 SORTIE(STD,100) 
 GRILLE(TAPE1,7,10,40)
 HEURE(TOUT)
 CHAMP(UV,12000)
EOT

#------------------------------------------
# prepare prog file (upaflds.v)
#------------------------------------------
echo 'prepare prog files'

prgfil=${TASK_INPUT}/prgfil/*
prgeta=${TASK_INPUT}/prgeta/*

${TASK_BIN}/pgsm -iment $prgfil  -ozsrt spe_00 -i pgsm1 

${TASK_BIN}/pgsm -iment $prgfil mlatlon -ozsrt spe_00 -i pgsm2 

${TASK_BIN}/pgsm -iment $prgeta mlatlon -ozsrt spe_00 -i pgsm3 

#-------------------------------------------------
# determine analog dates
# file required from PGSM runs: lu45 (spe_00)
#-------------------------------------------------
echo 'determine analog dates'

${TASK_BIN}/mfp36an -osanlg   anlg_00 \
                    -isgrids  mgriddx \
                    -improgs  spe_00 \
                    -iwupair  mupa28y \
                    -date     ${CMCSTAMP} 

#-------------------------------------------------
# prepare element matrix
# files required from analog forecasts: 
# lu30 (anlg_00), lu45 (spe_00)
#-------------------------------------------------
echo 'prepare forecast element matrix'

${TASK_BIN}/mfp36el  -ismnpop mpopclm -ismntmp climt24_61-90 -isstns mcanstn \
                     -irmxmn mft6day -isanlg anlg_00 -oslmnts melmnts \
                     -iwwind msfcwin -iwpcpn mpcp28y -iwopac mopadly -irsun msunmax \
                     -imthkns spe_00 -owverif mver345 -oscmqel mfxcn07_00 -date ${CMCSTAMP} 

#-------------------------------------------------
# send fxcn07
#-------------------------------------------------
cat mfxcn07_00

if [[ "$PREOP_STATE" != "ON" ]]; then
    cat mfxcn07_00|${TASK_BIN}/nanproc -s -b -p b -j ${JOBNAME}.1
fi

#----------------------------------------------
# prepare worded forecasts
# file required from elements forecasts: lu32
# (melmnts)
#----------------------------------------------
echo 'prepare worded forecasts'

${TASK_BIN}/mfp36wo -islmnts  melmnts \
                    -isdirec  maufdir \
                    -osfo12   mfocn12_00 \
                    -osfo13   mfocn13_00 \
                    -osbu12   mbucn12_00 \
                    -osbu13   mbucn13_00 \
                    -date     ${CMCSTAMP} 

${TASK_BIN}/editfst -strict -s mver345 -d mt345 <<EOF
end
EOF

#
# Dater les fichiers circulaires en sortie et recreer les liens symboliques
# 
cp mt345 ${TASK_OUTPUT}/DEPOT/${mver345_out}
cp melmnts ${TASK_OUTPUT}/DEPOT/${melmnts_out}

#ln -fs `true_path  ${TASK_OUTPUT}/DEPOT/${mver345_out}` ${TASK_OUTPUT}/data2/mver345
#ln -fs `true_path  ${TASK_OUTPUT}/DEPOT/${melmnts_out}` ${TASK_OUTPUT}/data2/melmnts

#----------------------------------------------
# the var. PREOP_STATE is defined in suite's
# .profile_ocm operational mode
#----------------------------------------------
if [[ "$PREOP_STATE" != "ON" ]]; then
    # send out tty messages
    #  focn12
    cat mfocn12_00|${TASK_BIN}/nanproc -s -b -p b -j ${JOBNAME}.2
    #  focn13
    cat mfocn13_00|${TASK_BIN}/nanproc -s -b -p b -j ${JOBNAME}.3
fi

#----------------------------------------------
# save backup messages
#----------------------------------------------
cp mbucn12_00 ${TASK_OUTPUT}/bkup/focn12_00z_bkp
cp mbucn13_00 ${TASK_OUTPUT}/bkup/focn13_00z_bkp

#
# the var. PREOP_STATE is defined in suite's .profile_ocm
# operational mode
#
if [[ "$PREOP_STATE" != "ON" ]]; then
    ${TASK_BIN}/nanproc  -b -f mfocn13_00 -p f -d nccsan_o.backups -r 4 
    ${TASK_BIN}/nanproc  -b -f mbucn13_00 -p f -d nccsan_o.backups -r 4
fi

cat mfocn12_00 mfocn13_00 mfocn13_00  mbucn13_00

#
#  now send it to the bbs
#
set +e
echo COPY OF mfxcn07_00 for meteomedia
cat mfxcn07_00

#
# Because ftpcis erases what it sends
#
cp mfxcn07_00 ${TASK_OUTPUT}/bulletins/mfxcn07_00
${TASK_BIN}/ftppds -c METEOMEDIA -i ${TASK_OUTPUT}/bulletins/mfxcn07_00 -t T -r G1 -h 00 -p FXCN07 -u MET
set -e

#
# Ajout Naysan : a enlever avant de finaliser. 
# Copier  tous les bulletins dans DEPOT pour pouvoir valider automatiquement
#
cp mfxcn07_00 ${TASK_OUTPUT}/DEPOT/${SEQ_SHORT_DATE}_mfxcn07
cp mfocn12_00 ${TASK_OUTPUT}/DEPOT/${SEQ_SHORT_DATE}_mfocn12
cp mfocn13_00 ${TASK_OUTPUT}/DEPOT/${SEQ_SHORT_DATE}_mfocn13



