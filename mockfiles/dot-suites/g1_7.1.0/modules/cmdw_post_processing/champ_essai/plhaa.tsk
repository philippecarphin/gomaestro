#!/bin/ksh
#***
#.*****************************************************************************
#.
#.     JOB NAME -  g1plhaa_y2k
#.
#.     STATUS - OPERATIONAL - SEMI-ESSENTIAL 
#.
#.     ORIGINATOR - OIS 
#.
#.     PRODUCT - plot chart
#.       
#.               plot_#        printed   time avbl(GMT)   description 
#.               ------        -------   -------------    -----------
#.
#.     cmc_plt   plhaa00         yes         n/a          trial field 500mb 
#.     cmc_plt   plhaa12         yes         n/a          trial field 500mb
#.
#.     non operationnelle
#.     cmc_plt   plhaa06         yes         n/a          trial field 500mb
#.     cmc_plt   plhaa18         yes         n/a          trial field 500mb
#.
#.
#.     
#.     DESCRIPTION -  the map contains 500mb gz, 500mb qq.
#.       
#.     REVISION 1.1:   Conversion AN 2000
#.                     Gilles Desautels - 21 aout 1998
#.
#.                     NOTE: on fait simplement un pointage de carte
#.                           les fonctions RPN sont les seules ici utilisees
#.                           aucun changement requis
#.                           (ici modification pour les test AN 2000)
#.
#.*****************************************************************************

#.*****************************************************************************
#.    REVISION 1.2    Conversion a Maestro
#.                    Naysan Saran - 11 Dec. 2013
#.***************************************************************************** 


###############################################################################
#  pgsm va chercher les champs dans les projections desirees
#  les cartes de controles pour pgsm

cat << EOFPGSM > pgsmdir
#
* DIRECTIVES PGSM QUI SERVENT A R5PLT53 (SERIE CHAMP ESSAI)
*******************************************************************************
* 13 juin  1991
* GILLES DESAUTELS (DDO)
*******************************************************************************
* UTILISE PLT530x SUR MFA POUR IMPORTER CE FICHIER
*         SUR CRAY ET L'EXECUTER
*
* On utilise comme source PGSM PDN=NOATRL0
*         on va chercher l'heure 6
*
* Aucun equivalent unix pour le moment
*
* LE NOM DU FICHIER PGSM PRODUIT EST ALORS DONNE
*         PAR LES CARTES DE JCL
*  ************************************************
*  JOB NAME - R5PLT53
*  JOB STATUS - SEMI-ESSENTIAL
*  ************************************************
* CARTES OPERATIONNELLES 2E ETAGE (archives)
* (ECHELLE 1/20M,  1 INTERVALLE DE GRILLE = 3/8 PO)
* UTILISE RESOLUTION 190.5 KM DANS LA BASE DE DONNEES POUR PGSM
*------------------------------------------------------------------------------
* ROTATION DE 21 DEGRES
* GRILLE ADAPTE POUR FAIRE  28.50 po EN X,SOIT 76 INTERVALLES (GRILLE 76+1=77)
*                           21.00 po EN Y,SOIT 56 INTERVALLES (GRILLE 56+1=57)
*
*        POLE NORD A 15.00 po EN X, SOIT POSITION 40+1 SUR LA GRILLE
*                    15.00       Y,               40+1
*
*
* DANS TRAMES ON UTILISE UNE ROTATION DE 90 DEGRES (ORN=ROT)
* -----------            HX= 2850
*                        HY= 2100
*                      SIZE= 3000
*
*------------------------------------------------------------------------------
*                                                                          PGSM
*------------------------------------------------------------------------------
 SORTIE(STD,15,A)
* Modification le 13 mars 1992: resolution = 76.2 km
* GRILLE(PS, 77, 57, 41., 41., 190500., 21.0, NORD)
 GRILLE(PS, 191, 141, 101.0, 101.0, 76200., 21.0, NORD)
 COMPAC=-16 
 HEURE(6) 
 CHAMP(Z,500) 
 CHAMP(Q,500) 
*
*------------------------------------------------------------------------------
*                                                                     FIN  PGSM
*------------------------------------------------------------------------------
#
EOFPGSM
#
#  execution du pgsm
#
nom_fichier_pgsm=${TASK_INPUT}/fichier_pgsm

rm -f femap
${TASK_BIN}/pgsm -iment $nom_fichier_pgsm -ozsrt femap  -i pgsmdir

###############################################################################
#
cat << EOFSIGMA > sigmadir
#
*******************************************************************************
*
* DIRECTIVES SIGMA POUR PRODUIRE R5PLT53 - PLT5301 PROG 6H (500MB   Z  Q)
* TRIAL FIELD - CHAMP D'ESSAI
*******************************************************************************
*
* 7 JUIN 1991
* GILLES DESAUTELS (DDO)
*******************************************************************************
*
* JOB NAME - R5PLT53
* JOB STATUS - ESSENTIAL
*
*******************************************************************************
* SPECIFICATIONS PGSM
* -------------------
* CARTES OPERATIONNELLES 2E ETAGE
* (ECHELLE 1/20M,  1 INTERVALLE DE GRILLE = 3/8 PO AVEC RESOLUTION DE 190.5 km)
*
*
* ROTATION DE 21 DEGRES
* GRILLE ADAPTE POUR FAIRE  28.50 po EN X,SOIT 76 INTERVALLES (GRILLE 76+1=77)
*                           21.00 po EN Y,SOIT 56 INTERVALLES (GRILLE 56+1=57)
*
*        POLE NORD A 15.00 po EN X, SOIT POSITION 40+1 SUR LA GRILLE
*                    15.00       Y,               40+1
*
*
* DANS TRAMES ON UTILISE UNE ROTATION DE 90 DEGRES (ORN=ROT)
* -----------            HX= 2850 
*                        HY= 2100
*                      SIZE= 3000
*
*******************************************************************************
*                DIRECTIVES SIGMA
*******************************************************************************
*                DIRECTIVES GLOBALES
*******************************************************************************
 CLIP = OUI
 NORMID = NON
 LISSAGE=OUI
*
*LA FONTE 15 CONTIENT LES MAJUSCULES, LA 16 LES MINUSCULES
*LA FONTE 10 CONTIENT LES LETTRES MAJUSCULES DES H,L
*LA FONTE 11 CONTIENT LES LETTRES MINUSCULES CORRESPONDANTES
*LA FONTE 17 CONTIENT LES CHIFFRES DES ETIQUETTES (FONTE ETROITE)
*LA FONTE  2 CONTIENT LES GRANDS + ET -
*LA FONTE  3 CONTIENT LES GRANDS LETTRES GRECQUES
*LA FONTE  9 CONTIENT LE CENTRE (X DANS O)
*
*   @         A  B  C  D  E  F  G  H  I
*   "CASE" 0  1  2  3  4  5  6  7  8  9
 SETFONT([15,16,10,11,12,13, 2, 3,17, 9])
*
* OPTIONS GEOGRAPHIQUES
*
 MAPLAB="ST",1,1,1,1,10,2,NON,OUI,NON,1,.5,OUI
 MAPCOLR=1
*
* EXPLICATION CONTINENTS-MERIDIEN-PARALLELES
* ET PERIMETRE DE LA CARTE (="YES" PAR DEFAUT)
*
 OPTIONS(MAPOPT,["OU","GL","GR",-1.0,"LA","NO"]) 
 OPTIONS(DASHSET,["SMALL",16.0,"NP",1500])
*
* EN UTILISANT 30.0 POUCES (LA CARTE FAIT 28.5 X 19.875)
*    LES DISTANCES RELATIVES SONT VOISINES DU 1/100 PO
*
 ECHELLE=20.0 
 POUCES=30.00
*
 ECHLGRI=NON
*
* PRECISE UN POINTILLE FIN (TOUJOURS INCLURE "NCRT",1)
*
 OPTIONS(ISPSET,["NCRT",1])
*
************************************************************************........
*                    GROUPE
************************************************************************........
* 
* GROUPE LOT=1-2   POUR LES ISOHYPSES DES HAUTEUR  1=majeur
*                  COULEUR CO=7 (BLEU)             2=mineur
* "CA",8 => FONTE 17 (FONTE ETROITE)
*
*
 GROUPE(1,["CO",7,"TH",5,
    "CA",8,"FC",0,"BC",7,"CF","YES","IN",2,"SI",200,"CN",34])
 GROUPE(2,["CO",7,"TH",2,
    "CA",8,"FC",0,"BC",7,"CF","YES","IN",2,"SI",200,"CN",34])
*
* GROUPE LOT=3-4-5 POUR LA TAILLE DES MAX-MIN DES HAUTEURS
*
 GROUPE(3,["SI",900,"IN",3,"FC",7,"ENH","YES","CN",01])
 GROUPE(4,["SI",175,"IN",2,"FC",7,"ENH","YES","CN",01])
 GROUPE(5,["SI",225,"IN",3,"FC",7,"ENH","YES","CN",01,"CA",8])
*
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
* 
* GROUPE LOT=6     POUR LE TOURBILLON
*                  COULEUR CO=2 (ROUGE)
*
 GROUPE(6,["CO",2,"TH",1,
           "CA",8,"FC",2,"BOX","YES","CLR","YES",
                  "IN",2,"SI",175,"CN",34]) 
*
* GROUPE LOT=9,10 POUR LES HAUTEURS DES MAX-MIN DU TOURBILLON ET + -
*        ON A MIS 1 DE MOINS QUE SUR LES AUTRES CARTES
*        POUR DONNER MOINS D'IMPORTANCE AU TOURBILLON
*
 GROUPE( 9,["FC",2,"SI",125,"IN",1,"ENH","YES","CN",01]) 
 GROUPE(10,["FC",2,"SI",175,"IN",1,"ENH","YES","CA",8,"CN",01])
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*
* ECRITURE ID CARTE
*
* GROUPE 25 POUR TITRE ANALYSIS-ANALYSE (TRIAL - ESSAI)
* GROUPE 26 POUR ID DU CMC ET DESCRIPTION DU CONTENU
*        27      HEURE DE VALIDITE
*        28      CONTENU DESCRIPTIF
*        29      TITRE DE LA CARTE
* "CA",0 => FONTE 15  (defaut)
* "CA",8 => FONTE  8  (etroit) (parfois utilise avec GROUPE 28)
*
 GROUPE(25,["CA",0,"SI",225,"IN",2,"FC",1,"CN",01])
 GROUPE(26,["CA",0,"SI",150,"IN",1,"FC",1,"CN",01]) 
 GROUPE(27,["CA",0,"SI",110,"IN",2,"FC",1,"CN",01]) 
 GROUPE(28,["CA",0,"SI",100,"IN",1,"FC",1,"CN",01]) 
 GROUPE(29,["CA",0,"SI",175,"IN",2,"FC",1,"CN",01]) 
************************************************************************........
*
*LE NUMERO DE LA CARTE OPERATIONNELLE
*
 MUMS([" HAA"," HAA"," HAA"," HAA"])
************************************************************************........
*
 PANNEAU([0.00, 0.00])T

*
************************************************************************........
*             CARTE 500 MB (5301-5311) (TOURBILLON + HAUTEUR)
************************************************************************........
* 
*DIRECTIVES POUR LES ETIQUETTES DU TOURBILLON
* 
 OPTIONS(ISPSET,["RNDL",1., "RNDV",1.]) 
 OPTIONS(ISPSET,["ECOL",250000950, "LABL",6, "ILAB",1])
 OPTIONS(ISPSET,["MAJR",6, "MINR",6, "NULB",0])
*  
*DIRECTIVES POUR LES MAX-MIN DU TOURBILLON
* 
 OPTIONS(ISPSET,["HCAR","@F+","LCAR","@F-","CCAR"," ","LHCE",2,"NHL",3])
 OPTIONS(ISPSET,["HILO",9,"CNTR",9,"VALU",10]) 
* 
*DIRECTIVES POUR LES LIGNES DE CONTOUR DU TOURBILLON
*PRECISE POUR CHAQUE CHAMP LE FACTEUR MULTIPLICATIF ET INTERVALLE CONTOUR
*(ENONCE OBLIGATOIRE)  RECOMMANDE DE NE PAS MODIFIER SCAL EN COURS DE ROUTE
*
 SCAL("QQ",1.E5, 2.) 
 LIMITE=-10., 60. 
 DASH([0525B])
 OPTIONS(ISPSET,["NCRT",1])
*
* OFFM FAIT QUE LE MESSAGE EN BAS DU GRAPHIQUE EST ELIMINE
* MXVA ET MYVA PERMET D'ELIMINER LES MAX-MIN TROP RAPPROCHES
* ON PEUT FAIRE VARIER CES NOMBRES SELON LE BRUIT DU CHAMP
* NORMALEMENT ON UTILISE MXVA ET MYVA  =3 OU 4 (ici 2)
*             (resolution est ici de 3/8 po, donc examen sur 15/8 x 15/8)
*
 OPTIONS(ISPSET,["OFFM",1, "MXVA",3, "MYVA",3])
*
 VARIAN("QQ", -1,-1,500,6,-1,[GEO])
*
*------------------------------------------------------------------------------ 
*
*DIRECTIVES POUR LES LIGNES DE CONTOUR DES HAUTEURS
*PRECISE POUR CHAQUE CHAMP LE FACTEUR MULTIPLICATIF ET INTERVALLE CONTOUR
*(ENONCE OBLIGATOIRE)  RECOMMANDE DE NE PAS MODIFIER SCAL EN COURS DE ROUTE
*
 SCAL ("GZ",   1.0, 6.0)
* 
*DIRECTIVES POUR LES ETIQUETTES DES HAUTEURS
* 
 OPTIONS(ISPSET,["RNDL",1., "RNDV",1.])
 OPTIONS(ISPSET,["ECOL",350600850, "LABL",1, "ILAB",1])
 OPTIONS(ISPSET,["MAJR",1, "MINR",2, "NULB",3])
* 
*DIRECTIVES POUR LES MAX-MIN DES HAUTEURS
* 
 OPTIONS(ISPSET,["HCAR","@BH","LCAR","@BL","CCAR","@I9","LHCE",1,"NHL",3])
 OPTIONS(ISPSET,["HILO",3,"CNTR",4,"VALU",5]) 
* 
*DIRECTIVES POUR LES LIGNES DE CONTOUR DES HAUTEURS
*
 LIMITE=426., 666.
 DASH([1777B])
 OPTIONS(ISPSET,["NCRT",1])
*
* MXVA ET MYVA PERMET D'ELIMINER LES MAX-MIN TROP RAPPROCHES
* ON PEUT FAIRE VARIER CES NOMBRES SELON LE BRUIT DU CHAMP
*
 OPTIONS(ISPSET,["OFFM",1, "MXVA",6, "MYVA",6])
*
*
 VARIAN("GZ", -1,-1,500,6,-1,[GEO])
*------------------------------------------------------------------------------ 
*
* PREPARATION POUR LE LETTRAGE
* SETHPAL NECESSAIRE POUR PRODUIRE EFFACEMENT AVEC PATRON 1
*
 SETHPAL([0001001],1)
*------------------------------------------------------------------------------ 
*
* BOITE INFO DU COIN BAS-GAUCHE
*
 BOITE([LEFT,BOTTOM, .10,  .10], [LEFT, BOTTOM, 4.60, 1.25], 2, 1, 001)
 WRITE([LEFT,BOTTOM,  0.25,  1.05],26,
  'CMC CANADA  @STR(MUMS,A4)',["CNTR",02,"IN",1])
 WRITE([LEFT,BOTTOM,  0.25,  0.80],26,
  'TRIAL P6H GLOBAL',["CNTR",02,"IN",1])
 WRITE([LEFT,BOTTOM,  0.25,  0.55],26,
  'V@DAT(DATV,H,Z ,DW,-,JS, ,J, ,MON,-,MOI, ,A)',["CNTR",02,"IN",2,"SI",175])
 WRITE([LEFT,BOTTOM,  0.25,  0.30],26,
  ' $JOBNAME_WITH_SUFFIX',["CNTR",02,"IN",1])
*
*------------------------------------------------------------------------------ 
*
* BOITE DE LA LEGENDE EN HAUT
* LA CARTE FAIT 28.50po, ON UTILISERA 12.75 po POUR LA LEGENDE
* CE QUI LAISSE 7.5 po DE PART ET D'AUTRE
*
*
*                  8.5          15.25
*                   !             !
*
* ---------------7.5---9.5---------------21.00-----------28.50
*                !      !                 !               !
*     -0.25      !      !                 !                     -0.18 et -0.48
*     -0.65      !      !-----------------!               -0.55
*                !      !                 !                     -0.70
*-0.85           -------!-----------------!               -0.85
*     -0.97 -1.14!      !                 !                     -0.97 et -1.14
*-1.25           -------!-----------------!               -1.25
*
*
*                   !     !         !
*                  8.5   9.70     15.45
*                        9.75     15.50
*                        9.80     15.55
*       texte:          10.00     15.75
*
*
*
*                        9.70   13.45   17.20
*  3 champs:             9.75   13.50   17.25
*                        9.80   13.55   17.30
*       texte:          10.00   13.75   17.50
*
*
 BOITE([LEFT,TOP,7.50,-1.25],[LEFT,TOP,21.00, 0.00],2,1,001)
*
*TIRE UNE LIGNE A .55 et .85
*
 BOITE([LEFT,TOP, 9.50,-0.55],[LEFT,TOP,21.00,-0.55],2,1,000)
 BOITE([LEFT,TOP, 7.50,-0.85],[LEFT,TOP,21.00,-0.85],2,1,000)
*
* LIGNE VERTICALE POUR SEPARER TRIAL - ESSAI
*
 BOITE([LEFT,TOP, 9.50,-1.25],[LEFT,TOP, 9.50, 0.00],2,1,000)
*
 WRITE([LEFT,TOP, 8.50,-0.20],25, 'GLOBAL')
 WRITE([LEFT,TOP, 8.50,-0.60],26, 'TRIAL-ESSAI', ["SI",175,"IN",2])
*
*    retire cutoff lors conversion AN 2000
*
* WRITE([LEFT,TOP, 8.50,-0.97],28, 'CUTOFF-TOMBEE')
* WRITE([LEFT,TOP, 8.50,-1.14],28, '$JOBNAME_WITH_SUFFIX')
*
* HEURE DE VALIDITE
*
 WRITE([LEFT,TOP, 15.25, -0.40],29,
 'V@DAT(DATV,H,Z ,DW,-,JS, ,J, ,MON,-,MOI, ,A)')
*
* DESCRIPTION DE LA CARTE
*
 WRITE([LEFT,TOP,15.25,-0.18],29,
 '6H FORECAST - PREVISION 6@AH')
*
 WRITE([LEFT,TOP,15.25,-0.70],29,
 '500 @AHP@AA    HEIGHT-HAUTEUR    VORTICITY-TOURBILLON')
*
* EXPLICATION DU CONTENU DES CARTES 
*
 BOITE([LEFT,TOP, 9.70,-1.20],[LEFT,TOP, 9.70,-0.90],4,7,000)
 BOITE([LEFT,TOP, 9.80,-1.20],[LEFT,TOP, 9.80,-0.90],2,7,000)
 WRITE([LEFT,TOP,10.00,-0.97],28,
 'HEIGHT - HAUTEUR',["CNTR",02, "FC",7])
 WRITE([LEFT,TOP,10.00,-1.14],28,
 '@I9 @BH @BL ...540, 546, 552... @AD@AA@AM',["CNTR",02, "FC",7])
*
*SIMULE LIGNE POINTILLE
*
 BOITE([LEFT,TOP,15.50 ,-1.20],[LEFT,TOP,15.50,-1.15],1,2,000)
 BOITE([LEFT,TOP,15.50 ,-1.09],[LEFT,TOP,15.50,-1.03],1,2,000)
 BOITE([LEFT,TOP,15.50 ,-0.97],[LEFT,TOP,15.50,-0.91],1,2,000)
*
 WRITE([LEFT,TOP,15.750,-0.97],28,
 'VORTICITY - TOURBILLON',["CNTR",02, "FC",2])
 WRITE([LEFT,TOP,15.750,-1.14],28,
 '@F+ @F- ...8, 10, 12... (1.E-5) /@AS',["CNTR",02, "FC",2])
*
* ON REFAIT LE CADRE PARCE QUE LES INDICATIFS H L PEUVENT L'AVOIR EFFACE
* 
 BOITE([LEFT,BOTTOM, 0.00, 0.00],[RIGHT, BOTTOM, 0.00, 0.00],2,1,000)
 BOITE([LEFT,TOP, 0.00, 0.00],[RIGHT, TOP, 0.00, 0.00],2,1,000)
 BOITE([LEFT,BOTTOM, 0.00, 0.00],[LEFT, TOP, 0.00, 0.00],2,1,000)
 BOITE([RIGHT,BOTTOM, 0.00, 0.00],[RIGHT, TOP, 0.00, 0.00],2,1,000)
* 
**********************************************************************
 FRAME(0) 
**********************************************************************

EOFSIGMA
rm -f metacod
rm -f segfile

${TASK_BIN}/sigma  -imflds femap \
       -i sigmadir \
       -date ${RUN}

${TASK_BIN}/trames -device v100 \
       -format rrbx \
       -dn rrbx \
       -colors 0 1 1 1 1 1 1 1 \
       -orn rot -size 3000 -hx 2850 -hy 2100 -offset 0

if [ ${RUNHOUR} =  "00" ] ; then
   ${TASK_BIN}/ocxcarte -f plhaa00 -d plot -r ${SEQ_SHORT_DATE}
fi
if [ ${RUNHOUR} =  "06" ] ; then
   ${TASK_BIN}/ocxcarte -f plhaa06 -d plot -r ${SEQ_SHORT_DATE}
fi
if [ ${RUNHOUR} =  "12" ] ; then
   ${TASK_BIN}/ocxcarte -f plhaa12 -d plot -r ${SEQ_SHORT_DATE}
fi
if [ ${RUNHOUR} =  "18" ] ; then
   ${TASK_BIN}/ocxcarte -f plhaa18 -d plot -r ${SEQ_SHORT_DATE}
fi

${TASK_BIN}/trames -device difax \
       -format rrbx \
       -ncp 1 -colors 0 1 1 1 1 1 1 1 \
       -dn rrbx \
       -orn rot -size 2660 -offset 0

#if [ ${RUNHOUR} =  "06" ] ; then
#   ocxcarte  -t -f ca0469c -d difax -r ${SEQ_SHORT_DATE}
#fi
#if [ ${RUNHOUR} =  "18" ] ; then
#   ocxcarte  -t -f ca0468c -d difax -r ${SEQ_SHORT_DATE}
#fi

rm -f pgsmdir
rm -f sigmadir
rm -f femap
rm -f *QQQ*

