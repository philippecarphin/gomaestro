#!/bin/sh
#CMOI_LEVEL oprun
#CMOI_PLATFORM op_f
#host: op_f
#.*****************************************************************************
#.
#.     STATUS - OPERATIONAL - SEMI-ESSENTIAL
#.
#.     ORIGINATOR - CMDW - Marc Klasa - mars 2019
#.                  (Creer a partir des cartes pour l'Afghanistan)
#.
#.     PRODUCT - difax chart
#.     
#.
#. OBJECT: contourage de champs avec SIGMA pour l'Afrique
#.       necessite PGSM pour definir la fenetre affichee
#.       calcul de humidite moyenne a l aide de PGSM
#.              (champs nomme HR au niveau 701)
#.       utilise les champs en coordonnees de pression
#.              du modele global
#.
#.       1) 500mb hauteur-tourbillon
#.       2) PNM et epaisseur 1000-500
#.       3) 700mb hauteur et humidite relative 850-700-500
#.       4) 850mb hauteur, temperature
#.
###############################################################################


nom_fichier_pgsm=${TASK_INPUT}/fichier_pgsm
echo $nom_fichier_pgsm


###############################################################################
cat << EOFPGSM > pgsmdir1
#
* CARTES PREVUES DU MODELE GLOBAL - cas special Afrique
* 2 x 2 PANNEAUX AVEC UN CONTENU DIFFERENT
*******************************************************************************
* 16 janv 1992
* GILLES DESAUTELS (DDO)
*******************************************************************************
*------------------------------------------------------------------------------
*                                                                  PGSM
*------------------------------------------------------------------------------
 SORTIE(STD,30,R)
*
* pour eliminer les problemes causes par les extrapolations
*
 EXTRAP(VOISIN)
* Grille Afrique
 GRILLE(LATLON,77,57,-36.0,-20.0,1.5,1.0)
*
*
 COMPAC=-16 
*
 HEURE($FCT)
*
 CHAMP(EPAIS,1000,500)
 CHAMP(PNM)
 CHAMP('GZ',500,700,850)
 CHAMP('TT',500,700,850)
 CHAMP('QQ',500)

 CONV(HR,0.0,.44)
 CHAMP(HR,850)
 CONV(HR,0.0,.54)
 CHAMP(HR,500)
 CONV(HR,0.0,1.02)
 CHAMP(HR,700)
 LIRES(HR,-1,-1,850,$FCT,-1,-1)
 PLUSS(HR,-1,-1,500,$FCT,-1,-1)
 PLUSS(HR,-1,-1,700,$FCT,-1,-1)
 PFOIS(0.0,0.5,1.0)
 ECRITS(HR,-1,-1,701,$FCT,-1,-1,-1,-1,IMPRIM)

*
*------------------------------------------------------------------------------
*                                                                     FIN  PGSM
*------------------------------------------------------------------------------

EOFPGSM
#
#  execution du pgsm
#
rm -f femap
${TASK_BIN}/pgsm -iment $nom_fichier_pgsm \
                 -ozsrt femap \
                 -i pgsmdir1

###############################################################################
#
#******************************************************************************
#*********** CARTE 1 **********************************************************
#******************************************************************************
#

cat << EOFSIGMA > sigmadir1
*
* carte 2 panneaux du global (cas special afrique)
*
*******************************************************************************
* 20 janv 1992
* GILLES DESAUTELS (DDO)
*******************************************************************************
*                DIRECTIVES SIGMA
*                DIRECTIVES GLOBALES
*******************************************************************************
 CLIP = OUI
 NORMID = NON
 LISSAGE=OUI
*
*LA FONTE 15 CONTIENT LES MAJUSCULES, LA 16 LES MINUSCULES
*LA FONTE 10 CONTIENT LES LETTRES MAJUSCULES DES H,L
*LA FONTE 11 CONTIENT LES LETTRES MINUSCULES CORRESPONDANTES
*LA FONTE 17 CONTIENT LES CHIFFRES DES ETIQUETTES (FONTE ETROITE)
*LA FONTE  2 CONTIENT LES GRANDS + ET -
*LA FONTE  3 CONTIENT LES GRANDS LETTRES GRECQUES
*LA FONTE  9 CONTIENT LE CENTRE (X DANS O)
*
*   @         A  B  C  D  E  F  G  H  I
*   "CASE" 0  1  2  3  4  5  6  7  8  9
 SETFONT([15,16,10,11,12,13, 2, 3,17, 9])
*
* OPTIONS GEOGRAPHIQUES
*
 MAPLAB="ST",1,1,1,1,10,2,NON,OUI,NON,1,.5,OUI
 MAPCOLR=1
 OPTIONS(MAPOPT,["OU","GL","GR",-1.0,"LA","NO"]) 
 OPTIONS(DASHSET,["SMALL",16.0,"NP",1500])
*
# ECHELLE=17.0 
 ECHELLE=23.3 
 POUCES=30.0
*
 ECHLGRI=NON
*
 OPTIONS(ISPSET,["NCRT",1])
*
* SETHPAL NECESSAIRE POUR PRODUIRE EFFACEMENT AVEC PATRON 1
*                         COLORIER LE MOTIF 2 (=099) (VERT - TRANSPARENT)
*                         COLORIER LE MOTIF 3 (=109) (VERT - TRANSPARENT)
*                         COLORIER LE MOTIF 4 (=099) (VIOLET-TRANSPARENT)
*                         COLORIER LE MOTIF 5 (=109) (VIOLET-TRANSPARENT)
*                         COLORIER LE MOTIF 6 (=113) (VERT - TRANSPARENT)
*
*               1       2       3       4       5       6
 SETHPAL([0001001,0616099,0616109,0516099,0516109,0616113],6)
*
************************************************************************........
*                    GROUPE
************************************************************************........
* 
* GROUPE LOT=1   POUR LES ISOHYPSES DES HUMIDITES  1-majeur-mineur (TH=2)
*                COULEUR CO=6 (VERT)
*                BOITE FOND BLANC
*
 GROUPE(1,["CO",6,"TH",2,"CA",8,
   "FC",6,"BOX","YES","CLR","YES",
   "IN",1,"SI",175,"CN",34])
*
* GROUPE LOT=2 POUR LA TAILLE DES HL-Centre DES HUMIDITES (+ -)
* GROUPE LOT=3 POUR LA TAILLE DES valeurs   DES HUMIDITES
* les valeurs d'humidite ne sont pas marquees sur la carte
* la raison etant que l'on depasse les limites 0 et 100 a
* cause des interpolations, de plus les gens des operations
* n'en voient pas la necessite
*
*
 GROUPE(2,["SI",125,"IN",2,"FC",6,"ENH", -1  ,"CN",01]) 
 GROUPE(3,["SI",225,"IN",2,"FC",6,"ENH", -1  ,"CN",01,"CA",8]) 
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
* 
* GROUPE LOT=6-7   POUR LES ISOHYPSES DES HAUTEURS     6-majeur (TH=2)
*                  COULEUR CO=7 (BLEU)                 7-mineur (TH=2)
*                  BOITE FOND NOIR
*
 GROUPE(6,["CO",7,"TH",4,"CA",8,
           "FC",0,"BC",7,"CF","YES",
           "IN",1,"SI",175,"CN",34])
 GROUPE(7,["CO",7,"TH",2,"CA",8,
           "FC",0,"BC",7,"CF","YES",
           "IN",1,"SI",175,"CN",34])
*
* GROUPE LOT=8-9-10 POUR LA TAILLE DES MAX-MIN DES HAUTEURS  (H L)
* a noter in=3 pour avoir un H L completement plein
* a noter si=80 pour le centre, ce qui donne un rond plein (et petit)
*
 GROUPE( 8,["SI",600,"IN",3,"FC",7,"ENH","YES","CN",01]) 
 GROUPE( 9,["SI",125,"IN",2,"FC",7,"ENH","YES","CN",01]) 
 GROUPE(10,["SI",225,"IN",2,"FC",7,"ENH","YES","CN",01,"CA",8]) 
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*
* GROUPE LOT=11-15 POUR LE TOURBILLON                11-majeur (TH=1)
*                  COULEUR CO=2 (ROUGE)              12-mineur (TH=1)
*
 GROUPE(11,["CO",2,"TH",1,"CA",8,
            "FC",2, "BOX","YES","CLR","YES",
            "IN",1,"SI",200,"CN",34])
 GROUPE(12,["CO",2,"TH",1,"CA",8,
            "FC",2,"BOX","YES","CLR","YES",
            "IN",1,"SI",200,"CN",34])
*
* GROUPE LOT=14,15 POUR LES HAUTEURS DES MAX-MIN DU TOURBILLON ET + -
*        ON A MIS IN=1 POUR DONNER MOINS D'IMPORTANCE AU TOURBILLON
*        les dimensions sont aussi inferieures car si=120 plutot que 160
*
 GROUPE(13,["FC",2,"SI",600,"IN",1,"ENH", -1  ,"CN",01])
 GROUPE(14,["FC",2,"SI",110,"IN",1,"ENH","YES","CN",01])
*mb  GROUPE(15,["FC",2,"SI",160,"IN",1,"ENH","YES","CN",01,"CA",8])
 GROUPE(15,["FC",2,"SI",175,"IN",1,"ENH","YES","CN",01,"CA",8])
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
* 
* GROUPE LOT=16-20  POUR LES ISOHYPSES DES EPAISSEURS  16-majeur (TH=2)
*                   COULEUR CO=5 (VIOLET)              17-mineur (TH=1)
*                   BOITE FOND BLANC
*
 GROUPE(16,["CO",5,"TH",4,"CA",8,
            "FC",5,"BOX","YES","CLR","YES",
            "IN",1,"SI",175,"CN",34])
 GROUPE(17,["CO",5,"TH",2,"CA",8,
            "FC",5,"BOX","YES","CLR","YES",
            "IN",1,"SI",175,"CN",34])
*
* GROUPE LOT=18-19-20 POUR LA TAILLE DES MAX-MIN DES EPAISSEURS (+ -)
* les + et - avec ENH=-1 ne sont pas assez lisible
*
 GROUPE(18,["SI",600,"IN",3,"FC",5,"ENH","YES","CN",01]) 
 GROUPE(19,["SI",125,"IN",2,"FC",5,"ENH","YES","CN",01]) 
 GROUPE(20,["SI",225,"IN",2,"FC",5,"ENH","YES","CN",01,"CA",8]) 
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
* 
* GROUPE LOT=21-22  POUR DES ISOHYPSES DES TEMPERATURES  21-majeur (TH=2)
*                   COULEUR CO=5 (VIOLET)                22-mineur (TH=1)
*                   BOITE FOND BLANC
*
 GROUPE(21,["CO",5,"TH",3,"CA",8,
            "FC",5,"BOX","YES","CLR","YES",
            "IN",1,"SI",175,"CN",34])
 GROUPE(22,["CO",5,"TH",2,"CA",8,
            "FC",5, "BOX","YES","CLR","YES",
            "IN",1,"SI",175,"CN",34])
*
* GROUPE LOT=18-19-20 POUR LA TAILLE DES MAX-MIN DES TEMPERATURES (+ -)
* defini precedemment (meme que pour les epaisseurs)
*
* GROUPE(18,["SI",600,"IN",3,"FC",5,"ENH","YES","CN",01]) 
* GROUPE(19,["SI",125,"IN",2,"FC",5,"ENH","YES","CN",01]) 
* GROUPE(20,["SI",225,"IN",2,"FC",5,"ENH","YES","CN",01,"CA",8]) 
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*
* ECRITURE ID CARTE
*
* GROUPE 25 POUR TITRE REGIONAL - GLOBAL
* GROUPE 26 POUR ID DU CMC ET DESCRIPTION DU CONTENU (BOITE NORMALE)
*        27      HEURE DE VALIDITE et ecriture dans les zones ombragees
*        28      CONTENU DESCRIPTIF
*        29      TITRE DE LA CARTE
*
* "CA",0 => FONTE 15  (defaut)
* "CA",8 => FONTE  8  (etroit)
*
 GROUPE(25,["CA",0,"SI",400,"CW",225,"IN",2,"FC",1,"CN",01])
 GROUPE(26,["CA",0,"SI",100,"IN",1,"FC",1,"CN",01])
 GROUPE(27,["CA",0,"SI",110,"IN",1,"FC",1,"CN",01])
 GROUPE(28,["CA",0,"SI",100,"IN",1,"FC",1,"CN",01])
 GROUPE(29,["CA",0,"SI",175,"IN",2,"FC",1,"CN",01])
*
*
************************************************************************
*
* On trace la grande geographie du panneau a gauche
*
************************************************************************
*
* FENETRE = 1,  1, 190, 140
 PANNEAU ([0.00,0.00])
*
 SCAL("QQ",1.E5, 2.)
 LIMITE=-10., 60.
 OPTIONS(ISPSET,["NCRT",1])
 DASH([0525B])
*
 OPTIONS(ISPSET,["RNDL",1., "RNDV",1.])
 OPTIONS(ISPSET,["ECOL",000000000, "LABL",11, "ILAB",0])
 OPTIONS(ISPSET,["MAJR",11, "MINR",11, "NULB",99])
*
*DIRECTIVES POUR LES + -
*
 OPTIONS(ISPSET,["HCAR","@F+","LCAR","@F-","CCAR","@I9","LHCE",2,"NHL",3])
 OPTIONS(ISPSET,["HILO",14,"CNTR",14,"VALU",15])
 OPTIONS(ISPSET,["OFFM",1, "MXVA",3, "MYVA",3])
*
 VARIAN("QQ","P",-1,500, $FCT, 0,[GEO])
*-------------------------------------------------------------------------
*
* deja defini SCAL ("GZ",   1.0, 6.0)
 LIMITE=426., 666.
 OPTIONS(ISPSET,["NCRT",1])
 DASH([1777B])
*
 OPTIONS(ISPSET,["RNDL",1., "RNDV",1.])
 OPTIONS(ISPSET,["ECOL",150450750, "LABL",6, "ILAB",1])
 OPTIONS(ISPSET,["MAJR",6, "MINR",7, "NULB",3])
*
*DIRECTIVES POUR LES MAX-MIN DES HAUTEURS
*
 OPTIONS(ISPSET,["HCAR","@BH","LCAR","@BL","CCAR","@I9","LHCE",1,"NHL",3])
 OPTIONS(ISPSET,["HILO",8,"CNTR",9,"VALU",10])
 OPTIONS(ISPSET,["OFFM",1, "MXVA",4, "MYVA",4])
*
 VARIAN("GZ","P",-1,500, $FCT, 0,[GEO,PAN])
*
*------------------------------------------------------------------------------
*
 BOITE([LEFT,TOP, 0.0,-1.25],[LEFT,TOP,14.25, 0.00],2,1,001)
*
*TIRE UNE LIGNE A .55 et .85
*
 LIGNE([LEFT,TOP, 0.00,-0.55],[LEFT,TOP,14.25,-0.55],2,1)
 LIGNE([LEFT,TOP, 0.00,-0.85],[LEFT,TOP,14.25,-0.85],2,1)
*
* LIGNE VERTICALE POUR SEPARER REGIONAL-GLOBAL
*
* LIGNE([LEFT,TOP, 2.00,-1.25],[LEFT,TOP, 2.00, 0.00],2,1)
*
* HEURE DE VALIDITE
*
 WRITE([LEFT,TOP,  7.12, -0.40],29,
 'V@DAT(DATV,H,Z ,DW,-,JS, ,J, ,MON,-,MOI, ,A)',["IN",2])
*
*
* DESCRIPTION DE LA CARTE
*
 WRITE([LEFT,TOP, 7.12,-0.15],29,
 '${FCT}H FORECAST - PREVISION ${FCT}@AH',
  ["CNTR",01])
 WRITE([LEFT,TOP, 7.12,-0.70],29,
 '500 @AHP@AA   HEIGHT-HAUTEUR   VORTICITY-TOURBILLON',
 ["CNTR",01])
*
* EXPLICATION DU CONTENU DES CARTES
* hauteur a 500mb
*
 LIGNE([LEFT,TOP, 0.20,-1.20 ],[LEFT,TOP,0.20,-0.90 ],2,7)
 LIGNE([LEFT,TOP, 0.30,-1.20 ],[LEFT,TOP,0.30,-0.90 ],1,7)
*
 WRITE([LEFT,TOP, 0.50,-0.97 ],28, 'HEIGHT - HAUTEUR',
 ["CNTR",02, "FC",7])
 WRITE([LEFT,TOP, 0.50,-1.14],28,
 '@I9 @BH @BL ...540, 546, 552... @AD@AA@AM',["CNTR",02, "FC",7])
*
*SIMULE LIGNE POINTILLE (pour tourbillon couleur=rouge =2)
*
 LIGNE([LEFT,TOP, 7.12,-1.20],[LEFT,TOP, 7.12,-1.15],1,2)
 LIGNE([LEFT,TOP, 7.12,-1.09],[LEFT,TOP, 7.12,-1.03],1,2)
 LIGNE([LEFT,TOP, 7.12,-0.97],[LEFT,TOP, 7.12,-0.91],1,2)
*
 WRITE([LEFT,TOP, 7.37, -0.97],28,
 'VORTICITY - TOURBILLON',["CNTR",02, "FC",2])
 WRITE([LEFT,TOP, 7.37,-1.14],28,
 '@F+ @F- ...8, 10, 12... (1.E-5) /@AS',["CNTR",02, "FC",2])
*
* on refait le cadre de la boite
*
 LIGNE([LEFT,BOTTOM, 0.00, 0.0],[RIGHT, BOTTOM, 0.00, 0.00],2,1)
 LIGNE([LEFT,TOP, 0.00, 0.00],[RIGHT, TOP, 0.00, 0.00],2,1)
 LIGNE([LEFT,BOTTOM, 0.00, 0.00],[LEFT, TOP, 0.00, 0.00],2,1)
 LIGNE([RIGHT,BOTTOM, 0.00, 0.00],[RIGHT, TOP, 0.00, 0.000],2,1)
*
*
*
*
*------------------------------------------------------------------------------
* ID DU CMC ET HEURE DE VALIDITE A GAUCHE
*
# MUMS(["${mums1}","${mums1}","${mums1}","${mums1}"])
*
*------------------------------------------------------------------------------
*
* BOITE INFO DU COIN BAS-GAUCHE  (version reduite) (imprime 1 seule fois)
*
 BOITE([LEFT,BOTTOM,0.05,0.05],[LEFT,BOTTOM, 3.2, 1.25],2,1,001)
*
 WRITE([LEFT,BOTTOM,  0.15,  0.50],26,
 '${FCT}@AH AFRICA - AFRIQUE  @STR(MUMS,A4)',["CNTR",12,"IN",2])
 WRITE([LEFT,BOTTOM,  0.15,  0.30],26,
 '@DAT(DATS,H,Z ,DW,-,JS, ,J, ,MON,-,MOI, ,A)',["CNTR",12,"IN",2])
 WRITE([LEFT,BOTTOM,  0.15,  0.10],26,
 ' $JOBNAME',["CNTR",12,"IN",2])
*
* il faut utiliser la meme description de fontes que celle du
* programme fortran drapeau.f qui a genere le segment 'DRAPO'
* a savoir: DATA NEWFONT/15,16,2,3,4,5,6,7,8,9/
*
 SETFONT([15,16, 2, 3, 4, 5, 6, 7, 8, 9])
 SEGMENT ('DRAPO',140,625, 2, 9, 0, 0, 0)
*  et par la suite revenir aux valeurs originales
 SETFONT([15,16,10,11,12,13, 2, 3,17, 9])
 SETHPAL([0001001,0616099,0616109,0516099,0516109,0616113],6)
*
*
*
*
************************************************************************
*
* On trace la grande geographie du panneau a droite  (pnm  et  1000-5000)
*
************************************************************************
*
* FENETRE= 1, 1, 190, 140
 PANNEAU([14.25, 0.00])
*
 SCAL ("DZ",   1.0, 6.0)
 LIMITE=426., 666.
 OPTIONS(ISPSET,["NCRT",2])
 DASH([0525B])
*
 NIVEAUX = 534., 540.
 MOTIFS =000,005,000
 HAFTON("DZ","P",-1,1000,500, $FCT,[GEO,NIV,PAT])
*
 OPTIONS(ISPSET,["RNDL",1., "RNDV",1.])
 OPTIONS(ISPSET,["ECOL",250550850, "LABL",16, "ILAB",1])
 OPTIONS(ISPSET,["MAJR",17, "MINR",17, "NULB",3])
*
*DIRECTIVES POUR LES MAX-MIN DES EPAISSEURS (+ - non requis selon JGDesmarais)
*
* OPTIONS(ISPSET,["HCAR","@F+","LCAR","@F-","CCAR"," ","LHCE",2,"NHL",3])
 OPTIONS(ISPSET,["HCAR"," ","LCAR"," ","CCAR"," ","LHCE",2,"NHL",3])
*
 OPTIONS(ISPSET,["HILO",20,"CNTR",19,"VALU",20])
 OPTIONS(ISPSET,["OFFM",1, "MXVA",4, "MYVA",4])
*
 VARIAN("DZ","P",-1,1000,500, $FCT,[GEO])
*
 SCAL ("PN",   1.0, 4.0)
 LIMITE=808., 1192.
 OPTIONS(ISPSET,["NCRT",1])
 DASH([1777B])
*
 OPTIONS(ISPSET,["RNDL",1., "RNDV",1.])
 OPTIONS(ISPSET,["ECOL",150450750, "LABL",6, "ILAB",1])
 OPTIONS(ISPSET,["MAJR",6, "MINR",7, "NULB",5])
*
*DIRECTIVES POUR LES MAX-MIN DES PRESSIONS PNM
*
 OPTIONS(ISPSET,["HCAR","@BH","LCAR","@BL","CCAR","@I9","LHCE",1,"NHL",4])
 OPTIONS(ISPSET,["HILO", 8,"CNTR", 9,"VALU",10])
 OPTIONS(ISPSET,["OFFM",1, "MXVA",4, "MYVA",4])
*
 VARIAN("PN","P",-1, 0,$FCT, 0,[GEO,PAN])
*
 BOITE([LEFT,TOP, 0.0,-1.25],[LEFT,TOP,14.25, 0.00],2,1,001)
*
*TIRE UNE LIGNE A .55 et .85
*
 LIGNE([LEFT,TOP, 0.00,-0.55],[LEFT,TOP,14.25,-0.55],2,1)
 LIGNE([LEFT,TOP, 0.00,-0.85],[LEFT,TOP,14.25,-0.85],2,1)
*
* LIGNE VERTICALE POUR SEPARER REGIONAL-GLOBAL
*
* LIGNE([LEFT,TOP, 2.00,-1.25],[LEFT,TOP, 2.00, 0.00],2,1)
*
* HEURE DE VALIDITE
*
 WRITE([LEFT,TOP,  7.12, -0.40],29,
 'V@DAT(DATV,H,Z ,DW,-,JS, ,J, ,MON,-,MOI, ,A)',["IN",2])
*
* DESCRIPTION DE LA CARTE
*
 WRITE([LEFT,TOP, 7.12,-0.15],29,
 '${FCT}H FORECAST - PREVISION ${FCT}@AH',
 ["CNTR",01])
 WRITE([LEFT,TOP, 7.12,-0.70],29,
 'MSL-PNM   1000-500 @AHP@AA THICKNESS-EPAISSEUR',
 ["CNTR",01])
*
* EXPLICATION DU CONTENU DES CARTES
* hauteur a PNM  et  1000-500
*
 LIGNE([LEFT,TOP, 0.20,-1.20 ],[LEFT,TOP,0.20,-0.90 ],2,7)
 LIGNE([LEFT,TOP, 0.30,-1.20 ],[LEFT,TOP,0.30,-0.90 ],1,7)
*
 WRITE([LEFT,TOP, 0.50,-0.97 ],28, 'MSL - PNM',
 ["CNTR",02, "FC",7])
 WRITE([LEFT,TOP, 0.50,-1.14],28,
 '@I9 @BH @BL ...996, 1000, 1004... @AHP@AA',["CNTR",02, "FC",7])
*
*SIMULE LIGNE POINTILLE (pour epaisseurs couleur=violet=5)
*
 LIGNE([LEFT,TOP, 7.12,-1.20],[LEFT,TOP, 7.12,-1.15],1,5)
 LIGNE([LEFT,TOP, 7.12,-1.09],[LEFT,TOP, 7.12,-1.03],1,5)
 LIGNE([LEFT,TOP, 7.12,-0.97],[LEFT,TOP, 7.12,-0.91],1,5)
*
 WRITE([LEFT,TOP, 7.37, -0.97],28,
 'THICKNESS - EPAISSEUR',["CNTR",02, "FC",5])
 WRITE([LEFT,TOP, 7.37,-1.14],28,
 '@F+ @F-  ...540, 546, 552... @AD@AA@AM',["CNTR",02, "FC",5])
*
* BOITE([LEFT,TOP, 8.00,-1.20],[LEFT,TOP, 9.00,-0.90],1,5,004)
**mb  BOITE([LEFT,TOP, 8.00,-1.20],[LEFT,TOP,10.00,-0.90],1,5,005)
 BOITE([LEFT,TOP, 10.57,-1.20],[LEFT,TOP,12.22,-0.90],1,5,005)
**mb  WRITE([LEFT,TOP, 9.00,-1.05],27,'534@A< @FD@FF @A<540',
 WRITE([LEFT,TOP, 11.42,-1.05],27,'534@A< @FD@FF @A<540',
 ["CNTR",01,"FC",5,"IN",2,"ENH","YES"])
*
* ON REFAIT LE CADRE PARCE QUE LES INDICATIFS H L PEUVENT L'AVOIR EFFACE
*
 LIGNE([LEFT,BOTTOM, 0.00, 0.0],[RIGHT, BOTTOM, 0.00, 0.00],2,1)
 LIGNE([LEFT,TOP, 0.00, 0.00],[RIGHT, TOP, 0.00, 0.00],2,1)
 LIGNE([LEFT,BOTTOM, 0.00, 0.00],[LEFT, TOP, 0.00, 0.00],2,1)
 LIGNE([RIGHT,BOTTOM, 0.00, 0.00],[RIGHT, TOP, 0.00, 0.000],2,1)
*
*
**********************************************************************
 FRAME(0) 
*
EOFSIGMA
#
#*********************************************************************
#*********** CARTE 2 *************************************************
#*********************************************************************
#
cat << EOFSIGMA > sigmadir2
*
* carte 2 panneaux du global (cas special afrique)
*
*******************************************************************************
* 20 janv 1992
* GILLES DESAUTELS (DDO)
*******************************************************************************
*                DIRECTIVES SIGMA
*                DIRECTIVES GLOBALES
*******************************************************************************
 CLIP = OUI
 NORMID = NON
 LISSAGE=OUI
*
*LA FONTE 15 CONTIENT LES MAJUSCULES, LA 16 LES MINUSCULES
*LA FONTE 10 CONTIENT LES LETTRES MAJUSCULES DES H,L
*LA FONTE 11 CONTIENT LES LETTRES MINUSCULES CORRESPONDANTES
*LA FONTE 17 CONTIENT LES CHIFFRES DES ETIQUETTES (FONTE ETROITE)
*LA FONTE  2 CONTIENT LES GRANDS + ET -
*LA FONTE  3 CONTIENT LES GRANDS LETTRES GRECQUES
*LA FONTE  9 CONTIENT LE CENTRE (X DANS O)
*
*   @         A  B  C  D  E  F  G  H  I
*   "CASE" 0  1  2  3  4  5  6  7  8  9
 SETFONT([15,16,10,11,12,13, 2, 3,17, 9])
*
* OPTIONS GEOGRAPHIQUES
*
 MAPLAB="ST",1,1,1,1,10,2,NON,OUI,NON,1,.5,OUI
 MAPCOLR=1
 OPTIONS(MAPOPT,["OU","GL","GR",-1.0,"LA","NO"]) 
 OPTIONS(DASHSET,["SMALL",16.0,"NP",1500])
*
# ECHELLE=17.0 
 ECHELLE=23.3 
 POUCES=30.0
*
 ECHLGRI=NON
*
 OPTIONS(ISPSET,["NCRT",1])
*
* SETHPAL NECESSAIRE POUR PRODUIRE EFFACEMENT AVEC PATRON 1
*                         COLORIER LE MOTIF 2 (=099) (VERT - TRANSPARENT)
*                         COLORIER LE MOTIF 3 (=109) (VERT - TRANSPARENT)
*                         COLORIER LE MOTIF 4 (=099) (VIOLET-TRANSPARENT)
*                         COLORIER LE MOTIF 5 (=109) (VIOLET-TRANSPARENT)
*                         COLORIER LE MOTIF 6 (=113) (VERT - TRANSPARENT)
*
*               1       2       3       4       5       6
 SETHPAL([0001001,0616099,0616109,0516099,0516109,0616113],6)
*
************************************************************************........
*                    GROUPE
************************************************************************........
* 
* GROUPE LOT=1   POUR LES ISOHYPSES DES HUMIDITES  1-majeur-mineur (TH=2)
*                COULEUR CO=6 (VERT)
*                BOITE FOND BLANC
*
 GROUPE(1,["CO",6,"TH",2,"CA",8,
   "FC",6,"BOX","YES","CLR","YES",
   "IN",1,"SI",175,"CN",34])
*
* GROUPE LOT=2 POUR LA TAILLE DES HL-Centre DES HUMIDITES (+ -)
* GROUPE LOT=3 POUR LA TAILLE DES valeurs   DES HUMIDITES
* les valeurs d'humidite ne sont pas marquees sur la carte
* la raison etant que l'on depasse les limites 0 et 100 a
* cause des interpolations, de plus les gens des operations
* n'en voient pas la necessite
*
*
 GROUPE(2,["SI",125,"IN",2,"FC",6,"ENH", -1  ,"CN",01]) 
 GROUPE(3,["SI",225,"IN",2,"FC",6,"ENH", -1  ,"CN",01,"CA",8]) 
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
* 
* GROUPE LOT=6-7   POUR LES ISOHYPSES DES HAUTEURS     6-majeur (TH=2)
*                  COULEUR CO=7 (BLEU)                 7-mineur (TH=2)
*                  BOITE FOND NOIR
*
 GROUPE(6,["CO",7,"TH",4,"CA",8,
           "FC",0,"BC",7,"CF","YES",
           "IN",1,"SI",175,"CN",34])
 GROUPE(7,["CO",7,"TH",2,"CA",8,
           "FC",0,"BC",7,"CF","YES",
           "IN",1,"SI",175,"CN",34])
*
* GROUPE LOT=8-9-10 POUR LA TAILLE DES MAX-MIN DES HAUTEURS  (H L)
* a noter in=3 pour avoir un H L completement plein
* a noter si=80 pour le centre, ce qui donne un rond plein (et petit)
*
 GROUPE( 8,["SI",600,"IN",3,"FC",7,"ENH","YES","CN",01]) 
 GROUPE( 9,["SI",125,"IN",2,"FC",7,"ENH","YES","CN",01]) 
 GROUPE(10,["SI",225,"IN",2,"FC",7,"ENH","YES","CN",01,"CA",8]) 
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*
* GROUPE LOT=11-15 POUR LE TOURBILLON                11-majeur (TH=1)
*                  COULEUR CO=2 (ROUGE)              12-mineur (TH=1)
*
 GROUPE(11,["CO",2,"TH",1,"CA",8,
            "FC",2, "BOX","YES","CLR","YES",
            "IN",1,"SI",200,"CN",34])
 GROUPE(12,["CO",2,"TH",1,"CA",8,
            "FC",2,"BOX","YES","CLR","YES",
            "IN",1,"SI",200,"CN",34])
*
* GROUPE LOT=14,15 POUR LES HAUTEURS DES MAX-MIN DU TOURBILLON ET + -
*        ON A MIS IN=1 POUR DONNER MOINS D'IMPORTANCE AU TOURBILLON
*        les dimensions sont aussi inferieures car si=120 plutot que 160
*
 GROUPE(13,["FC",2,"SI",600,"IN",1,"ENH", -1  ,"CN",01])
 GROUPE(14,["FC",2,"SI",110,"IN",1,"ENH","YES","CN",01])
*mb  GROUPE(15,["FC",2,"SI",160,"IN",1,"ENH","YES","CN",01,"CA",8])
 GROUPE(15,["FC",2,"SI",175,"IN",1,"ENH","YES","CN",01,"CA",8])
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
* 
* GROUPE LOT=16-20  POUR LES ISOHYPSES DES EPAISSEURS  16-majeur (TH=2)
*                   COULEUR CO=5 (VIOLET)              17-mineur (TH=1)
*                   BOITE FOND BLANC
*
 GROUPE(16,["CO",5,"TH",4,"CA",8,
            "FC",5,"BOX","YES","CLR","YES",
            "IN",1,"SI",175,"CN",34])
 GROUPE(17,["CO",5,"TH",2,"CA",8,
            "FC",5,"BOX","YES","CLR","YES",
            "IN",1,"SI",175,"CN",34])
*
* GROUPE LOT=18-19-20 POUR LA TAILLE DES MAX-MIN DES EPAISSEURS (+ -)
* les + et - avec ENH=-1 ne sont pas assez lisible
*
 GROUPE(18,["SI",600,"IN",3,"FC",5,"ENH","YES","CN",01]) 
 GROUPE(19,["SI",125,"IN",2,"FC",5,"ENH","YES","CN",01]) 
 GROUPE(20,["SI",225,"IN",2,"FC",5,"ENH","YES","CN",01,"CA",8]) 
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
* 
* GROUPE LOT=21-22  POUR DES ISOHYPSES DES TEMPERATURES  21-majeur (TH=2)
*                   COULEUR CO=5 (VIOLET)                22-mineur (TH=1)
*                   BOITE FOND BLANC
*
 GROUPE(21,["CO",5,"TH",3,"CA",8,
            "FC",5,"BOX","YES","CLR","YES",
            "IN",1,"SI",175,"CN",34])
 GROUPE(22,["CO",5,"TH",2,"CA",8,
            "FC",5, "BOX","YES","CLR","YES",
            "IN",1,"SI",175,"CN",34])
*
* GROUPE LOT=18-19-20 POUR LA TAILLE DES MAX-MIN DES TEMPERATURES (+ -)
* defini precedemment (meme que pour les epaisseurs)
*
* GROUPE(18,["SI",600,"IN",3,"FC",5,"ENH","YES","CN",01]) 
* GROUPE(19,["SI",125,"IN",2,"FC",5,"ENH","YES","CN",01]) 
* GROUPE(20,["SI",225,"IN",2,"FC",5,"ENH","YES","CN",01,"CA",8]) 
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*
* ECRITURE ID CARTE
*
* GROUPE 25 POUR TITRE REGIONAL - GLOBAL
* GROUPE 26 POUR ID DU CMC ET DESCRIPTION DU CONTENU (BOITE NORMALE)
*        27      HEURE DE VALIDITE et ecriture dans les zones ombragees
*        28      CONTENU DESCRIPTIF
*        29      TITRE DE LA CARTE
*
* "CA",0 => FONTE 15  (defaut)
* "CA",8 => FONTE  8  (etroit)
*
 GROUPE(25,["CA",0,"SI",400,"CW",225,"IN",2,"FC",1,"CN",01])
 GROUPE(26,["CA",0,"SI",100,"IN",1,"FC",1,"CN",01])
 GROUPE(27,["CA",0,"SI",110,"IN",1,"FC",1,"CN",01])
 GROUPE(28,["CA",0,"SI",100,"IN",1,"FC",1,"CN",01])
 GROUPE(29,["CA",0,"SI",175,"IN",2,"FC",1,"CN",01])
*
*
*
*
************************************************************************
******************************************************* GAUCHE *********
******************************************************* 700mb      *****
************************************************************************
 PANNEAU([0.0, 0.0]) 
* FENETRE = 1,  1, 190, 140
*
* OFFM FAIT QUE LE MESSAGE EN BAS DU GRAPHIQUE EST ELIMINE
* MXVA ET MYVA PERMET D'ELIMINER LES MAX-MIN TROP RAPPROCHES
* ON PEUT FAIRE VARIER CES NOMBRES SELON LE BRUIT DU CHAMP
*                                        et la distance de grille
*
* POUR UNE CARTE 1:40M distance = 0.075 po 
*      donc la distance minimum entre 2 H ou 2 L sera de
*       8 * .075 po = 0.60 po  si MXVA,MYVA=4
*      12 * .075 po = 0.90 po  si MXVA,MYVA=6
*
* OPTIONS(ISPSET,["OFFM",1, "MXVA",4, "MYVA",4])
************************************************************************........
*DIRECTIVES POUR LES LIGNES DE CONTOUR DES HUMIDITES
*
* le champ a 500  est bien multiplie par .27 
*            700                         .51
*            850                         .22
*                                      -----
*                                       1.00
*
* dans la directive SCAL qui suit on a un facteur 1 pour un domaine 0-100
*
 SCAL("HR", 100., 20.)
 LIMITE =10.0,  90.0
 OPTIONS(ISPSET,["NCRT",2])
 DASH([0525B])
*
 OPTIONS(ISPSET,["RNDL",1., "RNDV",1.])
 OPTIONS(ISPSET,["ECOL",250550850, "LABL",1, "ILAB",1])
 OPTIONS(ISPSET,["MAJR",1, "MINR",1, "NULB",99])
*
*DIRECTIVES POUR LES MAX-MIN DES HUMIDITES
*           (on ne les trace pas)
*
* OPTIONS(ISPSET,["HCAR","@F+","LCAR","@F-","CCAR"," ","LHCE",2,"NHL",3])
 OPTIONS(ISPSET,["HCAR"," ","LCAR"," ","CCAR"," ","LHCE",2,"NHL",3])
 OPTIONS(ISPSET,["HILO",2,"CNTR",2,"VALU", 3])
 OPTIONS(ISPSET,["OFFM",1, "MXVA",4, "MYVA",4])
*
 NIVEAUX=70.0,90.0
 MOTIFS=000, 002, 000
 HAFTON("HR","P",-1,701, $FCT, 0,[NIV,PAT,GEO])
*
 NIVEAUX=90.0
 MOTIFS=000, 003
 HAFTON("HR","P",-1,701, $FCT, 0,[NIV,PAT,GEO])
*
 VARIAN("HR","P",-1,701, $FCT, 0,[GEO])
*-------------------------------------------------------------------------
*
 SCAL ("GZ",   1.0, 6.0)
 LIMITE=180., 420.
 OPTIONS(ISPSET,["NCRT",1])
 DASH([1777B])
*
 OPTIONS(ISPSET,["RNDL",1., "RNDV",1.])
 OPTIONS(ISPSET,["ECOL",150450750, "LABL",6, "ILAB",1])
 OPTIONS(ISPSET,["MAJR",6, "MINR",7, "NULB",3])
*
*DIRECTIVES POUR LES MAX-MIN DES HAUTEURS
*
 OPTIONS(ISPSET,["HCAR","@BH","LCAR","@BL","CCAR","@I9","LHCE",1,"NHL",3])
 OPTIONS(ISPSET,["HILO",8,"CNTR",9,"VALU",10])
 OPTIONS(ISPSET,["OFFM",1, "MXVA",4, "MYVA",4])
*
 VARIAN("GZ","P",-1,700, $FCT, 0,[GEO,PAN])
*
* ON REFAIT LE CADRE PARCE QUE LES INDICATIFS H L PEUVENT L'AVOIR EFFACE
*
 LIGNE([LEFT,BOTTOM, 0.00, 0.0],[RIGHT, BOTTOM, 0.00, 0.00],2,1)
 LIGNE([LEFT,TOP, 0.00, 0.00],[RIGHT, TOP, 0.00, 0.00],2,1)
 LIGNE([LEFT,BOTTOM, 0.00, 0.00],[LEFT, TOP, 0.00, 0.00],2,1)
 LIGNE([RIGHT,BOTTOM, 0.00, 0.00],[RIGHT, TOP, 0.00, 0.000],2,1)
*
*
*******************************************************************************
* BOITE DE LA LEGENDE EN HAUT
* LA CARTE FAIT 10.50po, ON UTILISERA 10.50 po POUR LA LEGENDE
*
*                  1.0           5.25
*                   !             !
*
*                0.0---2.0------------10.50
*                !      !                 !             
*     -0.25      !      !                 !                     -0.18 et -0.48
*     -0.65      !      !-----------------!               -0.55
*                !      !                 !                     -0.70
*-0.85           -------!-----------------!               -0.85
*     -0.97 -1.14!      !                 !                     -0.97 et -1.14
*-1.25           -------!-----------------!               -1.25
*
*
*                   !     !         !
*                  1.0   0.20      8.20
*                        0.25      8.25
*                        0.30      8.30
*       texte:           2.50      8.50
*
*
*
*                        0.20    6.20   10.20
*  3 champs:             0.25    6.25   10.25
*                        0.30    6.30   10.30
*       texte:           2.50    6.50   10.50
*******************************************************************************
*
 BOITE([LEFT,TOP, 0.0,-1.25],[LEFT,TOP,14.25, 0.00],2,1,001)
*
*TIRE UNE LIGNE A .55 et .85
*
 LIGNE([LEFT,TOP, 0.00,-0.55],[LEFT,TOP,14.25,-0.55],2,1)
 LIGNE([LEFT,TOP, 0.00,-0.85],[LEFT,TOP,14.25,-0.85],2,1)
*
* LIGNE VERTICALE POUR SEPARER REGIONAL-GLOBAL
*
* LIGNE([LEFT,TOP, 2.00,-1.25],[LEFT,TOP, 2.00, 0.00],2,1)
*
* HEURE DE VALIDITE
*
 WRITE([LEFT,TOP,  7.12, -0.40],29,
 'V@DAT(DATV,H,Z ,DW,-,JS, ,J, ,MON,-,MOI, ,A)',["IN",2])
*
* DESCRIPTION DE LA CARTE
*
 WRITE([LEFT,TOP, 7.12,-0.15],29,
 '${FCT}H FORECAST - PREVISION ${FCT}@AH',
 ["CNTR",01])
 WRITE([LEFT,TOP, 7.12,-0.70],29,
 '700 @AHP@AA HEIGHT-HAUTEUR   850-700-500 @AHP@AA HUMIDITY-HUMIDITE',
 ["CNTR",01])
*
* EXPLICATION DU CONTENU DES CARTES
* hauteur a 700mb
*
* ligne verticale epaisse et mince
*
 LIGNE([LEFT,TOP, 0.20,-1.20 ],[LEFT,TOP,0.20,-0.90 ],2,7)
 LIGNE([LEFT,TOP, 0.30,-1.20 ],[LEFT,TOP,0.30,-0.90 ],1,7)
*
 WRITE([LEFT,TOP, 0.50,-0.97 ],28, 'HEIGHT - HAUTEUR',
 ["CNTR",02, "FC",7])
 WRITE([LEFT,TOP, 0.50,-1.14],28,
 '@I9 @BH @BL ...294, 300, 306... @AD@AA@AM',["CNTR",02, "FC",7])
*
* EXPLICATION DU CONTENU DES CARTES
* humidite
*
 WRITE([LEFT,TOP, 7.37, -0.97 ],28, 'HUMIDITY - HUMIDITE',
 ["CNTR",02, "FC",6])
*
* on a decide de ne pas ecrire les valeurs extremes
*
*
 WRITE([LEFT,TOP, 7.37,-1.14],28,
 '10, 30, 50, 70, 90 %',["CNTR",02, "FC",6])
*
*SIMULE LIGNE POINTILLE MINCE (pour humidite)
*
 LIGNE([LEFT,TOP, 7.12,-1.20],[LEFT,TOP, 7.12,-1.15],1,6)
 LIGNE([LEFT,TOP, 7.12,-1.09],[LEFT,TOP, 7.12,-1.03],1,6)
 LIGNE([LEFT,TOP, 7.12,-0.97],[LEFT,TOP, 7.12,-0.91],1,6)
*
* ajoute boite montrant les motifs (motif 2 et motif 3)
*
 BOITE([LEFT,TOP, 9.87,-1.20 ],[LEFT,TOP, 10.87,-0.90 ],2,6,002)
 BOITE([LEFT,TOP, 10.87,-1.20 ],[LEFT,TOP, 11.87,-0.90 ],2,6,003)
*
 WRITE([LEFT,TOP, 10.37,-1.05 ],27, '70', ["EN","YES","IN",01, "FC",6])
 WRITE([LEFT,TOP, 11.37,-1.05 ],27, '90', ["EN","YES","IN",01, "FC",6])
*
*------------------------------------------------------------------------------
* ID DU CMC ET HEURE DE VALIDITE A GAUCHE
*
# MUMS(["${mums2}","${mums2}","${mums2}","${mums2}"])
*
*------------------------------------------------------------------------------
*
* BOITE INFO DU COIN BAS-GAUCHE  (version reduite) (imprime 1 seule fois)
*
 BOITE([LEFT,BOTTOM,0.05,0.05],[LEFT,BOTTOM, 3.2, 1.25],2,1,001)
*
 WRITE([LEFT,BOTTOM,  0.15,  0.50],26,
 '${FCT}@AH AFRICA - AFRIQUE  @STR(MUMS,A4)',["CNTR",12,"IN",2])
 WRITE([LEFT,BOTTOM,  0.15,  0.30],26,
 '@DAT(DATS,H,Z ,DW,-,JS, ,J, ,MON,-,MOI, ,A)',["CNTR",12,"IN",2])
 WRITE([LEFT,BOTTOM,  0.15,  0.10],26,
 ' $JOBNAME',["CNTR",12,"IN",2])
*
* il faut utiliser la meme description de fontes que celle du
* programme fortran drapeau.f qui a genere le segment 'DRAPO'
* a savoir: DATA NEWFONT/15,16,2,3,4,5,6,7,8,9/
*
 SETFONT([15,16, 2, 3, 4, 5, 6, 7, 8, 9])
 SEGMENT ('DRAPO',140,625, 2, 9, 0, 0, 0)
*  et par la suite revenir aux valeurs originales
 SETFONT([15,16,10,11,12,13, 2, 3,17, 9])
 SETHPAL([0001001,0616099,0616109,0516099,0516109,0616113],6)
*
*
*
*
************************************************************************
*
* On trace la grande geographie du panneau a droite  (hauteur - temperature a 850)
*
************************************************************************
*
* FENETRE = 1, 1, 190, 140
 PANNEAU ([14.25, 0.00])
*
 SCAL ("TT",   1.0, 5.0)
 LIMITE=+70., -70.
 OPTIONS(ISPSET,["NCRT",2])
 DASH([0525B])
*
 NIVEAUX = -2.5, +2.5
 MOTIFS =000,005,000
 HAFTON("TT","P",-1, 850,$FCT, 0,[GEO,NIV,PAT])
*
 OPTIONS(ISPSET,["RNDL",1., "RNDV",1.])
 OPTIONS(ISPSET,["ECOL",250550850, "LABL",21, "ILAB",1])
 OPTIONS(ISPSET,["MAJR",21, "MINR",22, "NULB",13])
*
*DIRECTIVES POUR LES MAX-MIN DES TEMPERATURES
*
 OPTIONS(ISPSET,["HCAR","@F+","LCAR","@F-","CCAR"," ","LHCE",2,"NHL",3])
 OPTIONS(ISPSET,["HILO",20,"CNTR",19,"VALU",20])
 OPTIONS(ISPSET,["OFFM",1, "MXVA",4, "MYVA",4])
*
 VARIAN("TT","P",-1, 850, $FCT, 0,[GEO])
*
*
* deja defini  SCAL ("GZ",   1.0, 6.0)
 LIMITE= 54.,  246.
 OPTIONS(ISPSET,["NCRT",1])
 DASH([1777B])
*
 OPTIONS(ISPSET,["RNDL",1., "RNDV",1.])
 OPTIONS(ISPSET,["ECOL",150450750, "LABL",6, "ILAB",1])
 OPTIONS(ISPSET,["MAJR",6, "MINR",7, "NULB",3])
*
*DIRECTIVES POUR LES MAX-MIN DES PRESSIONS PNM
*
 OPTIONS(ISPSET,["HCAR","@BH","LCAR","@BL","CCAR","@I9","LHCE",1,"NHL",4])
 OPTIONS(ISPSET,["HILO", 8,"CNTR", 9,"VALU",10])
 OPTIONS(ISPSET,["OFFM",1, "MXVA",4, "MYVA",4])
*
 VARIAN("GZ","P",-1,850, $FCT, 0,[GEO,PAN])
*
*------------------------------------------------------------------------------
*
 BOITE([LEFT,TOP, 0.0,-1.25],[LEFT,TOP,14.25, 0.00],2,1,001)
*
*TIRE UNE LIGNE A .55 et .85
*
 LIGNE([LEFT,TOP, 0.00,-0.55],[LEFT,TOP,14.25,-0.55],2,1)
 LIGNE([LEFT,TOP, 0.00,-0.85],[LEFT,TOP,14.25,-0.85],2,1)
*
* LIGNE VERTICALE POUR SEPARER REGIONAL-GLOBAL
*
* LIGNE([LEFT,TOP, 2.00,-1.25],[LEFT,TOP, 2.00, 0.00],2,1)
*
* HEURE DE VALIDITE
*
 WRITE([LEFT,TOP,  7.12, -0.40],29,
 'V@DAT(DATV,H,Z ,DW,-,JS, ,J, ,MON,-,MOI, ,A)',["IN",2])
*
* DESCRIPTION DE LA CARTE
*
 WRITE([LEFT,TOP, 7.12,-0.15],29,
 '${FCT}H FORECAST - PREVISION ${FCT}@AH',
 ["CNTR",01])
 WRITE([LEFT,TOP, 7.12,-0.70],29,
 '850 @AHP@AA   HEIGHT-HAUTEUR   TEMPERATURE',
 ["CNTR",01])
*
* EXPLICATION DU CONTENU DES CARTES
* hauteur a 850mb
*
 LIGNE([LEFT,TOP, 0.20,-1.20 ],[LEFT,TOP,0.20,-0.90 ],2,7)
 LIGNE([LEFT,TOP, 0.30,-1.20 ],[LEFT,TOP,0.30,-0.90 ],1,7)
*
 WRITE([LEFT,TOP, 0.50,-0.97 ],28, 'HEIGHT - HAUTEUR',
  ["CNTR",02, "FC",7])
 WRITE([LEFT,TOP, 0.50,-1.14],28,
 '@I9 @BH @BL ...144, 150, 156... @AD@AA@AM',["CNTR",02, "FC",7])
*
*SIMULE LIGNE POINTILLE (pour temperatures couleur=violet=5)
*
 LIGNE([LEFT,TOP, 7.07,-1.20],[LEFT,TOP, 7.07,-1.15],2,5)
 LIGNE([LEFT,TOP, 7.07,-1.09],[LEFT,TOP, 7.07,-1.03],2,5)
 LIGNE([LEFT,TOP, 7.07,-0.97],[LEFT,TOP, 7.07,-0.91],2,5)
*
 LIGNE([LEFT,TOP, 7.17,-1.20],[LEFT,TOP, 7.17,-1.15],1,5)
 LIGNE([LEFT,TOP, 7.17,-1.09],[LEFT,TOP, 7.17,-1.03],1,5)
 LIGNE([LEFT,TOP, 7.17,-0.97],[LEFT,TOP, 7.17,-0.91],1,5)
*
 WRITE([LEFT,TOP, 7.37, -0.97],28,
 'TEMPERATURE',["CNTR",02, "FC",5])
* WRITE([LEFT,TOP, 5.50,-1.14],28,
* '@F+ @F-',["CNTR",02, "FC",5,"ENH",-1])
 WRITE([LEFT,TOP, 7.37,-1.14],28,
 '@F+ @F- ...-5, 0, 5... C',["CNTR",02, "FC",5])
*
* BOITE([LEFT,TOP, 8.00,-1.20],[LEFT,TOP, 10.00,-0.90],1,5,004)
 BOITE([LEFT,TOP, 9.87,-1.20],[LEFT,TOP, 11.87,-0.90],1,5,005)
 WRITE([LEFT,TOP, 10.87,-1.05],27,'-2.5@A< T @A<+2.5',
                  ["CNTR",01,"FC",5,"IN",2,"ENH","YES"])
*
* ON REFAIT LE CADRE PARCE QUE LES INDICATIFS H L PEUVENT L'AVOIR EFFACE
*
 LIGNE([LEFT,BOTTOM, 0.00, 0.0],[RIGHT, BOTTOM, 0.00, 0.00],2,1)
 LIGNE([LEFT,TOP, 0.00, 0.00],[RIGHT, TOP, 0.00, 0.00],2,1)
 LIGNE([LEFT,BOTTOM, 0.00, 0.00],[LEFT, TOP, 0.00, 0.00],2,1)
 LIGNE([RIGHT,BOTTOM, 0.00, 0.00],[RIGHT, TOP, 0.00, 0.000],2,1)
*
*
**********************************************************************
 FRAME(0) 
*
EOFSIGMA

rm -f segfile

cp ${TASK_INPUT}/segfile segfile

for map in 1 2
do

    rm -f metacod # metacod est cree par sigma et lu par trames

    ${TASK_BIN}/sigma -imflds femap \
                      -i sigmadir${map} \
                      -date ${RUN} 

    ${TASK_BIN}/trames -device difax \
                       -format rrbx \
                       -ncp 1 \
                       -colors 0 1 1 1 1 1 1 1 \
                       -dn rrbx${map} \
                       -orn rot -size 2660 -offset 0 

    ${TASK_BIN}/img_flip rrbx${map} rrbx${map}_flipped -flip 270

    cp rrbx${map}_flipped ${TASK_OUTPUT}/difax/${SEQ_SHORT_DATE}_AFR${map}_${FCT}H_${RUNHOUR}Z

    ${TASK_BIN}/toimv6 rrbx${map}_flipped MAPS_CMC_GLB_AFR${map}_P_2PAN_${FCT}H_${RUNHOUR}Z:CMC:CHART:IMV6 -chk_mono -metwidth -desc \'MAPS CMC GLB AFR${map} P 2PAN  ${FCT}H ${RUNHOUR}Z\'

    ${TASK_BIN}/c.dissem -j g1afr -p image -c pds_ocx -d /RAW_chart MAPS_CMC_GLB_AFR${map}_P_2PAN_${FCT}H_${RUNHOUR}Z:CMC:CHART:IMV6

done
