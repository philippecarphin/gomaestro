#!/bin/ksh
#CMOI_LEVEL op
#CMOI_PLATFORM op_f
#
#########################################################################
#########################################################################
#                                                                      ##
#        AVANT DE MODIFIER CETTE TACHE CONTACTER CMDW                  ##
#       ATTENTION BEFORE MODIFYING THIS JOB CONTACT CMDW               ##
#                                                                      ##
# sinon la responsabilit� de la maintenance de cette t�che ainsi       ##
# que de tous ses sous programmes tomberont sous votre responsabilit�  ##
#                                                                      ##
#########################################################################
#########################################################################
#
#
#
#################################################################################
#
# AUTEUR: CMDW - fev 2020
#
# OBJET: Calculs de la moyenne ponderee de l'humidite relative pour la basse 
#        troposphere et la troposphere moyenne. Ceci est un produit pour 
#        l'aviation (edigraf).
#
#
#################################################################################
#


inputfile=${TASK_INPUT}/fichier_pres

# Choisir HU 1000
echo -e "desire(-1,'HU',-1,-1,1000)" | ${TASK_BIN}/editfst -n -s ${inputfile} -d hu_1000
# Choisir HU 925
echo -e "desire(-1,'HU',-1,-1,925)" | ${TASK_BIN}/editfst -n -s ${inputfile} -d hu_925
# Choisir HU 850
echo -e "desire(-1,'HU',-1,-1,850)" | ${TASK_BIN}/editfst -n -s ${inputfile} -d hu_850
# Choisir HU 700
echo -e "desire(-1,'HU',-1,-1,700)" | ${TASK_BIN}/editfst -n -s ${inputfile} -d hu_700
# Choisir HU 500
echo -e "desire(-1,'HU',-1,-1,500)" | ${TASK_BIN}/editfst -n -s ${inputfile} -d hu_500
# Ajouter hu_1000 et hu_850
${TASK_BIN}/r.diag addf ./hu_1000 ./hu_850 ./hu_1000_850
# Multipler hu_925 par 2
${TASK_BIN}/r.diag xlin ./hu_925 ./2hu_925 -a 2 -b 0
# Ajouter hu_1000_850 et 2hu_925
${TASK_BIN}/r.diag addf ./hu_1000_850 ./2hu_925 ./hu_1000_2hu_925_hu_850

spooki_run --batch --implementation ${SPOOKI_IMPLEMENTATION} --runID ${RUNID} \
	           "[ReaderStd --input ${inputfile}] >> \
	            [Select --fieldName TT --verticalLevel 1000,925,850,700,500] >> \
                    [WriterStd --output tt]"

spooki_run --batch --implementation ${SPOOKI_IMPLEMENTATION} --runID ${RUNID} \
	           "[ReaderStd --input tt] >> \
	            [ZapSmart --fieldNameFrom TT --fieldNameTo TD] >> \
                    [WriterStd --output td]"

spooki_run --batch --implementation ${SPOOKI_IMPLEMENTATION} --runID ${RUNID} \
                   "[ReaderStd --input td tt] >> \
	            [HumiditySpecific] >> [WriterStd --output hus]" 

# Choisir HUS 1000
echo -e "desire(-1,'HU',-1,-1,1000)" | ${TASK_BIN}/editfst -n -s hus -d hus_1000
# Choisir HUS 925
echo -e "desire(-1,'HU',-1,-1,925)" | ${TASK_BIN}/editfst -n -s hus -d hus_925
# Choisir HUS 850
echo -e "desire(-1,'HU',-1,-1,850)" | ${TASK_BIN}/editfst -n -s hus -d hus_850
# Choisir HUS 700
echo -e "desire(-1,'HU',-1,-1,700)" | ${TASK_BIN}/editfst -n -s hus -d hus_700
# Choisir HUS 500
echo -e "desire(-1,'HU',-1,-1,500)" | ${TASK_BIN}/editfst -n -s hus -d hus_500
# Ajouter hus_1000 et hus_850
${TASK_BIN}/r.diag addf ./hus_1000 ./hus_850 ./hus_1000_850
# Multipler hus_925 par 2
${TASK_BIN}/r.diag xlin ./hus_925 ./2hus_925 -a 2 -b 0
# Ajouter hus_1000_850 et 2hus_925
${TASK_BIN}/r.diag addf ./hus_1000_850 ./2hus_925 ./hus_1000_2hus_925_hus_850

# Diviser hu_1000_2hu_925_hu_850 par hus_1000_2hus_925_hus_850
${TASK_BIN}/r.diag divf ./hu_1000_2hu_925_hu_850 ./hus_1000_2hus_925_hus_850 ./rh_low
# Rajouter les metadata a rh_low
echo -e "desire(-1,['>>','^^'])" | ${TASK_BIN}/editfst -n -s ${inputfile} -d rh_low

ip850=`${TASK_BIN}/r.ip1 850 2`

spooki_run --batch --implementation ${SPOOKI_IMPLEMENTATION} --runID ${RUNID} \
	           "[ReaderStd --input rh_low] >> [SetUpperBoundary --value 1.0] >> \
	            [Zap --fieldName HR --userDefinedIndex ${ip850} --pdsLabel HRWAVG --doNotFlagAsZapped] >> \
	            [WriterStd --noUnitConversion --output ${SEQ_SHORT_DATE}_${PROG}]"


# Ajouter hu_850 et hu_500
${TASK_BIN}/r.diag addf ./hu_850 ./hu_500 ./hu_850_500
# Multipler hu_700 par 2
${TASK_BIN}/r.diag xlin ./hu_700 ./2hu_700 -a 2 -b 0
# Ajouter hu_850_500 et 2hu_700
${TASK_BIN}/r.diag addf ./hu_850_500 ./2hu_700 ./hu_850_2hu_700_hu_500

# Ajouter hus_850 et hus_500
${TASK_BIN}/r.diag addf ./hus_850 ./hus_500 ./hus_850_500
# Multipler hus_700 par 2
${TASK_BIN}/r.diag xlin ./hus_700 ./2hus_700 -a 2 -b 0
# Ajouter hus_850_500 et 2hus_700
${TASK_BIN}/r.diag addf ./hus_850_500 ./2hus_700 ./hus_850_2hus_700_hus_500

# Diviser hu_850_2hu_700_hu_500 par hus_850_2hus_700_hus_500
${TASK_BIN}/r.diag divf ./hu_850_2hu_700_hu_500 ./hus_850_2hus_700_hus_500 ./rh_mid
# Rajouter les metadata a rh_mid
echo -e "desire(-1,['>>','^^'])" | ${TASK_BIN}/editfst -n -s ${inputfile} -d rh_mid

ip500=`${TASK_BIN}/r.ip1 500 2`

spooki_run --batch --implementation ${SPOOKI_IMPLEMENTATION} --runID ${RUNID} \
	           "[ReaderStd --input rh_mid] >> [SetUpperBoundary --value 1.0] >> \
	            [Zap --fieldName HR --userDefinedIndex ${ip500} --pdsLabel HRWAVG --doNotFlagAsZapped] >> \
                    [WriterStd --noUnitConversion --output ${SEQ_SHORT_DATE}_${PROG}]"

cat << EOFEDIT > editdir
DESIRE(-1,["HR"])
ZAP("P")
EOFEDIT

${TASK_BIN}/editfst -strict -s ${SEQ_SHORT_DATE}_${PROG} -d ${TASK_OUTPUT}/dpath/${SEQ_SHORT_DATE}_${PROG} -i editdir

