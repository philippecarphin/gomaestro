#!/bin/ksh
#.**********************************************************************
#.
#.     JOB NAME - g1plhsg 
#.
#.     STATUS        semi-operationnelle
#.
#.     ORIGINATOR  OIS
#
#.     PRODUCT - colormap
#.       
#.               colormap_#    cascaded(hrs)   time avbl(GMT)   description 
#.               ----------    -------------   -------------    -----------
#
#.     clr_map   natlan18           24             n/a          n. atlc sig wx
#.     clr_map   natlan24           24             n/a          n. atlc sig wx
#.
#.
#.     DESCRIPTION - north atlantic sig wx 18 and 24hr show
#.                   1) nuage bas et moyens
#.                   2) taux de precipitation
#.                   3) epaisseur 1000-700mb 
#.        
#.     MODIFS    Alain Bergeron CMDW   Sept. 1998
#.
#.     DESCRIPTION   Suite au remplacement du SEF par le GEM global, les 
#.                   champs NB,NM,NH ne sont plus disponibles dans les sorties
#.                   du modele. Il faut donc les calculer a partir de FN
#.                   (ajout au debut du script, base sur r1fracn) avec le
#.                   programme fracnua.
#
#      NOMVAR:       P0 pour identifier les temps
#                    FN variable de base
#                    NT requise en mode DEBUG
#
#      OUTPUT:       le fichier sortie_fn est un fichier standard
#                    contenant les champs NB,NM,NH sur la meme
#                    grille que celle de depart. Il faut copier
#                    ces champs dans le fichier operationnel
#                    des donnees ajoutees
#.**********************************************************************


nom_fichier_modele=${TASK_INPUT}/fichier_modele/*

#------------------------------------------------------------------
cat << EOFEDIT > editstd1
 DEBUG = OUI
 DESIRE(-1,"P0",-1,-1,-1,[6,12,18,24,30,36,42,48],-1)
EOFEDIT
cat << EOFEDIT > editstd2
 DEBUG = OUI
 DESIRE(-1,"FN",-1,-1,-1,[6,12,18,24,30,36,42,48],-1)
EOFEDIT
#-----------------------------------------------------------------
${TASK_BIN}/editfst -strict -s $nom_fichier_modele \
            -d tampon \
            -i editstd1

${TASK_BIN}/voir -iment tampon
#-----------------------------------------------------------------
${TASK_BIN}/editfst -strict -s $nom_fichier_modele \
            -d tampon \
            -i editstd2

${TASK_BIN}/voir -iment tampon
#---------------------------------------nettoyage----------------
rm -f editstd1
rm -f editstd2


#****************************************************************
#   ici on utilise les niveaux par defaut
#   (argument bas?, moy?, haut? absents)
#
#   aucun champs additionnel (mode -debug oui)
#****************************************************************

#--------------------------------------------------------------------
# Note de conversion a maestro - Naysan Saran Nov. 2013 
#--------------------------------------------------------------------
# fracnua_2000 transforme le chemin de $fnout en minuscules. 
# Pour y remedier, on cree un fichier local d'output que l'on 
# deplace par la suite
#--------------------------------------------------------------------

fnout=${TASK_OUTPUT}/DEPOT/${fracnuage_glb_out}
fnout_local=fracnuage_glb

rm -f $fnout
rm -f $fnout_local

#
#--------------------------------utilise pour etre
#                                capable d afficher avec xrec
cat << EOFEDIT > editstd0
 DEBUG = OUI
 DESIRE(-1,['^^','>>'],-1,-1,-1,-1,-1)
EOFEDIT
${TASK_BIN}/editfst -strict -s $nom_fichier_modele \
            -d $fnout_local \
            -i editstd0
rm -f editstd0
#---------------------------------programme fn
#OIS removing path info to use the program in ovbin 
#ABSPGM=$AFSISIO/programs

${TASK_BIN}/fracnua_2000  -irpgsm tampon \
                          -date ${CMCSTAMP} \
                          -dtstmp $CMCSTAMP \
                          -ofile  $fnout_local

mv $fnout_local $fnout 

# CARTE NATLAN (nord atlantique)

# previsions du spectral pour t+18 et t+24
# qui contient tous les parametres dans les fichiers spepres/

CARTE=natlan
FCST="18 24"
MODEL=GLOBAL
HEUR='HEURE(18,24)'
GRID='GRILLE(PS,73,43,49.4,56.9,100000.0,-70.0,NORD)'

#*****************************************************************
#            CAPTURE DES DONNEES DU MODELE
#*****************************************************************
# on genere le fichier standard $fnout
# qui doit contenir les parametres 'NB','NM','RT' et 'GZ'

OPREV1=${TASK_INPUT}/oprev1/*
OPREV2=${TASK_INPUT}/oprev2/*

#***********************************************
# directives de recherche des fichiers standards
#***********************************************

cat << EOT > fstdir1
DESIRE(-1,'RT',-1,-1,-1,
[$HR1,@,$HR2,DELTA,$DELT],-1)
EOT

cat << EOT > fstdir2
DESIRE('P','GZ',-1,-1,[1000,700],
[$HR1,@,$HR2,DELTA,$DELT],-1)
EOT

#=======================================================================
${TASK_BIN}/editfst -strict -s $OPREV1 -d $fnout -i fstdir1 -e

${TASK_BIN}/editfst -strict -s $OPREV2 -d $fnout -i fstdir2 -e
#=======================================================================
 
#***************************************************
# directives PGSM
# pour obtenir les champs prevus
# dans la projection $GRID desiree
#***************************************************

cat << EOT > pgsmdir1
 SORTIE(STD,10,A)
 EXTRAP(VOISIN)
 $GRID
 $HEUR
 CHAMP('NB',ALL)
 CHAMP('NM',ALL)
*conversion de m/s a mm/h
 CONV('RT',0.,3.6E6,0.185E-8)
 CHAMP('RT',ALL)
 CHAMP(EPAIS,1000,700)
EOT

#=======================================================================
${TASK_BIN}/pgsm -iment $fnout -ozsrt modele -i pgsmdir1
#=======================================================================


#*****************************************************************
#             DIRECTIVES SIGMA POUR CARTES COLOREES
#                         DE PREVISION
#                    AFFICHABLES A L'ECRAN (SUN)
#*****************************************************************
# DIRECTIVES SIGMA POUR PRODUIRE : nuages bas et moyens
#                                  et epaisseur (700-1000)mb
#                                  a differents temps de prevision
#                                  chacun sur 1 seul panneau
#******************************************************************************
# ORIGINAL: 28 mai 1991
#           GILLES DESAUTELS (DDO)
#
# MODIFICATION: juin 1992
#               ANNE FRIGON
#
#
for TEMPS in $FCST
do

cat << EOFSIGMA > sigmadir
*
*--------------------------
* GENERAL
*--------------------------
*
 CLIP   = OUI
 LISSAGE= OUI
*
*--------------------------
* FONTES
*--------------------------
*
*LA FONTE 15 CONTIENT LES MAJUSCULES, LA 16 LES MINUSCULES
*LA FONTE 10 CONTIENT LES LETTRES MAJUSCULES DES H,L
*LA FONTE 11 CONTIENT LES LETTRES MINUSCULES CORRESPONDANTES
*LA FONTE 17 CONTIENT LES CHIFFRES DES ETIQUETTES (FONTE ETROITE)
*LA FONTE  2 CONTIENT LES GRANDS + ET -
*LA FONTE  3 CONTIENT LES GRANDS LETTRES GRECQUES
*LA FONTE  9 CONTIENT LE CENTRE (X DANS O)
*
*   @         A  B  C  D  E  F  G  H  I
*   "CASE" 0  1  2  3  4  5  6  7  8  9
 SETFONT([15,16,10,11,12,13, 2, 3,17, 9])
*
*--------------------------
* GROUPES
*--------------------------
*
* GROUPE LOT=1     POUR LES ISOLIGNES DE NUAGES BAS
*                  COULEUR CO=1 (NOIR)
*                  et ETIQUETTES (NOIR)
*
 GROUPE(1,["CO",1,"TH",2,
   "CA",8,"FC",1,"IN",2,"SI",500,"BO","YES","CN",34])
*
* GROUPE LOT=3-4-5 POUR LA TAILLE DES MAX-MIN (dummy)
*
 GROUPE(3,["CO",1,"CA",0,"SI",800,"IN",3,"EN","YES","CN",01]) 
 GROUPE(4,["CO",1,"CA",9,"SI",300,"IN",2,"EN","YES","CN",01]) 
 GROUPE(5,["CO",1,"CA",8,"SI",600,"IN",2,"EN","YES","CN",01]) 
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*
* 
* GROUPE LOT=10    POUR LES ISOLIGNES DE "DZ" 700-1000mb
*                  COULEUR CO=7 (BLEU)
*                  et ETIQUETTES (NOIR sur blanc)
*
 GROUPE(10,["CO",7,"TH",2,
   "CA",8,"FC",1,"BC",0,"CL","YES","IN",2,"SI",500,"BO","YES","CN",34])
*
* GROUPE LOT=11    POUR LES ISOLIGNES DE "DZ" 700-1000mb
*                  COULEUR CO=2 (ROUGE)
*                  et ETIQUETTES (NOIR sur blanc)
*
 GROUPE(11,["CO",2,"TH",2,
   "CA",8,"FC",1,"BC",0,"CL","YES","IN",2,"SI",500,"BO","YES","CN",34])
*
* ECRITURE ID CARTE
*
* GROUPE 26 POUR TITRE PREVISIONS
*
 GROUPE(26,["CA",0,"SI",300,"IN",2,"FC",1,"CN",02]) 
*
* UTILISE VALEUR PAR DEFAUT POUR PANNEAU([0.00, 0.00])
*
 PANNEAU([0.0,0.0])
*
*--------------------------
* GEOGRAPHIE
*--------------------------
*
 MAPLAB="ST",1,2,1,1,10,0,NON,NON,NON,1,1.,OUI
 MAPCOLR=1
*
 POUCES=40.0
*
 ECHLGRI= NON
*
 DASH([1777B])
*
 OPTIONS(MAPOPT,["OU","GL","GR",-1.0,"LA","FALSE","TH",2]) 
*
*--------------------------
* NUAGES BAS EN TONS (BLEU)
*--------------------------
*
*DIRECTIVES POUR LES LIGNES DE CONTOUR
*PRECISE POUR CHAQUE CHAMP LE FACTEUR MULTIPLICATIF ET INTERVALLE CONTOUR
*(ENONCE OBLIGATOIRE)  RECOMMANDE DE NE PAS MODIFIER SCAL EN COURS DE ROUTE
*
 SCAL("NB",1.0,.1)
*
 LIMITE=0.,2.
*
 DASH([1252B])
*
*               1       2       3
 SETHPAL([0000000,0216111,0716024],3)
*
 NIVEAUX=   0.6,  2.0
 MOTIFS= 001,  003,  001
*
 HAFTON("NB","P",-1,-1,$TEMPS,-1,[GEO,NIV,PAT])
* 
*--------------------------
* LIGNES DE NUAGES BAS (NOIR)
*--------------------------
*
*DIRECTIVES POUR LES ETIQUETTES DES NUAGES (NOIR)
* 
 OPTIONS(ISPSET,["RNDL",.1, "RNDV",1.]) 
*
 OPTIONS(ISPSET,["ECOL",000100900, "LABL",1, "ILAB",1])
*
*DIRECTIVES POUR LES MAX-MIN DES NUAGES (dummy)
* 
 OPTIONS(ISPSET,["HCAR"," ","LCAR"," ","CCAR"," ","LHCE",3,"NHL",1])
 OPTIONS(ISPSET,["HILO",3,"CNTR",4,"VALU",5]) 
* 
*DIRECTIVES POUR LE TRACAGE DES NUAGES BAS (NOIR)
*
 OPTIONS(ISPSET,["MAJR",1, "MINR",1, "NULB", 4])
*
 NIVEAUX=0.6,2.0
*
 VARIAN("NB","P",-1,-1,$TEMPS,-1,[GEO,NIV])
*
*--------------------------
* LIGNES DE NUAGES MOYENS (NOIR)
*--------------------------
*
*DIRECTIVES POUR LES LIGNES DE CONTOUR
*PRECISE POUR CHAQUE CHAMP LE FACTEUR MULTIPLICATIF ET INTERVALLE CONTOUR
*(ENONCE OBLIGATOIRE)  RECOMMANDE DE NE PAS MODIFIER SCAL EN COURS DE ROUTE
*
 SCAL("NM",1.0,.1)
*
 NIVEAUX=0.6,2.0
*
 VARIAN("NM","P",-1,-1,$TEMPS,-1,[GEO,NIV])
*
*--------------------------
* NUAGES MOYENS EN TONS (ROUGE)
*--------------------------
*
 NIVEAUX=   0.6,  2.0
 MOTIFS= 001,  002,  001
*
 HAFTON("NM","P",-1,-1,$TEMPS,-1,[GEO,NIV,PAT])
* 
*--------------------------
* LIGNES DE TX DE PRECIP (VERT)
*--------------------------
*
*DIRECTIVES POUR LES LIGNES DE CONTOUR
*PRECISE POUR CHAQUE CHAMP LE FACTEUR MULTIPLICATIF ET INTERVALLE CONTOUR
*(ENONCE OBLIGATOIRE)  RECOMMANDE DE NE PAS MODIFIER SCAL EN COURS DE ROUTE
*
 SCAL("RT",1.0,1.)
*
 DASH([0707B])
*
 NIVEAUX=0.2,1.0,30.0
*
 OPTIONS(OPTN,["CO",6])
* 
 VARIAN("RT","P",-1,-1,$TEMPS,-1,[GEO,NIV])
*
*--------------------------
* TX DE PRECIP EN TONS (VERT)
*--------------------------
*
*               1       2       3
 SETHPAL([0000000,0616113,0616080],3)
*
 NIVEAUX=   0.2,  1.0,  30.0
 MOTIFS= 001,  002,  003,  001
*
 HAFTON("RT","P",-1,-1,$TEMPS,-1,[GEO,NIV,PAT])
* 
*--------------------------
* LIGNES DE "DZ" 700-1000mb (1)
*--------------------------
*
*DIRECTIVES POUR LES LIGNES DE CONTOUR
*PRECISE POUR CHAQUE CHAMP LE FACTEUR MULTIPLICATIF ET INTERVALLE CONTOUR
*(ENONCE OBLIGATOIRE)  RECOMMANDE DE NE PAS MODIFIER SCAL EN COURS DE ROUTE
*
 SCAL("DZ",1.0,3.)
*
 DASH([1463B])
*
*DIRECTIVES POUR LES ETIQUETTES DES "DZ" (NOIR)
* 
 OPTIONS(ISPSET,["RNDL",1., "RNDV",1.]) 
*
 OPTIONS(ISPSET,["ECOL",350650950, "LABL",10, "ILAB",1])
*
*DIRECTIVES POUR LES MAX-MIN DES "DZ" (dummy)
* 
 OPTIONS(ISPSET,["HCAR"," ","LCAR"," ","CCAR"," ","LHCE",3,"NHL",1])
 OPTIONS(ISPSET,["HILO",3,"CNTR",4,"VALU",5]) 
* 
*DIRECTIVES POUR LE TRACAGE DES "DZ" (BLEU)
*
 LIMITE=242.,284.
*
 OPTIONS(ISPSET,["MAJR",10, "MINR",10, "NULB", 4])
*
 VARIAN("DZ","P",-1,1000,700,$TEMPS,[GEO])
*
*--------------------------
* LIGNES DE "DZ" 700-1000mb (2)
*--------------------------
*
*DIRECTIVES POUR LE TRACAGE DES "DZ" (ROUGE)
*
 LIMITE=287.,350.
*
 OPTIONS(ISPSET,["MAJR",11, "MINR",11, "NULB", 4])
*
 VARIAN("DZ","P",-1,1000,700,$TEMPS,[GEO])
*
*--------------------------
* TITRES
*--------------------------
*
*               1
 SETHPAL([0001001],1)
*
 BOITE([LEFT,BOTTOM,0.10,0.10],[LEFT,BOTTOM,9.70,2.80],2,2,001)
*
 WRITE([LEFT,BOTTOM,  0.20,  2.40],26,
  'PREV. $TEMPS@AH MODELE $MODEL')
 WRITE([LEFT,BOTTOM,  0.20,  1.90],26,
  'NUAGES BAS (@AB@AL@AE@AU) ET MOYENS')
 WRITE([LEFT,BOTTOM,  0.20,  1.40],26,
  'TAUX DE PCPN (@AM@AM/@AH)')
 WRITE([LEFT,BOTTOM,  0.20,  0.90],26,
  'EPAISSEURS 1000-700MB (DAM)')
 WRITE([LEFT,BOTTOM,  0.20,  0.40],26,
  'V@DAT(DATV,H,Z ,DW,-,JS, ,J, ,MON,-,MOI, ,A)')
*
*--------------------------
 FRAME(0) 
*--------------------------
EOFSIGMA

# sigma cree les fichiers 'metacod' et 'segfile'

rm -f metacod
rm -f segfile

#====================================================
${TASK_BIN}/sigma -imflds modele -i sigmadir -date ${RUN}
#====================================================
 
# traduction du metacode pour obtenir l'information de l'image
# NOTE: pour obtenir image sur SUN12 au 2e etage
#       avec ANIM ou ANIM-RRBX
#       il ne faut pas donner le format rrbx

#====================================================
${TASK_BIN}/trames -mc metacod -segf segfile \
         -device sun12 -size 1200 -hy 1200 -dn razz2
#====================================================

# et envoi du raster aux operations
# pour que l'image soit affichable avec ANIM
case "$TEMPS"
in
    18) carte=colour27;;
    24) carte=colour28;;
esac

#====================================================

# send the chart to anim

${TASK_BIN}/ocxcarte -f $carte -d colour -r ${SEQ_SHORT_DATE}

#====================================================

done


