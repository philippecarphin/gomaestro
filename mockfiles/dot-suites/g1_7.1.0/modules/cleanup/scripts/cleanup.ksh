#!/bin/ksh

set -ex

DELETE_COMMAND=${DELETE_COMMAND:-"rm -f"}

while [ $# -gt 0 ]; do
    case $1 in
	--filter=*) filter=$(echo $1 | cut -d= -f2-);;
	--date=*) thisdate=$(echo $1 | cut -d= -f2-);;
	--delay=*) default_delay=$(echo $1 | cut -d= -f2-);;
	--path=*) thispath=$(echo $1 | cut -d= -f2-);;
	--delete=*) delete=$(echo $1 | cut -d= -f2-);;
	*) echo "This option is not recognized!!"
	    exit 1;;
    esac
    shift
done

if [ "${delete}" = 'yes' ]; then
    DELETE_COMMAND="rm -f"
elif [ "${delete}" = 'no' ]; then
    DELETE_COMMAND="echo rm -f"
else
    echo "The argument '--delete' must be 'yes' or 'no' instead of '${delete}'"
    exit 1
fi

DEFAULT_ERASE_DATE=$(${TASK_BIN}/r.date ${thisdate} -${default_delay})

for fic in $(find ${thispath} -type f); do
    echo fic="${fic}"
    pattern_matched=""
    while read line; do ## On lit toutes les lignes du fichier ${TASK_INPUT}/filter
	eval pattern=$(echo ${line} | cut -d' ' -f1)
	    
	delay=$(echo ${line} | cut -d' ' -f2)
	if [[ "${delay}" = *infinite* ]]; then
	    continue
	fi
	erase_date=$(r.date ${thisdate} -${delay} | cut -c-10)
	
	if [[ "${fic}" = *${pattern}* ]]; then
	    pattern_matched="yes"
	    if [[ "${fic}" = *${erase_date}* ]]; then
		$TASK_BIN/nodelogger -n $SEQ_NODE -s infox -m  "Erasing ${TRUE_HOST}:${fic} because of pattern ${line}"
		${DELETE_COMMAND} ${fic}
	    fi
	fi
    done < ${filter}
    
    if [ -z "${pattern_matched}" ]; then
	if [[ "${fic}" = *${DEFAULT_ERASE_DATE}* ]]; then
	    ${DELETE_COMMAND} ${fic}
	fi
    fi
done
