Nom équivalent : Forecast


Choses à faire après avoir fait son "git clone":
1. Créer les liens symboliques pour "listings", "logs" et "sequencing" sur des datas visibles de eccc-ppp1 et eccc-ppp2 pour pouvoir changer facilement entre ces deux machines (par exemple /home/ords)..
2. Créer le répertoire "hub" avec des liens eccc-ppp1 et eccc-ppp2 vers les datas appropriés.

Important :
* le module "maestro_validation_module" est un git subtree, le projet reside ici : https://gitlab.science.gc.ca/CMDS_validation/maestro_validation_module
* le module "cleanup" est un git subtree, le projet reside ici : https://gitlab.science.gc.ca/CMDI/maestro_module_cleanup

