Voici les instructions:
 
 # Mise à jour du submodule git
 
```
SUITE_USAGER=<suite_contenant_le_submodule>
COMPONENT=components/g1/cmdw_post_processing
NEW_BRANCH=rel-7.1.0/ops
```
 
   1) # Remplacer le submodule git du post-traitement cmdw (G1) par sa nouvelle version
 
```
# Aller où le submodule git du post-traitement de cmdw est installé
cd ${SUITE_USAGER}/${COMPONENT}
## Obtenir le nom du dépôt gitlab pour ajouter au commentaire plus bas:
GITLAB_REPO=$(git remote -vv|head -1 | awk '{print $2}')

## Mettre à jour la composante avec la nouvelle branche :
git fetch origin ${NEW_BRANCH}
git checkout -b ${NEW_BRANCH} origin/${NEW_BRANCH}
 
# Remonter à la super suite
cd ${SUITE_USAGER}

## Mettre à jour la branche dans le fichier .gitmodule :
git config -f .gitmodules submodule.${COMPONENT}.branch ${NEW_BRANCH}

## Commiter le changement au submodule avec le message approprie :
git commit ${COMPONENT} .gitmodules -F - <<EOF
Updated component ${COMPONENT} from
${GITLAB_REPO} to version ${NEW_BRANCH}
EOF
```

   2) # S'assurer que le fichier resources.def de gdps_aaaammdd/g1 contient les definitions des variables pour CMDW qui sont dans le fichier resources.def du submodule git cmdw_post_processing et faire le commit.


Il faudra exécuter le warmstart pour la première exécution
