short_date = 2021030209
short_other_date = 2021030208
full_path = $(PWD)/$(inner_path)
inner_path = mockfiles/dot-suites/philtest
curl_cmd := curl -X POST 'localhost:8008/start?exp=$(full_path)&datestamp=$(short_date)0000'
curl_other_cmd := curl -X POST 'localhost:8008/start?exp=$(full_path)&datestamp=$(short_other_date)0000'
serve:
	go run ./cmd/expserver
xflow:
	SEQ_EXP_HOME=$(PWD)/$(inner_path) xflow -d $(short_date)

test:
	go test ./... -v

start:
	$(curl_cmd)

double: start
	sleep 7
	$(curl_other_cmd)

stest:
	go run ./cmd/expserver &
	sleep 1
	$(curl_cmd)

kill:
	killall expserver

