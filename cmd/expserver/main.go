package main

import (
	"os"

	"gitlab.science.gc.ca/phc001/gomaestro/mrest"
)

func main() {
	dir := os.Getenv("PWD")
	s := mrest.ExpServer{Dir: dir}
	s.DemoServeExperiments()
}
