package flowxml

import (
	"fmt"
)

type stitcher struct {
	ExpHome string
}

func (s *stitcher) resolveModuleReferences(xml *XMLFlowNode) error {
	for i, ch := range xml.Children {
		switch ch.XMLName.Local {
		case "MODULE":
			if len(ch.Children) > 0 {
				return fmt.Errorf("Resolving module reference for node that already has children")
			}

			moduleFile := fmt.Sprintf("%s/modules/%s/flow.xml", s.ExpHome, ch.Name)
			newChild, err := unmarshalModuleFile(moduleFile)
			if err != nil {
				return err
			}

			// Module references may be links to another module.  Therefore the name attribute
			// of the toplevel node of the module file will not necessarily match the name attribute
			// of the module tag referencing it.  The resolved module object takes the name given in
			// the tag rather than the name attribute of the toplevel node of the module file.
			newChild.Name = ch.Name

			xml.Children[i] = newChild
		}

		err := s.resolveModuleReferences(xml.Children[i])
		if err != nil {
			return err
		}
	}
	return nil
}
