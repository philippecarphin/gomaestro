package flowxml

import (
	"fmt"
	"os"
	"testing"
)

var expHome string = os.Getenv("PWD") + "/../mockfiles/dot-suites/philtest"

func TestUnmarshalModule(t *testing.T) {
	_, err := unmarshalModuleFile(expHome + "/EntryModule/flow.xml")
	if err != nil {
		t.Fatal(err)
	}
}
func TestReadExperimentHome(t *testing.T) {
	r, err := ReadExperimentHome(expHome)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(r)
}
