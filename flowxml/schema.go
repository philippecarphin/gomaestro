package flowxml

// This package specifies the layout of XML files in the form of Go types.

import (
	"encoding/xml"
	"io/ioutil"
)

// XMLFlowNode represents the XML tags of flow.xml files.
type XMLFlowNode struct {
	XMLName   xml.Name
	Name      string          `xml:"name,attr"`
	Submits   []*XMLSubmits   `xml:"SUBMITS"`
	DependsOn []*XMLDependsOn `xml:"DEPENDS_ON"`
	Children  []*XMLFlowNode  `xml:",any"`
	XMLCommon
}

// XMLDependsOn represents DEPENDS_ON tags in XML
type XMLDependsOn struct {
	DepName string `xml:"dep_name,attr"`
	XMLCommon
}

// XMLSubmits represents the submits references that define the graph seen in XFlow.
type XMLSubmits struct {
	TagName xml.Name
	SubName string `xml:"sub_name,attr"`
	Type    string `xml:"type,attr"`
}

// XMLCommon defines the common attributes that a node can have
type XMLCommon struct {
	Status     string   `xml:"status,attr"`
	Type       string   `xml:"type,attr"`
	Unparsed   []string `xml:",any,attr"`
	ValidHour  string   `xml:"valid_hour,attr"`
	Index      string   `xml:"index,attr"`
	LocalIndex string   `xml:"local_index,attr"`
	Hour       string   `xml:"hour,attr"`
}

func unmarshalModuleFile(filename string) (*XMLFlowNode, error) {
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	var mod XMLFlowNode
	xml.Unmarshal(b, &mod)

	return &mod, nil
}
