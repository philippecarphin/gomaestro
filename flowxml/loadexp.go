// Package flowxml loads xml files from an expermient home directory.
package flowxml

import (
	"fmt"
)

// ReadExperimentHome returns a tree of XMLFlowNode from an experiment home with module references resolved.
func ReadExperimentHome(expHome string) (*XMLFlowNode, error) {

	s := stitcher{ExpHome: expHome}

	entryModule, err := unmarshalModuleFile(fmt.Sprintf("%s/EntryModule/flow.xml", s.ExpHome))
	if err != nil {
		return nil, err
	}

	s.resolveModuleReferences(entryModule)

	return entryModule, nil
}
